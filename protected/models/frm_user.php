<?php

class frm_user extends CFormModel
{
	public $id;
	public $fname;
	public $lname;
	public $username;
	public $password;
	public $department;
	public $department_name;
	public $tel;
	public $address;
	public $email;
	public $remark;
	public $line;
        
	/*
	public function rules()
	{
		return array(
			array('id','name'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	
	*/

	public function save_insert()
	{
		//check error
		//เช็คว่ามีข้อมูลหรือไม่
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sql ="select count(*) as aa from ictech_mas_user where status=1 and create_by=".$createby." and username='".$this->username."' and password='".$this->password."' ";
        
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem){
			if ($dataitem['aa']>0){
				Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
				return false;
				}
			}		
		
		//save
		$sql = " INSERT INTO ictech_mas_user (line,fname,lname,username,password,department_id,department,mobile,email,address,remark,create_date,create_by)";
        $sql.= " VALUES(:line,:fname,:lname,:username,:password,:department_id,:department,:mobile,:email,:address,:remark,now(),$createby)";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":fname", $this->fname);	
		$command->bindValue(":lname", $this->lname);	
		$command->bindValue(":username", $this->username);	
		$command->bindValue(":password", $this->password);	
		$command->bindValue(":department", $this->department_name);	
		$command->bindValue(":department_id", $this->department);	
		$command->bindValue(":mobile", $this->tel);	
		$command->bindValue(":email", $this->email);	
		$command->bindValue(":address", $this->address);	
		$command->bindValue(":remark", $this->remark);		
		$command->bindValue(":line", $this->line);		
		if($command->execute()) {
            $id = Yii::app()->db->getLastInsertID();           
			return true;            
		} else { 
			Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}		
	}
    public function save_update()
	{
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		//save
		$sql ="select count(*) as aa from ictech_mas_user where status=1 and create_by=".$createby." and username='".$this->username."' and password='".$this->password."' and id!='".$this->id."'";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem)
        {
            if ($dataitem['aa']>0){
                Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
                return false;
            }
        }	
        $sql = " update ictech_mas_user set ";
        $sql.= " line=:line,fname=:fname,lname=:lname,username=:username,password=:password, ";
        $sql.= " department_id=:department_id,department=:department,mobile=:mobile,email=:email, ";
        $sql.= " address=:address,remark=:remark, ";
        $sql.= " update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);
        $command->bindValue(":line", $this->line);	
        $command->bindValue(":fname", $this->fname);	
		$command->bindValue(":lname", $this->lname);	
		$command->bindValue(":username", $this->username);	
		$command->bindValue(":password", $this->password);	
		$command->bindValue(":department", $this->department_name);	
		$command->bindValue(":department_id", $this->department);	
		$command->bindValue(":mobile", $this->tel);	
		$command->bindValue(":email", $this->email);	
		$command->bindValue(":address", $this->address);	
		$command->bindValue(":remark", $this->remark);					
        if($command->execute()) {
            return true;
        } else {
            Yii::app()->session['errmsg']='ไม่สามารถบันทึกข้อมูลได้';
            return false;
        }	
	}
		
}
