<?php

class frm_icround extends CFormModel
{
	public $id;
	public $data_check_list;
	public $id_check_list;
	public $data_comment_list;
	public $data_sub_check_list;
	public $id_sub_check_list;
	public $data_sub_comment_list;
    
	public $chk;
	public $status;
       
	/*
	public function rules()
	{
		return array(
			array('id','name'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	
	*/

    public function save_update_list()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
        
        $sql = 'call spUpdateICRoundList ('.$this->id.','.$this->chk.','.$createby.')' ;
        $command=yii::app()->db->createCommand($sql);
	   	if($command->execute()) {
            return true;        
        } else { 
            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
            return false;
        }	
       
    }
    public function save_update_list_sub()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
        
        $sql = 'call spUpdateICRoundListSub ('.$this->id.','.$this->chk.','.$createby.')' ;
        $command=yii::app()->db->createCommand($sql);
	   	if($command->execute()) {
            return true;        
        } else { 
            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
            return false;
        }	
       
    }
    
    
    
	public function save_insert()
	{
        $i=0;
        $j=0;
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
        $sum_data = $this->id_check_list == '' ? 0 : count($this->id_check_list);  
        $sum_sub_data = $this->id_sub_check_list == '' ? 0 : count($this->id_sub_check_list);  
        
            
        if( $sum_data>0){
          
            foreach($this->id_check_list as $icround_id)
            {   //echo var_dump($this->data_check_list);exit();
                if( $icround_id!=""){
                    $sql = "INSERT INTO ictech_mas_icround_checklist (department_id,icround_id,chk,remark,create_date,create_by) ";
                    $sql.= "VALUES(:department_id,:icround_id,:check,:remark,now(),$createby)";
                    $command=yii::app()->db->createCommand($sql);		
                    $command->bindValue(":icround_id", (int)$icround_id);	
                    $command->bindValue(":department_id", (int)$this->id);	
                    $command->bindValue(":check", $this->data_check_list[$i]=="" ? null:$this->data_check_list[$i]);
                    $command->bindValue(":remark", $this->data_comment_list[$i]);		
                    if($command->execute()) {
                        //$id = Yii::app()->db->getLastInsertID();           

                    } else { 
                        Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ1';
                        return false;
                    }	
                }
                  $i++;  
            }
            
        }
        if($sum_sub_data>0){
          
            //echo var_dump($this->id_check_list);exit();
            foreach($this->id_sub_check_list as $icround_id)
            {
                if( $icround_id!=""){
                     $sql = "INSERT INTO ictech_mas_icround_checklist_sub (department_id,icround_id,chk,remark,create_date,create_by) ";
                    $sql.= "VALUES(:department_id,:icround_id,:check,:remark,now(),$createby)";
                    $command=yii::app()->db->createCommand($sql);		
                    $command->bindValue(":icround_id", (int)$icround_id);	
                    $command->bindValue(":department_id", (int)$this->id);	
                    $command->bindValue(":check", $this->data_sub_check_list[$j]=="" ? null: $this->data_sub_check_list[$j]);		
                    $command->bindValue(":remark", $this->data_sub_comment_list[$j]);		
                    if($command->execute()) {
                        //$id = Yii::app()->db->getLastInsertID();           

                    } else { 
                        Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ2';
                        return false;
                    }	
                }
                $j++;  
                    
            }
        }
        
        
        
        
        return true;
	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function save_update()
	{
		$i=0;
        $j=0;
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
        $sum_data = $this->id_check_list == '' ? 0 : count($this->id_check_list);  
        $sum_sub_data = $this->id_sub_check_list == '' ? 0 : count($this->id_sub_check_list);  
        if( $sum_data>0){
          
            foreach($this->id_check_list as $icround_id)
            {
                //echo var_dump($this->id_check_list);exit();
                if( $this->data_comment_list[(int)$icround_id]!=""){
                    $sql = " update ictech_mas_icround_checklist set remark=:remark,";
                    $sql.= " update_date=now(),update_by=".$createby;
                    $sql.= " where id=".$this->id_check_list[(int)$icround_id];
                    //echo var_dump($sql);exit();
                    $command=yii::app()->db->createCommand($sql);		
                    $command->bindValue(":remark", $this->data_comment_list[(int)$icround_id]);		
                    if($command->execute()) {
                        //$id = Yii::app()->db->getLastInsertID();           

                    } else { 
                        Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ1'.$icround_id.$this->data_comment_list[(int)$icround_id];
                        return false;
                    }	
                }
                    
            }
        }
        if($sum_sub_data>0){
            foreach($this->id_sub_check_list as $icround_id)
            {
                if( $this->data_sub_comment_list[(int)$icround_id]!=""){
                    $sql = "update ictech_mas_icround_checklist_sub set remark=:remark, ";
                    $sql.= " update_date=now(),update_by=".$createby;
                    $sql.= " where id=".$this->id_sub_check_list[(int)$icround_id];
                    $command=yii::app()->db->createCommand($sql);		
                    $command->bindValue(":remark", $this->data_sub_comment_list[(int)$icround_id]);		
                    if($command->execute()) {
                        //$id = Yii::app()->db->getLastInsertID();           

                    } else { 
                        Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ2';
                        return false;
                    }	
                }
                    
            }
        }
        
        
        
        
        return true;
	}
		
}
