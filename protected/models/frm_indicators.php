<?php

class frm_indicators extends CFormModel
{
	public $id;
	public $code;	
	public $name;	
	public $title_detail;	
    
	public $hai;	
	public $cai;	
	
    public $theme;
    public $unit;
    
    public $data_faq;
    public $data_detail;
    public $data_unit;
    public $chkHai;
    public $faq_sub;
    public $data_number;
    
	public function rules()
	{
		return array(
			array('hai', 'cai', 'id','name','title_detail','code','theme','theme','data_faq','data_detail','data_unit','chkHai', 'faq_sub', 'data_number', 'safe'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	public function save_update_detail(){
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $i=0;
        $sum2 = $this->data_unit == '' ? 0 : count($this->data_unit);
        //echo var_dump($this->theme);exit();
        if($this->theme==1){
            
            $sql =" select count(*) as aa from ictech_mas_indicators where status=1 and create_by=".$createby." and name='".trim($this->name)."' ";
            $sql.=" and id!=".$this->id;
            $data =Yii::app()->db->createCommand($sql)->queryAll();
            foreach($data as $dataitem){
                if ($dataitem['aa']>0){
                    Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
                    return false;
                }
            }	
            
            $sql = " UPDATE ictech_mas_indicators SET name=:name,hai=:hai";
            $sql.= " ,unit=:unit,update_date=now(),update_by=".$createby;
            $sql.= " ,tooltip=:tooltip";
            $sql.= " where id=".$this->id;
            $command=yii::app()->db->createCommand($sql);
            $command->bindValue(":hai", $this->chkHai);	
            $command->bindValue(":tooltip", trim($this->title_detail));	
            $command->bindValue(":name", trim($this->data_faq[$i]));		
            $command->bindValue(":unit", trim($this->data_unit[$i]));		
            if($command->execute()) {
                $sql =" select count(*) as aa from ictech_mas_indicators_hai_cai where status=1 and create_by=".$createby." and indicator_id=".$this->id;
                $data =Yii::app()->db->createCommand($sql)->queryAll();
                foreach($data as $dataitem){
                    if ($dataitem['aa']>0){
                        $sql = "UPDATE ictech_mas_indicators_hai_cai SET ";
                        $sql.= " hai=:hai,cai=:cai,update_date=now(),update_by=".$createby;
                        $sql.= " where create_by=".$createby." and indicator_id=".$this->id;
                        $command=yii::app()->db->createCommand($sql);
                        $command->bindValue(":hai", trim($this->hai));	
                        $command->bindValue(":cai", trim($this->cai));		
                        if($command->execute()) {
                            //$id = Yii::app()->db->getLastInsertID();
                            return true;
                        } else { 
                            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                            return false;
                        }	
                    }else{
                        $sql = "INSERT INTO ictech_mas_indicators_hai_cai ";
                        $sql.= " (indicator_id, hai,cai,create_date,create_by)";
                        $sql.= " VALUES (:indicator_id,:hai,:cai,now(),$createby)";
                        $command=yii::app()->db->createCommand($sql);
                        $command->bindValue(":indicator_id", $this->id);		
                        $command->bindValue(":hai", trim($this->hai));		
                        $command->bindValue(":cai", trim($this->cai));		
                        if($command->execute()) {
                            //$id = Yii::app()->db->getLastInsertID();
                            return true;
                        } else { 
                            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                            return false;
                        }	
                    }
                }	
                
                
            } else { 
                Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }	
              
        }
         if($this->theme==2){
            $sql = " UPDATE ictech_mas_indicators SET ";
            $sql.= " tooltip=:tooltip,update_date=now(),update_by=".$createby;
            $sql.= " where id=".$this->id;
            $command=yii::app()->db->createCommand($sql);
            $command->bindValue(":tooltip", $this->title_detail);			
            if($command->execute()) {
                while($i < $sum2){			
                    if($this->data_unit[$i]!="")
                    { 
                        $sql = "UPDATE ictech_mas_indicators_detail SET ";
                        $sql.= " name=:name,unit=:unit,update_date=now(),update_by=".$createby;
                        $sql.= " where id=".$i;
                        $command=yii::app()->db->createCommand($sql);
                        $command->bindValue(":name", $this->data_faq[$i]);		
                        $command->bindValue(":unit", $this->data_unit[$i]);		
                        if($command->execute()) {
                            //$id = Yii::app()->db->getLastInsertID();
                            //return true;
                        } else { 
                            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                            return false;
                        }	
                    }
                    $i++;
                 }
                return true;
            } else { 
                Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }	
         }
        if($this->theme==4){
            $sql =" select count(*) as aa from ictech_mas_indicators where status=1 and create_by=".$createby." and name='".$this->name."' ";
            $sql.=" and id!=".$this->id;
            $data =Yii::app()->db->createCommand($sql)->queryAll();
            foreach($data as $dataitem){
                if ($dataitem['aa']>0){
                    Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
                    return false;
                }
            }	
            
            $sql = " UPDATE ictech_mas_indicators SET ";
            $sql.= " name=:name,update_date=now(),update_by=".$createby;
            $sql.= " where id=".$this->id;
            $command=yii::app()->db->createCommand($sql);
            $command->bindValue(":name", $this->name);				
            if($command->execute()) {
                //$id = Yii::app()->db->getLastInsertID();
                return true;
            } else { 
                Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }	
              
        }
        //return true;
    }
	public function save_insert_detail()
    {
        $i=0;
        $j=0;
        $k=0;
        $l=0;
        //echo var_dump($this->data_unit);exit();
        $sum1 = $this->data_faq == '' ? 0 : count($this->data_faq);        
        $sum2 = $this->data_unit == '' ? 0 : count($this->data_unit);
        $sum3 = $this->data_detail == '' ? 0 : count($this->data_detail);
        
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
        $sql = "select id from ictech_mas_indicators where create_by=".$createby;
        $sql.= " order by id desc";
        $xxx =Yii::app()->db->createCommand($sql)->queryAll();
        $code = $xxx[0]["id"]+1;       
        
        if($this->theme==1){
             while($i < $sum1){			
                if($this->data_faq[$i]!="")
                { 
                    
                    $sql = "INSERT INTO ictech_mas_indicators (code,hai,name,tooltip,unit,theme,create_date,create_by) VALUES(:code,:hai,:name,:tooltip,:unit,:theme,now(),$createby)";
                    $command=yii::app()->db->createCommand($sql);
                    $command->bindValue(":code", $createby.sprintf("%04d", $code));	
                    $command->bindValue(":hai", $this->chkHai);	
                    $command->bindValue(":theme", $this->theme);	
                    $command->bindValue(":name", $this->data_faq[$i]);		
                    $command->bindValue(":unit", $this->data_unit[$i]);	
                    $command->bindValue(":tooltip", $this->title_detail);	
                    if($command->execute()) {
                        $id = Yii::app()->db->getLastInsertID();                        
                        if($this->chkHai=="1"){
                            $sql ="call spSaveHAIAndCAI (".$id.",'".$this->hai."','".$this->cai."',$createby)" ;
                            $rows =Yii::app()->db->createCommand($sql)->execute();
                        }    
                        $sql = 'call spSaveICCMornitor ('.$id.','.$createby.')' ;
                        $rows =Yii::app()->db->createCommand($sql)->execute();           
			            return true; 
                    } else { 
                        Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                        return false;
                    }	
                }
                $i++;
             }
        }
        if($this->theme==2){
            $sql = "INSERT INTO ictech_mas_indicators (hai,name,tooltip,theme,create_date,create_by) VALUES(:hai,:name,:tooltip,:theme,now(),$createby)";
            $command=yii::app()->db->createCommand($sql);
            $command->bindValue(":hai", $this->chkHai);	
            $command->bindValue(":theme", $this->theme);	
            $command->bindValue(":name", $this->name);
            $command->bindValue(":tooltip", $this->title_detail);
            if($command->execute()) {
                $id = Yii::app()->db->getLastInsertID();  
                $sql = 'call spSaveICCMornitor ('.$id.','.$createby.')' ;
                $rows =Yii::app()->db->createCommand($sql)->execute();           
                //return true; 
                while($i < $sum2){			
                    if($this->data_unit[$i]!="")
                    { 
                        $sql = "INSERT INTO ictech_mas_indicators_detail (indicator_id,name,unit,create_date,create_by) VALUES(:indicator_id,:name,:unit,now(),$createby)";
                        $command=yii::app()->db->createCommand($sql);
                        $command->bindValue(":indicator_id", $id);		
                        $command->bindValue(":name", $this->data_faq[$i]);		
                        $command->bindValue(":unit", $this->data_unit[$i]);		
                        if($command->execute()) {
                            //$id = Yii::app()->db->getLastInsertID();
                            //return true;
                        } else { 
                            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                            return false;
                        }	
                    }
                    $i++;
                 }
                //return true;
            } else { 
                Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }	
        }
        if($this->theme==3){
            $sql = "INSERT INTO ictech_mas_indicators (hai,name,tooltip,theme,create_date,create_by) VALUES(:hai,:name,:tooltip,:theme,now(),$createby)";
            $command=yii::app()->db->createCommand($sql);
            $command->bindValue(":hai", $this->chkHai);	
            $command->bindValue(":theme", $this->theme);	
            $command->bindValue(":name", $this->name);	
            $command->bindValue(":tooltip", $this->title_detail);	
            
            if($command->execute()) {
                $id = Yii::app()->db->getLastInsertID();                
                while($i < $sum2)
                {			
                    if($this->data_unit[$i]!="")
                    { 
                        $sql = "INSERT INTO ictech_mas_indicators_detail (indicator_id,name,unit,create_date,create_by) VALUES(:indicator_id,:name,:unit,now(),$createby)";
                        $command=yii::app()->db->createCommand($sql);
                        $command->bindValue(":indicator_id", $id);		
                        $command->bindValue(":name", $this->data_faq[$i]);		
                        $command->bindValue(":unit", $this->data_unit[$i]);		
                        if($command->execute()) {
                            //$id = Yii::app()->db->getLastInsertID();
                            //return true;
                        } else { 
                            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                            return false;
                        }	
                    }
                    $i++;
                 }
                
                $sql = "INSERT INTO ictech_mas_indicators_question2 (name,question_id,create_date,create_by) ";
                $sql.= "VALUES(:name,:question_id,now(),$createby) ";
                $command=yii::app()->db->createCommand($sql);		
                $command->bindValue(":name", $this->faq_sub);
                $command->bindValue(":question_id", $id);
                if($command->execute()){
                    $id = Yii::app()->db->getLastInsertID();
                    while($j < $sum3){			
                        if($this->data_detail[$j]!="")
                        {   

                            $sql = "INSERT INTO ictech_mas_indicators_question2_detail (question_id,name,item_order,create_date,create_by) ";
                            $sql.= "VALUES(:question_id,:name,:item_order,now(),$createby) ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":question_id", $id);
                            $command->bindValue(":item_order", $this->data_number[$j]);
                            $command->bindValue(":name", $this->data_detail[$j]);
                            if($command->execute()){
                                //$id = Yii::app()->db->getLastInsertID();
                                //Yii::app()->session->remove('id_import');

                            }else{
                                Yii::app()->session['errmsg']='error '.$sql;
                                return false;							
                            }
                        }
                        $j++;
                    }
                 
                 }
                //return true;
            } else { 
                Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }	
        }
       // echo var_dump($this->theme);exit();
        if($this->theme==4){
            
            $sql = "INSERT INTO ictech_mas_indicators (name,theme,create_date,create_by) VALUES(:name,:theme,now(),$createby)";
            $command=yii::app()->db->createCommand($sql);
            $command->bindValue(":name", $this->name);			
            $command->bindValue(":theme", $this->theme);			
            if($command->execute()) {
                $id = Yii::app()->db->getLastInsertID();
                $sql = 'call spSaveICCMornitor ('.$id.','.$createby.')' ;
                $rows =Yii::app()->db->createCommand($sql)->execute(); 
                //return true;
            } else { 
                Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }	
              
        }
        return true;
    }

	public function save_insert()
	{
		//check error
		//เช็คว่ามีข้อมูลหรือไม่
        
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sql ="select count(*) as aa from ictech_mas_indicators where status=1 and create_by=".$createby." and (name='".$this->name."' or code='".$this->code."') ";
        
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem){
			if ($dataitem['aa']>0){
				Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
				return false;
				}
			}		
		
		//save
			
		
		$sql = "INSERT INTO ictech_mas_indicators (code,hai,name,create_date,create_by) VALUES(:code,:hai,:name,now(),$createby)";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":code", $this->code);	
		$command->bindValue(":hai", $this->hai);	
		$command->bindValue(":name", $this->name);		
		if($command->execute()) {
			$id = Yii::app()->db->getLastInsertID();
            $sql = 'call spSaveIndicatorsMonitor ('.$createby.','.$id.')' ;
            $command=yii::app()->db->createCommand($sql);		
            if($command->execute()) {              
                return true;
            } else { 
                Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }		
			return true;
		} else { 
			Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}	

	public function save_update()
	{
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		//save
		$sql ="select count(*) as aa from ictech_mas_indicators where status=1 and create_by=".$createby." and (name='".$this->name."' or code='".$this->code."') and id!='".$this->id."'";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem)
        {
            if ($dataitem['aa']>0){
                Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
                return false;
            }
        }	
        $sql = "update ictech_mas_indicators set code=:code, hai=:hai,name=:name, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);
        //$command1->bindValue(":user_id", addslashes($this->user_id));
        $command->bindValue(":code", $this->code);
        $command->bindValue(":hai", $this->hai);
        $command->bindValue(":name", $this->name);				
        if($command->execute()) {
            return true;
        } else {
            Yii::app()->session['errmsg']='ไม่สามารถบันทึกข้อมูลได้';
            return false;
        }	
	}
	public function save_delete()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		

        $sql = "update ictech_mas_indicators set status=0, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);			
            if($command->execute()) {
                return true;
            } else {
                Yii::app()->session['errmsg']='ไม่สามารถลบข้อมูลได้';
                return false;
        }	
	}
		
}
