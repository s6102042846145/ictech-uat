<?php

class frm_indicatordata extends CFormModel
{
	public $id;
	public $indicator_id;	
	public $question1;	
	public $question2;	
	public $question3;
	public $question4;
	public $answer1;	
	public $answer2;	
	public $answer3;	
	public $answer4;	
    public $save_date;
    public $hai;
    public $cai;
    public $chkhai;
    
            
	public function rules()
	{
		return array(
			array('id','indicator_id', 'question1', 'question2', 'question3','question4', 'answer1', 'answer2', 'answer3', 'answer4', 'save_date', 'safe'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	
	

	public function save_insert()
	{
        $i=0;
        $j=0;
        $k=0;
        $l=0;
        
       //$this->id คือ เทมเพลต
        $theme1 = $this->question1 == '' ? 0 : count($this->question1);
        
        $theme2 = $this->question2 == '' ? 0 : count($this->question2);
        $theme3 = $this->question3 == '' ? 0 : count($this->question3);
        $theme4 = $this->question4 == '' ? 0 : count($this->question4);
        //echo var_dump($this->id);exit();
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $dept = Yii::app()->user->getInfo('department_id');
        if($this->chkhai==1){
            $sql = " select count(*) as aa from ictech_mas_indicators_hai_cai_answer where status=1 ";
            $sql.= " and indicator_id='".$this->indicator_id."' ";
            //echo var_dump($sql);exit();
            $data =Yii::app()->db->createCommand($sql)->queryAll();
            foreach($data as $dataitem)
            {
                if ($dataitem['aa']>0){
                    $sql = " update ictech_mas_indicators_hai_cai_answer set hai=:hai, cai=:cai, "; 
                    $sql.= " update_date=now(), update_by=".$createby;
                    $sql.= " where indicator_id=:indicator_id ";
                    $command=yii::app()->db->createCommand($sql);		
                    $command->bindValue(":indicator_id", $this->indicator_id);
                    $command->bindValue(":hai", $this->hai);
                    $command->bindValue(":cai", $this->cai);
                    if($command->execute()){
                        //return true;
                    }else{
                        Yii::app()->session['errmsg']='error 9'.$sql;
                        return false;							
                    }
                }else{
                    $sql = " INSERT INTO ictech_mas_indicators_hai_cai_answer ";
                    $sql.= " (indicator_id,hai,cai,create_date,create_by) ";
                    $sql.= " VALUES(:indicator_id,:hai,:cai,now(),$createby) ";
                    $command=yii::app()->db->createCommand($sql);		
                    $command->bindValue(":indicator_id", $this->indicator_id);
                    $command->bindValue(":hai", $this->hai);
                    $command->bindValue(":cai", $this->cai);
                    if($command->execute()){

                    }else{
                        Yii::app()->session['errmsg']='error 7'.$sql;
                        return false;							
                    }
                }
            }
        }
        if($this->id==1){
            while($i < $theme1){			
                if($this->question1[$i]!="")
                {   
                    $sql = " select count(*) as aa from ictech_mas_indicators_answer where status=1 ";
                    $sql.= " and department_id=".$dept." and indicator_id='".$this->indicator_id."' ";
                    $sql.= " and  SUBSTRING(save_date, 1, 10)='".$this->save_date."' ";
                    $sql.= " and  question_id='".$this->answer1[$i]."' ";
                    //echo var_dump($sql);exit();
                    $data =Yii::app()->db->createCommand($sql)->queryAll();
                    foreach($data as $dataitem)
                    {
                        if ($dataitem['aa']>0){
                            $sql = " update ictech_mas_indicators_answer set name=:name, update_date=now(), update_by=".$createby;
                            $sql.= " where indicator_id=:indicator_id and question_id=:question_id ";
                            $sql.= " and department_id=".$dept." and SUBSTRING(save_date, 1, 10)='".$this->save_date."' ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":indicator_id", $this->indicator_id);
                            $command->bindValue(":question_id", $this->answer1[$i]);
                            $command->bindValue(":name", $this->question1[$i]);
                            if($command->execute()){

                            }else{
                                Yii::app()->session['errmsg']='error 8'.$sql;
                                return false;							
                            }
                        }else{
                            $sql = "INSERT INTO ictech_mas_indicators_answer (department_id,indicator_id,question_id,name,save_date,create_date,create_by) ";
                            $sql.= "VALUES(:department_id,:indicator_id,:question_id,:name,now(),now(),$createby) ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":indicator_id", $this->indicator_id);
                            $command->bindValue(":question_id", $this->answer1[$i]);
                            $command->bindValue(":name", $this->question1[$i]);
                            $command->bindValue(":department_id", $dept);
                            if($command->execute()){

                            }else{
                                Yii::app()->session['errmsg']='error 7'.$sql;
                                return false;							
                            }
                        }
                    }	


                }
                $i++;
            }
        }
        if($this->id==2){
            while($j < $theme2){			
                if($this->question2[$j]!="")
                {   
                    $sql = " select count(*) as aa from ictech_mas_indicators_answer where status=1 ";
                    $sql.= " and department_id=".$dept." and indicator_id='".$this->indicator_id."' ";
                    $sql.= " and  SUBSTRING(save_date, 1, 10)='".$this->save_date."' ";
                    $sql.= " and  question_id='".$this->answer2[$j]."' ";
                    $data =Yii::app()->db->createCommand($sql)->queryAll();
                    foreach($data as $dataitem)
                    {
                        if ($dataitem['aa']>0){
                            $sql = " update ictech_mas_indicators_answer set name=:name, update_date=now(), update_by=".$createby;
                            $sql.= " where indicator_id=:indicator_id and question_id=:question_id ";
                            $sql.= " and department_id=".$dept." and SUBSTRING(save_date, 1, 10)='".$this->save_date."' ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":indicator_id", $this->indicator_id);
                            $command->bindValue(":question_id", $this->answer2[$j]);
                            $command->bindValue(":name", $this->question2[$j]);
                            if($command->execute()){

                            }else{
                                Yii::app()->session['errmsg']='error 6'.$sql;
                                return false;							
                            }
                        }else{
                            $sql = "INSERT INTO ictech_mas_indicators_answer (department_id,indicator_id,question_id,name,save_date,create_date,create_by) ";
                            $sql.= "VALUES(:department_id,:indicator_id,:question_id,:name,now(),now(),$createby) ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":indicator_id", $this->indicator_id);
                            $command->bindValue(":question_id", $this->answer2[$j]);
                            $command->bindValue(":name", $this->question2[$j]);
                            $command->bindValue(":department_id", $dept);
                            if($command->execute()){

                            }else{
                                Yii::app()->session['errmsg']='error 5'.$sql;
                                return false;							
                            }
                        }
                    }
                }
                $j++;
            }
        }
        if($this->id==3){
            //echo var_dump($this->question3);exit();
             while($k < $theme3){			
                if($this->question3[$k]!="")
                {   
                    $sql = " select count(*) as aa from ictech_mas_indicators_answer where status=1 ";
                    $sql.= " and department_id=".$dept." and indicator_id='".$this->indicator_id."' ";
                    $sql.= " and  SUBSTRING(save_date, 1, 10)='".$this->save_date."' ";
                    $sql.= " and  question_id='".$this->answer3[$k]."' ";
                    $data =Yii::app()->db->createCommand($sql)->queryAll();
                    foreach($data as $dataitem)
                    {
                        if ($dataitem['aa']>0){
                            $sql = " update ictech_mas_indicators_answer set name=:name , update_date=now(), update_by=".$createby;
                            $sql.= " where indicator_id=:indicator_id and question_id=:question_id ";
                            $sql.= " and department_id=".$dept." and SUBSTRING(save_date, 1, 10)='".$this->save_date."' ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":indicator_id", $this->indicator_id);
                            $command->bindValue(":question_id", $this->answer3[$k]);
                            $command->bindValue(":name", $this->question3[$k]);
                            if($command->execute()){

                            }else{
                                Yii::app()->session['errmsg']='error 1'.$sql;
                                return false;							
                            }
                        }else{
                            $sql = "INSERT INTO ictech_mas_indicators_answer (department_id,indicator_id,question_id,name,save_date,create_date,create_by) ";
                            $sql.= "VALUES(:department_id,:indicator_id,:question_id,:name,now(),now(),$createby) ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":indicator_id", $this->indicator_id);
                            $command->bindValue(":question_id", $this->answer3[$k]);
                            $command->bindValue(":name", $this->question3[$k]);
                            $command->bindValue(":department_id", $dept);
                            if($command->execute()){

                            }else{
                                Yii::app()->session['errmsg']='error 4'.$sql;
                                return false;							
                            }
                        }
                    }
                }
                $k++;
            }        
            //echo var_dump($theme4);exit();
            while($l < $theme4){			
                //echo var_dump($this->answer4);exit();
                if($this->question4[$l]!="")
                {   
                    $sql = " select count(*) as aa from ictech_mas_indicators_answer2 where status=1 ";
                    $sql.= " and department_id=".$dept." and indicator_id='".$this->indicator_id."' ";
                    $sql.= " and  SUBSTRING(save_date, 1, 10)='".$this->save_date."' ";
                    $sql.= " and  question_id='".$this->question4[$l]."'  ";
                    $data =Yii::app()->db->createCommand($sql)->queryAll();
                    foreach($data as $dataitem)
                    {
                        if ($dataitem['aa']>0){
                            $sql = " update ictech_mas_indicators_answer2 set name=:name, update_date=now(), update_by=".$createby;
                            $sql.= " where indicator_id=:indicator_id and question_id=:question_id ";
                            $sql.= " and department_id=".$dept." and SUBSTRING(save_date, 1, 10)='".$this->save_date."' ";
                            //echo var_dump($sql.'-'.$l.$this->indicator_id.'-'.$this->question4[$l].'-'.$this->answer4[$l]);//exit();
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":indicator_id", $this->indicator_id);
                            $command->bindValue(":question_id", $this->question4[$l]);
                            $command->bindValue(":name", $this->answer4[$l]);
                            if($command->execute()){
                                
                            }else{
                                Yii::app()->session['errmsg']='error 2'.$sql;
                                return false;							
                            }
                        }else{
                            
                            $sql = "INSERT INTO ictech_mas_indicators_answer2 (department_id,indicator_id,question_id,name,save_date,create_date,create_by) ";
                            $sql.= "VALUES(:department_id,:indicator_id,:question_id,:name,now(),now(),$createby) ";
                            //echo var_dump($sql.$this->indicator_id.'-'.$this->question4[$l].'-'.$this->answer4[$l]);//exit();
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":indicator_id", $this->indicator_id);
                            $command->bindValue(":question_id", $this->question4[$l]);
                            $command->bindValue(":name", $this->answer4[$l]);
                            $command->bindValue(":department_id", $dept);
                            if($command->execute()){

                            }else{
                                Yii::app()->session['errmsg']='error 3'.$sql;
                                return false;							
                            }
                        }
                    }
                }
                $l++;
            }
        }
        return true;
        
	}	

	public function save_update()
	{
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		//save
		$sql ="select count(*) as aa from ictech_mas_indicators where status=1 and create_by=".$createby." and (name='".$this->name."' or code='".$this->code."') and id!='".$this->id."'";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem)
        {
            if ($dataitem['aa']>0){
                Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
                return false;
            }
        }	
        $sql = "update ictech_mas_indicators set code=:code, hai=:hai,name=:name, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);
        //$command1->bindValue(":user_id", addslashes($this->user_id));
        $command->bindValue(":code", $this->code);
        $command->bindValue(":hai", $this->hai);
        $command->bindValue(":name", $this->name);				
        if($command->execute()) {
            return true;
        } else {
            Yii::app()->session['errmsg']='ไม่สามารถบันทึกข้อมูลได้';
            return false;
        }	
	}
	public function save_delete()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		

        $sql = "update ictech_mas_indicators set status=0, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);			
            if($command->execute()) {
                return true;
            } else {
                Yii::app()->session['errmsg']='ไม่สามารถลบข้อมูลได้';
                return false;
        }	
	}
		
}
