<?php

class frm_login extends CFormModel
{
	public $username;
	public $password;
	private $_identity;

	public function rules()
	{
		return array(
			array('username, password', 'safe'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}

	public function login()
	{
		
		if($this->username==''){
			Yii::app()->session['errmsg_login']='กรุณาระบุรหัสมาชิก';	
			return false;			
		}
		
		if($this->password==''){
			Yii::app()->session['errmsg_login']='กรุณาระบุรหัสผ่าน';	
			return false;			
		}		
		
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			//$duration=3600*24*Yii::app()->params['prg_ctrl']['authCookieDuration'];
			//Yii::app()->user->login($this->_identity,$duration);
			Yii::app()->user->login($this->_identity);
			return true;
		} else {
			if($this->_identity->errorCode == UserIdentity::ERROR_USERNAME_INVALID) {
				Yii::app()->session['errmsg_login2']='รหัสมาชิก ไม่ถูกต้อง';
				
			} elseif($this->_identity->errorCode == UserIdentity::ERROR_PASSWORD_INVALID) {
				 Yii::app()->session['errmsg_login2']='รหัสผ่าน ไม่ถูกต้อง';

			} elseif($this->_identity->errorCode == UserIdentity::ERROR_USERNAME_NOTLDAP) {
				Yii::app()->session['errmsg_login2']='รหัสมาชิกหรือรหัสผ่าน ไม่ถูกต้อง';

				 
			} elseif($this->_identity->errorCode == UserIdentity::ERROR_USERNAME_NOTADMIN) {
				Yii::app()->session['errmsg_login2']='คุณไม่มีสิทธิ์ในการเข้าใช้งานระบบ';
				
			} else {
				Yii::app()->session['errmsg_login2']='Invalid Exception.';
			}			
			Yii::app()->CommonFnc->log_login('Error',Yii::app()->session['errmsg_login2'],$this->username);
			return false;
		}
			
	}


		
}
