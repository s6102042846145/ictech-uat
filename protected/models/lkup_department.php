<?php

class lkup_department extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'mas_bank';
	}

    public function attributeLabels() {
        return array(
        );
    }
    /*
    public function getIndicators($id= null)
    {
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $strSql = " from ictech_mas_department_monitor a ";
        $strSql.=" RIGHT  JOIN ictech_mas_indicators b on b.id=a.indicators_id and b.create_by=".$createby." and a.department_id='".$id."' " ;
        $strSql.=" where b.`status`!=0 ";
        
        $count=Yii::app()->db->createCommand('select count(*) '.$strSql)->queryScalar();
        $sql=" select b.code,b.name,b.id,case when IFNULL(a.indicators_id,0)=0 then '' else 'checked' end checked ".$strSql;	
        //echo var_dump($sql);exit();
		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 'id', 'code', 'name'
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
    */
    public static function Searchdatalist($id = null){
         
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql = 'call spGetDepartmentUseICRound ('.$id.','.$createby.')' ;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
    }
    public static function Search($keyword = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	        
		$sqlCon=" and create_by=".$createby;
        
        $count=Yii::app()->db->createCommand('select count(*) from ictech_mas_department where status!=0 '.$sqlCon)->queryScalar();	
        $sql=" select * ";
        $sql.=" from ictech_mas_indicators_question where status!=0 ".$sqlCon;	

		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
    
    public static function getIndicators($id= null)
    {
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="";
        if($id==null){
            $sql.=" select id,name from ictech_mas_indicators where status!=0 and create_by=".$createby ;
        }else{
            /*
            $sql.=" select a.department_id,b.code,b.name,b.id,case when IFNULL(a.status,0)=0 then '' else 'checked' end checked ";
            $sql.=" from ictech_mas_department_monitor a ";
            $sql.=" RIGHT JOIN ictech_mas_indicators b on b.id=a.indicators_id ";
            $sql.=" left join ictech_mas_indicators c on a.indicators_id=c.id ";
            $sql.=" where c.`status`!=0  ";
            $sql.=" and b.create_by=".$createby ;
            $sql.=" and a.department_id=".$id ;
            */
            $sql.=" select id,name,checked from (
            select a.id,a.name 
            from ictech_mas_indicators a
            where a.status!=0 
            and a.create_by=".$createby." )aa
            left join (SELECT case when IFNULL(status,0)=0 then '' else 'checked' end checked,indicators_id  from ictech_mas_department_monitor where department_id=".$id.") bb on aa.id=bb.indicators_id ";

        }
        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }	
    
    public static function getDataIC($id,$department_id)
	{
        //$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		//$sqlCon=" and create_by=".$createby;	
        $sql="select * from ictech_mas_icround where status!=0 and id='".$id."' and department_id='".$department_id."'";	
        //echo var_dump($sql);exit();
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    public static function getDataICDetail($id)
	{
        $sql="select * from ictech_mas_icround_detail where status!=0 and icround_id='".$id."' ";	
        //echo var_dump($sql);exit();
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    public static function getSearch()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby;	
        $sql="select id,code,name from ictech_mas_department where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    public static function getICRound($id=null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby;	
        $sql="select * from ictech_mas_icround where status!=0 and department_id='".$id."'  ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		//ictech_mas_icround
	}
    
    
    public static function getDepartment($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_department where id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
	public static function getHai($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators_detail_hai_cai where indicator_id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getHaiAndCai($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select hai from ictech_mas_indicators where id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    
    public static function SearchFAQ($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	        
		$sqlCon=" and create_by=".$createby;
        
        $count=Yii::app()->db->createCommand('select count(*) from ictech_mas_indicators_question where status!=0 '.$sqlCon)->queryScalar();	
        $sql=" select id,name,unit ";
        $sql.=" ,indicator_id, theme ";
        $sql.=" from ictech_mas_indicators_question where status!=0 and indicator_id=".$id.$sqlCon;	

		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
    public static function SearchTitle()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	        
		$sqlCon=" and create_by=".$createby;
        
        $count=Yii::app()->db->createCommand('select count(*) from ictech_mas_indicators_detail_title where status!=0 '.$sqlCon)->queryScalar();	
        $sql="select id,name,detail from ictech_mas_indicators_detail_title where status!=0 ".$sqlCon;	

		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 //
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
    
    public static function getTitle($id = null)
	{
        $strSQL = "";
        if($id!=0){
            $strSQL .="and id=".$id;
        }
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators_detail_title where status!=0 ".$strSQL." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getUnit($id = null)
	{
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators_question where status!=0 and id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    
    public static function getQuesttionTitle($id = null)
	{
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators_question where status!=0 and id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getQuesttionTitle2($id = null)
	{
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators_question2 where status!=0 and question_id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getQuesttion($id = null)
	{
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql = " select * from ictech_mas_indicators_question_detail ";
        $sql.= " where status!=0 and question_id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getQuesttion2($id = null)
	{
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql = " select * from ictech_mas_indicators_question2_detail ";
        $sql.= " where status!=0 and question_id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
}	

