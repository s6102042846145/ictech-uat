<?php

class frm_video extends CFormModel
{
	public $id;	
	public $name;	
	public $detail;	
    
	public function rules()
	{
		return array(
			array('id','name', 'detail'),				
		);
	}
	public function save_insert()
	{
		//check error
		//เช็คว่ามีข้อมูลหรือไม่
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sql ="select count(*) as aa from ictech_tran_video where status=1 and create_by=".$createby." and name='".$this->name."' ";
        
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem){
			if ($dataitem['aa']>0){
				Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
				return false;
				}
			}		
		
		//save
		$sql = "INSERT INTO ictech_tran_video (name,detail,create_date,create_by) VALUES(:name,:detail,now(),$createby)";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":name", $this->name);	
		$command->bindValue(":detail", $this->detail);		
		if($command->execute()) {
            $id = Yii::app()->db->getLastInsertID();           
			return true;            
		} else { 
			Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}	

	public function save_update()
	{
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		//save
		$sql ="select count(*) as aa from ictech_tran_video where status=1 and create_by=".$createby." and name='".$this->name."' and id!='".$this->id."'";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem)
        {
            if ($dataitem['aa']>0){
                Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
                return false;
            }
        }	
        $sql = "update ictech_tran_video set name=:name,detail=:detail, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);
        $command->bindValue(":name", $this->name);	
		$command->bindValue(":detail", $this->detail);				
        if($command->execute()) {
            return true;
        } else {
            Yii::app()->session['errmsg']='ไม่สามารถบันทึกข้อมูลได้';
            return false;
        }	
	}
	public function save_delete()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		

        $sql = "update ictech_tran_video set status=0, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);			
            if($command->execute()) {
                return true;
            } else {
                Yii::app()->session['errmsg']='ไม่สามารถลบข้อมูลได้';
                return false;
        }	
	}
		
}

