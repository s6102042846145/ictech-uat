<?php

class frm_department extends CFormModel
{
	public $id;
	public $code;	
	public $name;	
	public $department_id;	
	public $indicator_id;	
	public $indicators_id;	
	public $status;	
    
	public $hai;	
	public $cai;	
	public $title;	
	public $group_id;	
    
    
    
    public $theme;
    public $faq_sub;
    public $title_detail;
    
    public $data_id;
    public $data_faq;
    public $data_unit;
    public $data_number;
    public $data_detail;
	
	public function rules()
	{
		return array(
			array('code', 'id','name', 'department_id', 'indicators_id','data_id','data_faq', 'status', 'title', 'group_id', 'safe'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	public function save_insert_icround()
	{   
        $i=0;        
        $sum1 = $this->data_faq == '' ? 0 : count($this->data_faq);   
        //echo var_dump($sum1);exit();
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
        
        $group_id=0;
        $sql = " select group_id from ictech_mas_icround where status=1 and department_id=".$this->department_id;   
        $sql.= " GROUP BY group_id";
        $data =Yii::app()->db->createCommand($sql)->queryAll();
        $group_id=count($data)+1;
        
         while($i < $sum1)
         {			
            if($this->data_faq[$i]!="")
            { 
                $sql = " select * from ictech_mas_icround  ";
                $sql.= " where status=1 and department_id=".$this->department_id;   
                $sql.= " and title='".trim($this->name)."' limit 1";
                //$sql.= " and title='".trim($this->data_faq[$i])."'";
                $data =Yii::app()->db->createCommand($sql)->queryAll();
                if(count($data)>0){
                    foreach($data as $dataitem)
                    {
                        $sql = "INSERT INTO ictech_mas_icround (department_id,group_id,title,name,create_date,create_by) ";
                        $sql.= "VALUES(:department_id,:group_id,:title,:name,now(),$createby)";
                        $command=yii::app()->db->createCommand($sql);		
                        $command->bindValue(":group_id", $dataitem['group_id']);	
                        $command->bindValue(":department_id", $dataitem['department_id']);	
                        $command->bindValue(":title", $dataitem['title']);		
                        $command->bindValue(":name", trim($this->data_faq[$i]));		
                        if($command->execute()) {
                            //$id = Yii::app()->db->getLastInsertID();           

                        } else { 
                            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                            return false;
                        }	
                    }		
                }else{
                    $sql = "INSERT INTO ictech_mas_icround (department_id,group_id,title,name,create_date,create_by) ";
                    $sql.= "VALUES(:department_id,:group_id,:title,:name,now(),$createby)";
                    $command=yii::app()->db->createCommand($sql);		
                    $command->bindValue(":group_id", $group_id);	
                    $command->bindValue(":department_id", $this->department_id);	
                    $command->bindValue(":title", trim($this->name));		
                    $command->bindValue(":name", trim($this->data_faq[$i]));		
                    if($command->execute()) {
                        //$id = Yii::app()->db->getLastInsertID();           

                    } else { 
                        Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                        return false;
                    }	
                }
            }
            $i++;
         }
        return true;  
	}	
	public function save_insert_icround2()
	{   
        $i=0;        
        $sum1 = $this->data_faq == '' ? 0 : count($this->data_faq);  
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        
        //echo var_dump($this->data_id[0]);exit();
        
        $sql = " UPDATE ictech_mas_icround set title=:title,update_date=now(),update_by=".$createby;
        $sql.= " WHERE group_id=".$this->group_id;
        $command=yii::app()->db->createCommand($sql);
        $command->bindValue(":title", $this->title);	
        if($command->execute()) {
            //$id = Yii::app()->db->getLastInsertID();           
            $sql = " UPDATE ictech_mas_icround set name=:name,flx=:flx,create_date=now(),update_by=".$createby;
            $sql.= " WHERE id=".$this->id;
            $command=yii::app()->db->createCommand($sql);		
            $command->bindValue(":name", $this->name);		
            $command->bindValue(":flx", 1);		
            if($command->execute()) {
                //$id = Yii::app()->db->getLastInsertID();           

            } else { 
                Yii::app()->session['errmsg']=$this->name.$sql.'1 เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }	
        } else { 
            Yii::app()->session['errmsg']='2 เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
            return false;
        }	
		 while($i < $sum1)
         {			
            if($this->data_faq[$i]!="")
            { 
                $sql ="select count(*) as aa from ictech_mas_icround_detail where status=1 and id=".$this->data_id[$i];        
                $data =Yii::app()->db->createCommand($sql)->queryAll();
                foreach($data as $dataitem)
                {
                    if ($dataitem['aa']>0){
                        $sql = " UPDATE ictech_mas_icround_detail set name=:name,update_date=now(),update_by=:update_by "; 
                        $sql.= " WHERE id=".$this->data_id[$i];
                        $command=yii::app()->db->createCommand($sql);		
                        $command->bindValue(":update_by", $createby);		
                        $command->bindValue(":name", $this->data_faq[$i]);				
                        if($command->execute()) {
                            //$id = Yii::app()->db->getLastInsertID();           

                        } else { 
                            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                            return false;
                        }	
                    }else{
                        $sql = "INSERT INTO ictech_mas_icround_detail (icround_id,department_id,name,create_date,create_by) ";
                        $sql.= "VALUES(:icround_id,:department_id,:name,now(),$createby)";
                        $command=yii::app()->db->createCommand($sql);		
                        $command->bindValue(":icround_id", $this->id);	
                        $command->bindValue(":name", $this->data_faq[$i]);	
                        $command->bindValue(":department_id", $this->department_id);	
                        if($command->execute()) {
                            //$id = Yii::app()->db->getLastInsertID();           

                        } else { 
                            Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                            return false;
                        }	
                    }
                }
            }
            $i++;
         }
        return true; 
	}	
    

	public function save_insert()
	{
		//check error
		//เช็คว่ามีข้อมูลหรือไม่
        
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sql ="select count(*) as aa from ictech_mas_department where status=1 and create_by=".$createby." and name='".$this->name."' ";
        
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem){
			if ($dataitem['aa']>0){
				Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
				return false;
				}
			}		
		
		//save
			
		
		$sql = "INSERT INTO ictech_mas_department (code,name,create_date,create_by) VALUES(:code,:name,now(),$createby)";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":code", $this->code);	
		$command->bindValue(":name", $this->name);		
		if($command->execute()) {
            $id = Yii::app()->db->getLastInsertID();
            $sql = 'call spSaveDepartmentMonitor ('.$createby.','.$id.')' ;
            $rows =Yii::app()->db->createCommand($sql)->execute();           
			return true;            
		} else { 
			Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}	

	public function save_update()
	{
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		//save
		$sql ="select count(*) as aa from ictech_mas_department where status=1 and create_by=".$createby." and (name='".$this->name."' or code='".$this->code."')  and id!='".$this->id."'";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem)
        {
            if ($dataitem['aa']>0){
                Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
                return false;
            }
        }	
        $sql = "update ictech_mas_department set code=:code,name=:name, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);
        //$command1->bindValue(":user_id", addslashes($this->user_id));
        $command->bindValue(":code", $this->code);
        $command->bindValue(":name", $this->name);				
        if($command->execute()) {
            return true;
        } else {
            Yii::app()->session['errmsg']='ไม่สามารถบันทึกข้อมูลได้';
            return false;
        }	
	}
	public function save_delete()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		

        $sql = "update ictech_mas_department set status=0, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);			
            if($command->execute()) {
                return true;
            } else {
                Yii::app()->session['errmsg']='ไม่สามารถลบข้อมูลได้';
                return false;
        }	
	}
    
    public function save_hai()
	{
		//$department_id = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('department_id'):0;	
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		$sql = "INSERT INTO ictech_mas_indicators_detail_hai_cai ";
        $sql.= "(indicator_id,department_id,hai,cai,create_date,create_by) ";
        $sql.= "VALUES(:indicator_id,:department_id,:hai,:cai,now(),$createby)";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":indicator_id", $this->indicator_id);	
		//$command->bindValue(":department_id", $department_id);	
		$command->bindValue(":hai", $this->hai);	
		$command->bindValue(":cai", $this->cai);		
		if($command->execute()) {            
			return true;            
		} else { 
			Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}	
    public function update_hai()
	{
		//$department_id = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('department_id'):0;	
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		$sql = "update ictech_mas_indicators_detail_hai_cai ";
        $sql.= "set indicator_id=:indicator_id ,hai=:hai ,cai=:cai ,update_date=now(),update_by=$createby ";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":indicator_id", $this->id);	
		//$command->bindValue(":department_id", $this->department_id);	
		$command->bindValue(":hai", $this->hai);	
		$command->bindValue(":cai", $this->cai);		
		if($command->execute()) {            
			return true;            
		} else { 
			Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}	
    
    
    public function save_indicators()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		//save
		$sql =" select count(*) as aa from ictech_mas_department_monitor where create_by=".$createby;
        $sql.=" and indicators_id='".$this->indicators_id."' and department_id='".$this->department_id."' ";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem)
        {
            if ($dataitem['aa']>0){
                $sql = " update ictech_mas_department_monitor set status=:status, ";
                $sql.= " update_date=now(), update_by=$createby where create_by=".$createby;
                $sql.= " and indicators_id='".$this->indicators_id."' and department_id='".$this->department_id."'";
                $command=yii::app()->db->createCommand($sql);
                $command->bindValue(":status", $this->status);		
                if($command->execute()) {
                    return true;
                } else {
                    Yii::app()->session['errmsg']='ไม่สามารถบันทึกข้อมูลได้';
                    return false;
                }	
            }else{
                $sql = "INSERT INTO ictech_mas_department_monitor (department_id,indicators_id,create_date,create_by) VALUES(:department_id,:indicators_id,now(),$createby)";
                $command=yii::app()->db->createCommand($sql);		
                $command->bindValue(":department_id", $this->department_id);	
                $command->bindValue(":indicators_id", $this->indicators_id);		
                if($command->execute()) {
                    $id = Yii::app()->db->getLastInsertID();
                    return true;
                } else { 
                    Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                    return false;
                }		
            }
        }	
        
	}
    public function save_insert_detail()
	{
        $i=0;
        $j=0;
        $sum1 = count($this->data_faq);
        $sum2 = count($this->data_detail);
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        
        if($this->theme!=1){
            $sql = "INSERT INTO ictech_mas_indicators_question ";
            $sql.= "(name,indicator_id,tooltip,theme,create_date,create_by) ";
            $sql.= "VALUES(:name,:indicator_id,:tooltip,:theme,now(),$createby) ";
            $command=yii::app()->db->createCommand($sql);		
            $command->bindValue(":name", $this->name);
            $command->bindValue(":indicator_id", $this->indicator_id);
            $command->bindValue(":tooltip", $this->title_detail);
            $command->bindValue(":theme", $this->theme);
            if($command->execute()){
                $id = Yii::app()->db->getLastInsertID();            
                while($i < $sum1){			
                    if($this->data_faq[$i]!="")
                    {   
                        $sql = "INSERT INTO ictech_mas_indicators_question_detail (question_id,name,unit,create_date,create_by) ";
                        $sql.= "VALUES(:question_id,:name,:unit,now(),$createby) ";
                        $command=yii::app()->db->createCommand($sql);		
                        $command->bindValue(":question_id", $id);
                        $command->bindValue(":name", $this->data_faq[$i]);
                        $command->bindValue(":unit", $this->data_unit[$i]);
                        if($command->execute()){
                            //$id = Yii::app()->db->getLastInsertID();
                            //Yii::app()->session->remove('id_import');

                        }else{
                            Yii::app()->session['errmsg']='error '.$sql;
                            return false;							
                        }
                    }
                    $i++;
                }
            }else{
                Yii::app()->session['errmsg']='error '.$sql;
                return false;							
            }
            if($this->theme==3){
                $sql = "INSERT INTO ictech_mas_indicators_question2 (name,question_id,create_date,create_by) ";
                $sql.= "VALUES(:name,:question_id,now(),$createby) ";
                $command=yii::app()->db->createCommand($sql);		
                $command->bindValue(":name", $this->faq_sub);
                $command->bindValue(":question_id", $id);
                if($command->execute()){
                    $id = Yii::app()->db->getLastInsertID();
                    while($j < $sum2){			
                        if($this->data_detail[$j]!="")
                        {   

                            $sql = "INSERT INTO ictech_mas_indicators_question2_detail (question_id,name,item_order,create_date,create_by) ";
                            $sql.= "VALUES(:question_id,:name,:item_order,now(),$createby) ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":question_id", $id);
                            $command->bindValue(":item_order", $this->data_number[$j]);
                            $command->bindValue(":name", $this->data_detail[$j]);
                            if($command->execute()){
                                //$id = Yii::app()->db->getLastInsertID();
                                //Yii::app()->session->remove('id_import');

                            }else{
                                Yii::app()->session['errmsg']='error '.$sql;
                                return false;							
                            }
                        }
                        $j++;
                    }
                }else{
                    Yii::app()->session['errmsg']='error '.$sql;
                    return false;							
                }
            }
        }else{
            $sql = "INSERT INTO ictech_mas_indicators_question ";
            $sql.= "(name,indicator_id,tooltip,theme,create_date,create_by) ";
            $sql.= "VALUES(:name,:indicator_id,:tooltip,:theme,now(),$createby) ";
            $command=yii::app()->db->createCommand($sql);		
            $command->bindValue(":name", "ไม่มีหัวข้อ");
            $command->bindValue(":indicator_id", $this->indicator_id);
            $command->bindValue(":tooltip", $this->title_detail);
            $command->bindValue(":theme", $this->theme);
            if($command->execute()){
                $id = Yii::app()->db->getLastInsertID();            
                while($i < $sum1){			
                    if($this->data_faq[$i]!="")
                    {   
                        $sql = "INSERT INTO ictech_mas_indicators_question_detail (question_id,name,unit,create_date,create_by) ";
                        $sql.= "VALUES(:question_id,:name,:unit,now(),$createby) ";
                        $command=yii::app()->db->createCommand($sql);		
                        $command->bindValue(":question_id", $id);
                        $command->bindValue(":name", $this->data_faq[$i]);
                        $command->bindValue(":unit", $this->data_unit[$i]);
                        if($command->execute()){
                            //$id = Yii::app()->db->getLastInsertID();
                            //Yii::app()->session->remove('id_import');

                        }else{
                            Yii::app()->session['errmsg']='error '.$sql;
                            return false;							
                        }
                    }
                    $i++;
                }
            }else{
                Yii::app()->session['errmsg']='error '.$sql;
                return false;							
            }
        }
        return true;
        
    }
    public function save_update_detail()
	{
        $i=0;
        $j=0;
        $sum1 = count($this->data_faq);
        $sum2 = count($this->data_detail);
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        
        if($this->theme!=1){
            $sql = " UPDATE ictech_mas_indicators_question set ";
            $sql.= " name=:name,tooltip=:tooltip,update_date=now(),update_by=:update_by where id='".$this->id."'";
            $command=yii::app()->db->createCommand($sql);		
            $command->bindValue(":name", $this->name);
            $command->bindValue(":tooltip", $this->title_detail);
            $command->bindValue(":update_by", $createby);
            if($command->execute()){
                //$id = Yii::app()->db->getLastInsertID();  
                $sql ="delete from ictech_mas_indicators_question_detail where question_id='".$this->id."'";
                $command=yii::app()->db->createCommand($sql);
                if($command->execute()){
                    //$id = Yii::app()->db->getLastInsertID();
                    //Yii::app()->session->remove('id_import');

                }else{
                    Yii::app()->session['errmsg']='error '.$sql;
                    return false;							
                }
                while($i < $sum1){			
                    if($this->data_faq[$i]!="")
                    {   
                        $sql = "INSERT INTO ictech_mas_indicators_question_detail (question_id,name,unit,create_date,create_by) ";
                        $sql.= "VALUES(:question_id,:name,:unit,now(),$createby) ";
                        $command=yii::app()->db->createCommand($sql);		
                        $command->bindValue(":question_id", $this->id);
                        $command->bindValue(":name", $this->data_faq[$i]);
                        $command->bindValue(":unit", $this->data_unit[$i]);
                        if($command->execute()){
                            //$id = Yii::app()->db->getLastInsertID();
                            //Yii::app()->session->remove('id_import');

                        }else{
                            Yii::app()->session['errmsg']='error '.$sql;
                            return false;							
                        }
                    }
                    $i++;
                }
            }else{
                Yii::app()->session['errmsg']='error '.$sql;
                return false;							
            }
            if($this->theme==3){
                $sql = " UPDATE ictech_mas_indicators_question2 SET ";
                $sql.= " name=:name,update_date=now(),update_by=:update_by ";
                $sql.= " where question_id=:question_id ";
                $command=yii::app()->db->createCommand($sql);		
                $command->bindValue(":name", $this->faq_sub);
                $command->bindValue(":question_id", $this->id);
                $command->bindValue(":update_by", $createby);
                if($command->execute()){
                    //$id = Yii::app()->db->getLastInsertID();
                    $sql ="delete from ictech_mas_indicators_question2_detail where question_id='".$this->id."'";
                    $command=yii::app()->db->createCommand($sql);
                    if($command->execute()){}
                    while($j < $sum2){			
                        if($this->data_detail[$j]!="")
                        {   

                            $sql = "INSERT INTO ictech_mas_indicators_question2_detail (question_id,name,item_order,create_date,create_by) ";
                            $sql.= "VALUES(:question_id,:name,:item_order,now(),$createby) ";
                            $command=yii::app()->db->createCommand($sql);		
                            $command->bindValue(":question_id", $this->id);
                            $command->bindValue(":item_order", $this->data_number[$j]);
                            $command->bindValue(":name", $this->data_detail[$j]);
                            if($command->execute()){
                                //$id = Yii::app()->db->getLastInsertID();
                                //Yii::app()->session->remove('id_import');

                            }else{
                                Yii::app()->session['errmsg']='error '.$sql;
                                return false;							
                            }
                        }
                        $j++;
                    }
                }else{
                    Yii::app()->session['errmsg']='error '.$sql;
                    return false;							
                }
            }
        }else{
                $sql ="delete from ictech_mas_indicators_question_detail where question_id='".$this->id."'";
                $command=yii::app()->db->createCommand($sql);
                if($command->execute()){
                    //$id = Yii::app()->db->getLastInsertID();
                    //Yii::app()->session->remove('id_import');

                }else{
                    Yii::app()->session['errmsg']='error '.$sql;
                    return false;							
                }
            
                while($i < $sum1){			
                    if($this->data_faq[$i]!="")
                    {   
                        $sql = "INSERT INTO ictech_mas_indicators_question_detail (question_id,name,unit,create_date,create_by) ";
                        $sql.= "VALUES(:question_id,:name,:unit,now(),$createby) ";
                        $command=yii::app()->db->createCommand($sql);		
                        $command->bindValue(":question_id", $this->id);
                        $command->bindValue(":name", $this->data_faq[$i]);
                        $command->bindValue(":unit", $this->data_unit[$i]);
                        if($command->execute()){
                            //$id = Yii::app()->db->getLastInsertID();
                            //Yii::app()->session->remove('id_import');

                        }else{
                            Yii::app()->session['errmsg']='error '.$sql;
                            return false;							
                        }
                    }
                    $i++;
                }
           
        }
        return true;
        
    }
		
}
