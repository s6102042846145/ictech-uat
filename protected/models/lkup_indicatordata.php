<?php
class lkup_indicatordata extends CActiveRecord

{

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}



	public function tableName()

	{

		return 'mas_user';

	}



    public function attributeLabels() {

        return array(

        );

    }
    public static function getDropdownDisease(){
        $id = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('create_by'):0;
        $sql = 'call spDropdownDisease ('.$id.')' ;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
    }
    public static function getDropdownSpecimen(){
        $id = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('create_by'):0;
        $sql = 'call spDropdownSpecimen ('.$id.')' ;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
    }
    public static function Search($keyword = null)
	{
        date_default_timezone_set("Asia/Bangkok");
        $date_now=date("Y-m-d");
        if($keyword==null){
            $keyword=$date_now;            
            $strSQL=" left ";
        }else if($keyword==$date_now){
            $keyword=$date_now;            
            $strSQL=" left ";
        }else if($keyword>=date('Y-m-d', strtotime('-3 day', strtotime($date_now))) && $keyword<=$date_now){
            //$keyword=date("Y-m-d");            
            $strSQL=" left ";
        }else{
            $strSQL=" ";
        }
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	 
        $department_id = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('department_id'):0;
		
        
        $count=Yii::app()->db->createCommand("select count(*) 
from ictech_mas_department_monitor a 
".$strSQL." join (SELECT * from ictech_mas_indicators_answer where department_id=".$department_id." and save_date like '".$keyword."%' GROUP BY indicator_id) b on a.indicators_id=b.indicator_id 
join ictech_mas_indicators c on a.indicators_id=c.id 
where a.status=1 and a.department_id=".$department_id." 
and indicators_id in (SELECT id from ictech_mas_indicators)")->queryScalar();	

        $sql=" SELECT a.indicators_id id,c.theme ";
        //$sql.=" ,case when b.create_date!='' then CONCAT(DATE_FORMAT(b.create_date, '%d' ),'/', DATE_FORMAT(b.create_date, '%m' ),'/', DATE_FORMAT(b.create_date, '%Y' ) +543 ) ELSE CONCAT(DATE_FORMAT(now(), '%d' ),'/', DATE_FORMAT(now(), '%m' ),'/', DATE_FORMAT(now(), '%Y' ) +543 )  end create_date ";
        $sql.=", '".$keyword."' create_date ";
        $sql.=",case when b.create_date!='' then 1 ELSE 0 end item_order 
         ,c.name indicator_name,b.create_date create_status 
from ictech_mas_department_monitor a 
".$strSQL." join (SELECT * from ictech_mas_indicators_answer where department_id=".$department_id." and save_date like '".$keyword."%' GROUP BY indicator_id) b on a.indicators_id=b.indicator_id 
join ictech_mas_indicators c on a.indicators_id=c.id 
where a.status=1 and a.department_id=".$department_id."  
and indicators_id in (SELECT id from ictech_mas_indicators) 
         ";	
//echo var_dump($sql);exit();
		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
    
    
    public static function getDepartmentMornitors($id = null)
	{
        $department_id = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('department_id'):0;	
        $sql=" select * from ictech_mas_indicators where id = (SELECT indicators_id ";
        $sql.= " from ictech_mas_department_monitor ";
        $sql.= " where status=1 and department_id='".$department_id."' and indicators_id='".$id."'";
        $sql.= " ) ";
        $sql.= " order by theme ";
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    
    //no use
    public static function getDepartmentMornitor()
	{
        $department_id = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('department_id'):0;	
        $sql=" select * from ictech_mas_indicators_question where indicator_id in (SELECT indicators_id ";
        $sql.= " from ictech_mas_department_monitor ";
        $sql.= " where status=1 and department_id='".$department_id."' ";
        $sql.= " ) ";
        $sql.= " order by theme ";
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    //end 
    public static function getQuestionHeadTable($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('create_by'):0;	
        $sql="select DISTINCT name th from ictech_mas_indicators_detail where status=1 and create_by=".$createby." and indicator_id=".$id;	   
        
        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getQuestion($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators where status=1 and id=".$id;	        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getQuestionData($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators_detail where status=1 and indicator_id=".$id;	        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getHAIAndCAI($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('create_by'):0;
        $sql="select * from ictech_mas_indicators_hai_cai where status=1 and indicator_id=".$id;	        
        $sql.=" and create_by=".$createby;	        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getPopupIndicator($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('create_by'):0;
        $sql="select tooltip from ictech_mas_indicators where status=1 and id=".$id;	        
        $sql.=" and create_by=".$createby;	        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
     public static function getQuestion2($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators_question2 where status=1 and question_id=".$id;	
        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function getQuestion2Detail($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators_question2_detail where status=1 and question_id=".$id;	
        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
 
    public static function chkData($id,$save_date)
    {
        $dept = Yii::app()->user->getInfo('department_id');
        //$createby = !Yii::app()->user->isGuest?Yii::app()->user->getInfo('create_by'):0;
        $sql =" select * from ictech_mas_indicators_answer ";
        $sql.=" where status=1 and indicator_id=".$id." and SUBSTRING(create_date, 1, 10)='".$save_date."' " ;	
        $sql.=" and department_id=".$dept ;	
        //echo var_dump($sql);exit();
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }
    public static function chkData2($id=null)
    {
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql =" select * from ictech_mas_indicators_answer2 ";
        $sql.=" where status=1 and indicator_id=".$id  ;	
        //echo var_dump($sql);exit();
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }
    public static function chkData3($id=null)
    {
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql =" select * from ictech_mas_indicators_hai_cai_answer ";
        $sql.=" where status=1 and indicator_id=".$id  ;	
        //echo var_dump($sql);exit();
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    } 
        
        
       
    
    
    
    public static function getData($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators ";	 
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
	
}	

