<?php


class lkup_icround extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'mas_user';
	}

    public function attributeLabels() {
        return array(
        );
    }
    
    public static function getSearch()
	{
       
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby;	
        $sql="select id, concat(fname,' ',lname) fullname, username,password,department,mobile,email,address,remark from ictech_mas_icround where status!=0 ".$sqlCon;		
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
    public static function Searchdatalist()
	{       
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql = 'call spGetICRoundCheckList ('.$createby.')' ;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
        
	} 
    public static function checkSave($id)
	{       
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql = 'call spCheckListInMonthFromICRound ('.$id.','.$createby.')' ;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
        
	}
    public static function getData($id = null)
    {
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby." and id=".$id;	
        $sql="select * from ictech_mas_icround where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }
    public static function getDataGroup($id = null)
    {
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby." and department_id=".$id." group by title";	
        $sql="select * from ictech_mas_icround where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }
    public static function getDataGroupDetail($id = null)
    {
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby." and department_id=".$id." ";	
        $sql="select * from ictech_mas_icround where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }
    public static function getDataDetail($id = null)
    {
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby." and department_id=".$id." ";		
        $sql="select * from ictech_mas_icround_detail where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }
    public static function getDataCheckData($id = null,$createdate)
    {
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon="and create_by=".$createby." and department_id=".$id." and SUBSTRING(create_date, 1, 10)='".$createdate."' ";		
        $sql="select * from ictech_mas_icround_checklist where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }
    public static function getDataCheckSubData($id = null,$createdate)
    {
        
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby." and department_id=".$id."  and SUBSTRING(create_date, 1, 10)='".$createdate."' ";	
        $sql="select * from ictech_mas_icround_checklist_sub where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }
    
    
	public function search($keyword=null,$lavel=null) 
	{
		
		$sqlCon="";
		if(Yii::app()->user->getInfo('lavel')!=1){			
			$sqlCon.=" and a.create_by=".Yii::app()->user->getInfo('id');
		}
		
		if($keyword!=''){
			$sqlCon.= " and (a.code like '%".$keyword."%' ";
			$sqlCon.= " or a.name  like '%".$keyword."%') ";	
			/*		
			$sqlCon.= " or line  like '%".$keyword."%' ";	
			$sqlCon.= " or facebook  like '%".$keyword."%' ";	
			$sqlCon.= " or address  like '%".$keyword."%' ";	
			$sqlCon.= " or tel like '%".$keyword."%') ";	
			*/
			
		}
		if($lavel!=''){
			$sqlCon.= " and a.lavel ='".$lavel."' ";
		}
		
		$count=Yii::app()->db->createCommand("select count(*) from mas_user a where a.status=1 ".$sqlCon)->queryScalar();
		$sql ="select @rownum := @rownum + 1 AS rank, ";
		$sql.="a.id, a.code, a.name, ";
		$sql.="case a.status when 1 then 'ใช้งานอยู่' else 'ยกเลิกใช้งาน' end as status, ";
		$sql.="b.name as lavel,c.name as createby ";
		$sql.="from mas_user a ";
		$sql.="left join mas_lavel b on a.lavel=b.id ";
		$sql.="left outer join mas_user c on c.id=a.create_by ";
		$sql.=",(SELECT @rownum := 0) r  ";
		$sql.="where a.status=1 ".$sqlCon ;
		
		//echo var_dump($sql);exit;
		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 'rank', 'id', 'code', 'name', 'facebook', 'line', 'status', 'lavel', 'pass','createby',
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
	
	public function getUser($id = null)
	{
		
	   	$sql ="select id,code, name, line, facebook, email,address,tel,pass, ";
		$sql.="lavel,status ";
	   	$sql.="from mas_user where id='".$id."' ";	   
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}

}	
