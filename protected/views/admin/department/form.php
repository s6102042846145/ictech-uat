<?php
	$this->pageTitle = 'รายละเอียดตัวชี้วัด' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<!-- Main content -->
    <section class="content mt-3">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header thsarabunnew">
                <label class="card-title">ข้อมูลหน่วยงานและตัวชี้วัด</label>
                <div class="card-tools">
                  <ul class="pagination pagination-sm float-right">
                      <li class="page-item"><button type="button" class="btn btn-success" onclick="ajax_savedata()">บันทึก</button></li>
                  </ul>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body thsarabunnew">
                  <div class="form-row">
                      <div class="col-md-4"> 
                          <div class="position-relative form-group"> 
                              <label for="txtcode">รหัส</label>
                              <input id="txtcode" type="text" class="form-control" autocomplete="off" maxlength="20">        
                          </div> 
                      </div>
                      <div class="col-md-8">
                          <div class="position-relative form-group"> 
                              <label for="txtname">ชื่อหน่วยงาน</label>
                              <input id="txtname" type="text" class="form-control" maxlength="200" autocomplete="off">        
                          </div> 
                      </div>
                  </div>
                  <div class="table-responsive">
                      <table id="tbindicators" class="w-100 table table-bordered table-striped thsarabunnew">
                        <thead>
                            <tr>
                                <th>ตัวชี้วัด</th>
                                <th style="width:50px;" class="text-center">เลือก</th>
                            </tr>
                        </thead>
                      </table>
                  </div>
                  <div class="p-4 text-center">
                      <input type="button" onClick="window.history.go(-1); return false;" value="ย้อนกลับ" class="btn btn-default" />
                  </div>
              </div>
              <!-- /.card-body -->
                
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
<input id="hdfid" type="hidden" />
<script>
$().ready(function (){
    var id = getUrlParameter("id");
    $('#hdfid').val(id);
    $('#tbindicators').DataTable( {
        "ajax": {
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/indicators"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json"
        },
        "columns": [
            //{ "data": "code" },
            { "data": "name" },
            {
                data:   "checked",
                'render': function (data, type, full,type){
                     //return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                     return '<input type="checkbox" id="chk'+full.id+'" onclick="selectdata('+full.id+')" ' + data + ' />';
                 },
                className: "dt-body-center"
            }
        ],
        pageLength: 20,
        "searching": true,
        "bLengthChange": false
    }); 
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/departmentdata"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {                    
                $('#txtcode').val(data.code);
                $('#txtname').val(data.name);		
            }else{
                alert(data.msg);
            } 
        }
    });	
});
function selectdata(id)
{
    var status = $('#chk'+ id).is(":checked");
    var department_id = $('#hdfid').val();
    //$("#exampleModalCenter").modal('show');   
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setdata"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','indicators_id':id,'department_id':department_id,'status':status==true ? 1: 0},
        dataType: "json",				
        success: function (data) 
        {
            if (data.status=='success') {			
               // $("#exampleModalCenter").modal('hide');                          
            }
            else{
                alert(data.msg);
            } 
        }
    });
}
function ajax_savedata() 
{
    var id=$('#hdfid').val();
    var code=$('#txtcode').val();
    var name=$('#txtname').val();
    if(code=='')
    {
        alert('กรุณากรอกรหัสหน่วยงาน');
        return;
    }
    if(name=='')
    {
        alert('กรุณากรอกชื่อหน่วยงาน');
        return;
    }    
    $("#exampleModalCenter").modal('show');
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/savedata"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','code':code,'name':name,'id':id},
        dataType: "json",				
        success: function (data) 
        {
            if (data.status=='success') 
            {  
                window.history.go(-1); 
                return false;
            }
            else
            {
                alert(data.msg);
            } 
        }
    });
}	
</script>