<?php
	$this->pageTitle = 'Infection Control Checklist' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>

  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/summernote/summernote-bs4.min.css">

<!-- Main content -->
<section class="content mt-3 thsarabunnew">
   
    
  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">
              <label>ตั้งค่าคำถาม</label>
          </h3>
           <input type="button" value="สร้างคำถาม" id="btnAddFAQ" class="btn btn-default float-right" />
        </div>
        <!-- /.card-header -->
        <div class="card-body">            
            <div class="table-responsive">
                <table id="tbdata" class="w-100 table table-bordered table-striped thsarabunnew">
                  <thead>
                  <tr>                    
                    <th>หัวข้อการประเมิน</th>
                    <th>รายละเอียดการประเมิน</th>
                    <th  style="width:50px;" class="text-center">แก้ไข</th>
                    <th  style="width:50px;" class="text-center">ลบ</th>
                  </tr>
                  </thead>

                </table>
              </div>
            </div>
        <div class="card-footer">
            <div class="p-4 text-center">
                  <a href="/admin/department/" class="btn btn-default">ย้อนกลับ</a>
              </div>
        </div>
      </div>
    </div>
    <!-- /.col-->
  </div>
    
    
<div class="modal fade" id="modal-faq" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
              <h3 class="card-title">
                 <label>สร้างคำถาม</label>
              </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
                  
            <!-- /.card-header -->
            <div id="divTheme1" class="card-body">
                <div class="row">
                    <div class="col-md-12" >
                        <label for="txttheme1_title" class="form-label">หัวข้อ</label>
                        <input type="text" class="form-control" id="txttheme1_title" >

                    </div>
                     <div class="col-md-12 mt-3">
                        <button onClick="addFAQRowT1()" class="btn btn-warning float-right">
                            เพิ่มคำถาม
                        </button>
                   </div> 
                </div>
                <div class="t1 row">
                    <div class="col-md-12">
                        <label for="txttheme1_faq0" class="form-label">คำถาม</label>
                        <input type="text" class="form-control" id="txttheme1_faq0" />
                    </div>
               </div>
            </div>
            
        <!-- /.col-->
           
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button"  class=" btn btn-primary" onClick="ajax_savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-sub" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
              <h3 class="card-title">
                 <label>แก้ไขคำถาม</label>
              </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
                  
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12" >
                    <label for="txttitle" class="form-label">หัวข้อ</label>
                    <input type="text" class="form-control" id="txttitle" >

                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="txtname" class="form-label">คำถาม</label>
                    <input type="text" class="form-control" id="txtname" />
                </div> 
                <div class="col-md-12 mt-3">
                    <button onClick="addFAQRowT2()" class="btn btn-warning float-right">
                        เพิ่มคำถามย่อย
                    </button>
               </div> 
           </div>
            <div class="t2 row">
                <div class="col-md-12">
                    <label for="txttheme2_faq0" class="form-label">คำถาม</label>
                    <input type="text" class="form-control" id="txttheme2_faq0" />
                </div> 
                
           </div>
        </div>
            
        <!-- /.col-->
           
            
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnSave" class=" btn btn-primary" onClick="savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</section>


<!-- /.content -->
<input type="hidden" id="hdfid" />
<input type="hidden" id="hdfdatarow" />
<input type="hidden" id="hdfdatarow2" />
<input type="hidden" id="hdfdatanumber" />
<input type="hidden" id="hdfindicator_id" />
<input type="hidden" id="hdfdepartment" />
<input type="hidden" id="hdfgroup_id" />
<input type="hidden" id="hdfstatus" />
<script>
    $().ready(function (){
        //$("#exampleModalCenter").modal('show');
        $("#modal-faq").on('hidden.bs.modal', function (e) {
           $(".theme1").remove();
            $("#hdfid").val("");
            $("#txttheme1_title").val("");
            
        });
        $("#modal-sub").on('hidden.bs.modal', function (e) {
           $('#hdfdatarow2').val("");
        });
        $('#btnAddFAQ').click(function () {
            $("#modal-faq").modal('show');
            $("#txttheme1_title").val("");
            $("#txttheme1_faq0").val("");
        });
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
        var id = getUrlParameter("id");
        $("#hdfdepartment").val(id);
        $('#tbdata').DataTable( {
            "ajax": {
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/searchicround"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json"
            },
            "columns": [
                { "data": "title" },
                { "data": "name" },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="แก้ไข" onclick="setUpdate('+full.id+')" class="badge bg-primary cursor-pointer"><i class="fas fa-edit"></i></span>';
                     },
                    className: "dt-body-center"
                },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="ลบ" onclick="setDelete('+full.id+')" class="btndelete badge bg-danger cursor-pointer"><i class="fas fa-trash-alt"></i></span>';
                     },
                    className: "dt-body-center"
                }
            ],
            pageLength: 40,
           "bLengthChange": false
        }); 
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/SearchDataCheckList"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if(data.data[0].cnt>0){
                    $("#btnAddFAQ").addClass("d-none");
                    $("button.btn.btn-warning.float-right").addClass("d-none");
                    $("#btnSave").addClass("d-none");
                    //$(".btndelete").addClass("d-none");
                    //$("#exampleModalCenter").modal('hide');  
                    $("#hdfstatus").val(1);//1=มีการใช้แบบฟอร์มแล้ว
                }else{
                    //$("#exampleModalCenter").modal('hide');  
                }
            }
        });
           
        
    });
     function setDelete(id) {   
        var status= $("#hdfstatus").val();
        if(status==1){
            alert("ไม่สามรถทำการลบข้อมูลนี้ได้ เนื่องจากมีการใช้แบบฟอร์มนี้แล้ว !");
            return;
        } 
         
        var r = confirm("คุณต้องการลบข้อมูลนี้ใช่หรือไม่ !");

        if (r == true) {
            //$("#exampleModalCenter").modal('show');
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/annualplan/deletedata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {	
                        $('#tbdata').DataTable().ajax.reload();
                        //$("#exampleModalCenter").modal('hide');
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
        }
    }	
    function addFAQRowT1()
    {
        var count = $('#hdfdatarow').val();	
        if(count==''){ count=1;}
        var id = Number(count)+1;
        $('.t1').append(' <div class="col-md-12 theme1"><label for="txttheme1_faq'+ id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txttheme1_faq'+ id +'" /></div>');                        
        $('#hdfdatarow').val(id);
    }
    function addFAQRowT2()
    {
        var count = $('#hdfdatarow2').val();	
        if(count==''){ count=1;}
        var id = Number(count)+1;
        $('.t2').append(' <div class="col-md-12 theme2"><label for="txttheme2_faq'+ id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txttheme2_faq'+ id +'" /></div>');                        
        $('#hdfdatarow2').val(id);
    }   
    
    function setUpdate(id) 
    {    
        //var id = $(el).parent().attr("data-id"); 
        var department_id = $("#hdfdepartment").val();
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setUpdateIC"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'department_id':department_id},
            dataType: "json",				
            success: function (data) {
                if(data.data.length>0){
                    var param = data.data[0];
                    if (data.status=='success') { 
                        //alert(1)
                        $("#txttitle").val(param.title);
                        $("#hdfgroup_id").val(param.group_id);
                        $("#txtname").val(param.name);
                        $("#hdfid").val(param.id);
                        var html = "";
                        $.each(data.data2, function(i, item) {
                            //alert(data.question[i].id);
                            html+= '<div class="col-md-12 theme2"><label for="txttheme2_faq'+ data.data2[i].id +'" class="form-label">คำถาม</label><input type="text" data-id="'+ data.data2[i].id +'" value="'+ data.data2[i].name +'" class="form-control" id="txttheme2_faq'+ data.data2[i].id +'" /></div>';
                            
                            $('#hdfdatarow2').val(data.data2.length);

                        });
                        $('.t2').html(html);
                        for (let i = 0; i < data.data2.length; i++) {
                          //text += data.data[i] + "<br>";
                          //$("#txtname"+data.data[i].question_id).val(data.data[i].name);

                        }
                        /*
                        for (let i = 0; i < data.data2.length; i++) {
                          $("#ch"+data.data2[i].id+"[value='" + data.data2[i].question_id + "']").prop('checked', data.data2[i].name==0 ? false: true);
                        }
                        */
                    }
                    else
                    {
                        alert(data.msg);
                    } 
                }
                $("#modal-sub").modal('show');
                
            }
        });	
    } 
    function ajax_savedata()
    {
        var i = 0;
        var id = $('#hdfid').val();
        var name = $('#txttheme1_title').val();
        var data_faq = [];
        var data_row = $("#hdfdatarow").val();
        var department_id = $("#hdfdepartment").val();
        if(data_row==''){ data_row=1}
        while(i <= data_row)
        {       
            var faq = '#txttheme1_faq'+i;
            data_faq[i] = $(faq).val();
            i++;
        }
        
        $("#modal-faq").modal('hide');
        //$("#exampleModalCenter").modal('show');
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/savedataicround"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'name':name,'department_id':department_id,'data_faq':data_faq},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {	
                    $('#tbdata').DataTable().ajax.reload();
                    //$("#exampleModalCenter").modal('hide');
                }else{
                    //$("#exampleModalCenter").modal('hide');
                    alert(data.msg);
                } 
            }
        });

    }  
    function savedata()
    {
        var i = 0;
        var id = $('#hdfid').val();
        var title = $('#txttitle').val();
        var group_id =$("#hdfgroup_id").val();
        var name = $('#txtname').val();
        var data_faq = [];
        var data_id = [];
        var data_row = $("#hdfdatarow2").val();
        var department_id = $("#hdfdepartment").val();
        if(data_row==''){ data_row=1}
        while(i <= data_row)
        {       
            var faq = '#txttheme2_faq'+i;
            data_faq[i] = $(faq).val();
            data_id[i] = $(faq).attr("data-id")==undefined ? 0 : $(faq).attr("data-id"); 
            i++;
        }
        //console.log(title+name);return;
        $("#modal-sub").modal('hide');
        //$("#exampleModalCenter").modal('show');
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/savedataicround2"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'title':title,'group_id':group_id,'name':name,'department_id':department_id,'data_id':data_id,'data_faq':data_faq},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {	
                    $('#tbdata').DataTable().ajax.reload();
                    //$("#exampleModalCenter").modal('hide');
                }else{
                    //$("#exampleModalCenter").modal('hide');
                    alert(data.msg);
                } 
            }
        });

    }    
</script>
<script>

$(document).ready(function (){
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    };
    $('input[type=radio][name=radioTheme]').change(function() {
        if (this.value == '1') {                    
            $("#divTheme1").removeClass("d-none");
            $("#divTheme2").addClass("d-none");
            $("#divTheme3").addClass("d-none");
        }else if (this.value == '2') {
            $("#divTheme1").addClass("d-none");
            $("#divTheme2").removeClass("d-none");
            $("#divTheme3").addClass("d-none");
        }
        /*
        else if (this.value == '3') {
            $("#divTheme1").addClass("d-none");
            $("#divTheme2").addClass("d-none");
            $("#divTheme3").removeClass("d-none");
        }*/
    });
     $('#modal-faq').on('hidden.bs.modal', function (e) {
        //$('input:radio[name="theme"]').val("");
         /*
        $("#step-1").removeClass("d-none");
        $("#step-2").addClass("d-none");
        $("#step-3").addClass("d-none");
        $("#step-4").addClass("d-none");
        $('input:radio[name="theme"]').prop('checked', false);
        $("#hdfid").val("");
        $("#btnSave").addClass("d-none");
        $("#btnNext").removeClass("d-none");
        */
         //$("#exampleModalCenter").modal("show");
         
     });

    
    
    
    
    $('#btnAddTitle').click(function () {
        $(".modal-title").html("เพิ่มข้อมูล");  
        $("#modal-title").modal('show');
    });
    
});   

function getHAI(){
    var indicator_id = getUrlParameter('indicators');
    $('#hdfindicator_id').val(indicator_id);
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setHAI"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','indicator_id':indicator_id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {		
                $("#txtHAI").summernote("code", data.hai);
                $("#txtCAI").summernote("code", data.cai);
            }
            else{
                alert(data.msg);
            } 
        }
    });
}
function saveHai(){
    var hai =  $("#txtHAI").val();
    var cai =  $("#txtCAI").val();
    var id =  getUrlParameter('indicators');
    
    //alert(id+hai);return;
    //alert(hai);return;
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/saveHAI"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'hai':hai,'cai':cai},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {
                alert("บันทึกข้อมูลสำเร็จ !");
            }
            else{
                alert(data.msg);
            } 
        }
    });
}
    
function setUpdateTitle(el) 
{    var id = $(el).parent().attr("data-id"); 
    
     $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setUpdateTitle"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {     
                $("#txttitle").val(data.name);
                $("#txttitle_detail").summernote({
                    height: 150,   //set editable area's height
                    codemirror: { // codemirror options
                    theme: 'monokai'
                  }
                });
                
                $("#txttitle_detail").summernote("code", data.detail);
                $(".modal-title").html("แก้ไขข้อมูล");  
                $("#modal-title").modal('show');
            }else{
                alert(data.msg);
            } 
        }
    });	
}   
function setUpdateFAQ(el) 
{    
    var id = $(el).parent().attr("data-id");     
     $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setUpdateFAQ"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') { 
                $("#step-1").addClass("d-none");
                if(data.theme=="1"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").addClass("d-none");
                }
                if(data.theme=="2"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").removeClass("d-none");
                }
                if(data.theme=="3"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").removeClass("d-none");
                    $("#step-4").removeClass("d-none");
                }
                
                $("#btnNext").addClass("d-none");
                $("#btnSave").removeClass("d-none");
                
                $("#hdfid").val(data.id);
                $("#hdftheme").val(data.theme);
                //$('input:radio[name="theme"]').val(data.theme);
                $("#txttitle_detail").summernote("code", data.tooltip);
                $("#txttitle").val(data.name);
                $("#txtunit").val(data.unit);
                
                var html = "";
                $.each(data.question, function(i, item) {
                    //alert(data.question[i].id);
                    html+= '<div data-ids="'+ data.question[i].id +'" class="col-md-8 "><label for="txtfaq'+ data.question[i].id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txtfaq'+ data.question[i].id +'" value="'+ data.question[i].name +'" /></div><div class="col-md-4 "><label for="txtunit'+ data.question[i].id +'" class="form-label">หน่วย</label><input type="text" class="form-control" id="txtunit'+ data.question[i].id +'" value="'+ data.question[i].unit +'" /></div>';
    
                    $('#hdfdatarow').val(data.question[i].id);
                    
                });
                $('.q1').html(html);
                if(data.theme!="1"){
                    var html2 = "";
                    $("#txtfaq_sub").val(data.title);
                    $.each(data.question2, function(i, item) {
                        //alert(data.question[i].id);
                        html2+= '<div data-id="'+ data.question2[i].id +'" class="col-md-2 "><label for="txtnumber'+ data.question2[i].id +'" class="form-label">ลำดับ</label><input type="text" class="form-control text-center" id="txtnumber'+ data.question2[i].id +'" value="'+ data.question2[i].item_order +'" disabled /></div><div class="col-md-10"><label for="txtdetail'+ data.question2[i].id +'" class="form-label">รายละเอียด</label><input type="text" class="form-control" value="'+ data.question2[i].name +'" id="txtdetail'+ data.question2[i].id +'" /></div>';

                        $('#hdfdatanumber').val(data.question2[i].id);	

                    });
                    $('.q2').html(html2);
                }
                $(".modal-title").html("แก้ไขข้อมูล");  
                $("#modal-faq").modal('show');
            }else{
                alert(data.msg);
            } 
        }
    });	
}    

</script>