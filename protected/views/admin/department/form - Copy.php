<?php
	$this->pageTitle = 'รายละเอียดตัวชี้วัด' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>

  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/summernote/summernote-bs4.min.css">

<!-- Main content -->
<section class="content mt-3 thsarabunnew">
    <?php
        $data = lkup_department::getHaiAndCai($_GET['indicators']);
        if($data[0]["hai"]==1)
        {
    ?>
    <script>
        $().ready(function (){
           getHAI(); 
        });
    </script>
          <div class="row">
            <div class="col-md-12">
              <div class="card card-outline card-info">
                <div class="card-header">
                  <h3 class="card-title">
                    HAI/CAI
                  </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <label>การติดเชื้อในรพ.(HAI)</label>
                  <textarea id="txtHAI"></textarea>
                  <label>การติดเชื้อในชุมชน (CAI)</label>
                  <textarea id="txtCAI"></textarea>
                </div>
                <div class="card-footer">
                  <input type="button" value="Save" class="btn btn-success float-right" onClick="saveHai();" />
                </div>
              </div>
            </div>
            <!-- /.col-->
          </div>
    
    
    <?php 
        }
    
            
            /*
    <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title"><label>
            ตั้งค่าหัวข้อ</label>
          </h3>
          <input type="button" value="เพิ่มข้อมูล" id="btnAddTitle" class="btn btn-default float-right" />
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <?php
               $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'list-grid-title',
                    'dataProvider' => $modelTitle,
                    'htmlOptions' => array('width' => '500px'),
                    'itemsCssClass' => 'table table-bordered table-striped',	
                    'rowHtmlOptionsExpression'=>'array("data-id"=>$data["id"])',
                    'summaryText' => 'แสดงข้อมูล: {start} - {end} จาก {count} รายการ',
                    'pagerCssClass'=>'mailbox-pager',
                    'pager' => array(
                        'class'=>'CLinkPager',
                        'header' => '',
                        'firstPageLabel'=>'หน้าแรก',
                        'prevPageLabel'=>'ก่อนหน้า',
                        'nextPageLabel'=>'หน้าถัดไป', 
                        'lastPageLabel'=>'หน้าสุดท้าย',	

                    ),		
                    'columns' => array(
                        array(
                            'name'=>'name',
                            'header' => 'หัวข้อ',
                            'htmlOptions'=>array('style'=>'text-align:left;'),
                            'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                          ),
                            
                         array(
                            'name'=>'detail',
                            'header' => 'คำอธิบาย',
                            'htmlOptions'=>array('style'=>'text-align:left;'),
                            'headerHtmlOptions'=>array('style'=>' text-align:center;'),
                          ),
                         array(
                            'header' => 'แก้ไข',
                            'class' => 'CLinkColumn',
                            'label' => '<i class="fas fa-edit"></i>',
                            'htmlOptions' => array(
                                'width' => '30px',
                                'align' => 'center',
                                'onclick'=>'setUpdateTitle(this);'
                            ),
                            'linkHtmlOptions'=>array('class'=>'btn btn-info'),
                            'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                        ), 	
                        array(
                            'header' => 'ลบ',
                            'class' => 'CLinkColumn',
                            'label' => '<i class="fas fa-trash-alt"></i>',				
                            'htmlOptions' => array(
                                'width' => '30px',
                                'align' => 'center',
                                'onclick' => 'setDeleteTitle(this);'
                            ),
                            'linkHtmlOptions'=>array('class'=>'btn btn-danger'),
                            'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                        )
                    ),
                ));
            ?> 
            
            
        </div>
        <div class="card-footer">
          
        </div>
      </div>
    </div>
    <!-- /.col-->
  </div>
  */
  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">
              <label>ตั้งค่าคำถาม</label>
          </h3>
           <input type="button" value="สร้างคำถาม" id="btnAddFAQ" class="btn btn-default float-right" />
        </div>
        <!-- /.card-header -->
        <div class="card-body">            
            <?php
               $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'list-grid-question',
                    'dataProvider' => $modelFAQ,
                    'htmlOptions' => array('width' => '500px'),
                    'itemsCssClass' => 'table table-bordered table-striped',	
                    'rowHtmlOptionsExpression'=>'array("data-id"=>$data["id"])',
                    'summaryText' => 'แสดงข้อมูล: {start} - {end} จาก {count} รายการ',
                    'pagerCssClass'=>'mailbox-pager',
                    'pager' => array(
                        'class'=>'CLinkPager',
                        'header' => '',
                        'firstPageLabel'=>'หน้าแรก',
                        'prevPageLabel'=>'ก่อนหน้า',
                        'nextPageLabel'=>'หน้าถัดไป', 
                        'lastPageLabel'=>'หน้าสุดท้าย',	

                    ),		
                    'columns' => array(
                        array(
                            'name'=>'name',
                            'header' => 'คำถาม',
                            'htmlOptions'=>array('style'=>'text-align:left;'),
                            'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                          ),
                            /*
                         array(
                            'name'=>'unit',
                            'header' => 'หน่วย',
                            'htmlOptions'=>array('style'=>'text-align:center;'),
                            'headerHtmlOptions'=>array('style'=>'width:100px; text-align:center;'),
                          ),*/
                        array(
                            //'name'=>'active',
                            'type' => 'html',
                            'value' => array($this, 'getTheme'),
                            'header' => 'เทมเพลต',
                            'htmlOptions'=>array(
                                'style'=>'text-align:center;width:50px;'),
                                //'class'=>function($data){return $data['status']==2?"abc":"yes";}),
                            'headerHtmlOptions'=>array('style'=>'width:50px; text-align:center;'),
                        ),	
                         array(
                            'header' => 'แก้ไข',
                            'class' => 'CLinkColumn',
                            'label' => '<i class="fas fa-edit"></i>',
                            'htmlOptions' => array(
                                'width' => '30px',
                                'align' => 'center',
                                'onclick'=>'setUpdateFAQ(this);'
                            ),
                            'linkHtmlOptions'=>array('class'=>'btn btn-info'),
                            'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                        ), 	
                        array(
                            'header' => 'ลบ',
                            'class' => 'CLinkColumn',
                            'label' => '<i class="fas fa-trash-alt"></i>',				
                            'htmlOptions' => array(
                                'width' => '30px',
                                'align' => 'center',
                                'onclick' => 'setDeleteFAQ(this);'
                            ),
                            'linkHtmlOptions'=>array('class'=>'btn btn-danger'),
                            'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                        )
                    ),
                ));
            ?> 
        </div>
        <div class="card-footer">
        </div>
      </div>
    </div>
    <!-- /.col-->
  </div>
    
    
<div class="modal fade" id="modal-title" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h3 class="card-title"><label class="modal-title"></label></h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
                <!--div class="col-md-12 mb-3">
                    <label for="txttitle" class="form-label">หัวข้อ</label>
                    <input type="text" class="form-control" id="txttitle" >
                    <label for="txttitle_detail" class="form-label">คำอธิบายหัวข้อ</label>
                    <textarea rows="4" cols="50" id="txttitle_detail"></textarea>
                </div-->
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onClick="ajax_savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
    
<div class="modal fade" id="modal-faq" data-backdrop="static">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
            <h3 class="card-title"><label class="modal-title"></label></h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row" id="step-1"> 
                <div class="col-md-4">
                    <label class="label" for="theme1">
                        <img src="<?php echo Yii::app()->params['prg_ctrl']['theme-1'] ?>" style="width:100%; background-color: #ccc;" /><br />
                        <input type="radio" id="theme1" name="theme" value="1" />
                    </label>
                </div>
                <div class="col-md-4">
                     <label class="label" for="theme2">
                        <img src="<?php echo Yii::app()->params['prg_ctrl']['theme-2'] ?>" style="width:100%; background-color: #ccc;" /><br />
                        <input type="radio" id="theme2" name="theme" value="2" />
                    </label>
                </div>
                <div class="col-md-4">
                     <label class="label" for="theme3">
                        <img src="<?php echo Yii::app()->params['prg_ctrl']['theme-3'] ?>" style="width:100%; background-color: #ccc;" /><br />
                        <input type="radio" id="theme3" name="theme" value="3" />
                    </label>
                </div>
                
            </div>
            <div class="row d-none" id="step-3"> 
                <div class="col-md-12" >
                    <label for="txttitle" class="form-label">หัวข้อ</label>
                    <input type="text" class="form-control" id="txttitle" >
                    <label for="txttitle_detail" class="form-label">Popup</label>
                    <textarea rows="4" cols="50" id="txttitle_detail"></textarea>
                </div>
            </div>
            <div class=" d-none" id="step-2"> 
                <div class="col-md-12 text-right">
                    <button onClick="addFAQRow()" class="btn btn-warning">
                        เพิ่มคำถาม
                    </button>
                </div>
                <div class="q1 row">
                    <div class="col-md-8">
                        <label for="txtfaq0" class="form-label">คำถาม</label>
                        <input type="text" class="form-control" id="txtfaq0" />
                    </div>
                    <div class="col-md-4">
                        <label for="txtunit0" class="form-label">หน่วย</label>
                        <input type="text" class="form-control" id="txtunit0" />
                   </div> 
               </div>
                         
            </div>
            <div class=" d-none" id="step-4">
                <div class="row mt-4">
                    <div class="col-md-8">
                        <label for="txtfaq_sub" class="form-label">หัวข้อคำถาม</label>
                        <input type="text" class="form-control" id="txtfaq_sub" />
                    </div>
                    <div class="col-md-4">
                        <!--
                        <label for="txtamount" class="form-label">จำนวน</label>
                        <input type="text" class="form-control" id="txtamount" />
                        -->
                   </div>
               </div>
               <div class="col-md-12 text-right">
                    <button onClick="addNumberRow()" class="btn btn-warning">
                        เพิ่มลำดับ
                    </button>
                </div>
                <div class="q2 row">
                    <div class="col-md-2">
                        <label for="txtnumber0" class="form-label">ลำดับ</label>
                        <input type="text" class="form-control text-center" id="txtnumber0" value="1" disabled />
                    </div>
                    <div class="col-md-10">
                        <label for="txtdetail0" class="form-label">รายละเอียด</label>
                        <input type="text" class="form-control" id="txtdetail0" />
                   </div>
               </div>
            </div>
            
            
            <style>
            label.label {
              /*float: left;*/
              /*padding: 0 1em;*/
              text-align: center;
            }
            </style>
            
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnNext" class="btn btn-primary" onClick="next_step();">Next</button>
          <button type="button" id="btnSave" class="d-none btn btn-primary" onClick="ajax_savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</section>


<!-- /.content -->
<input type="hidden" id="hdfid" />
<input type="hidden" id="hdfdatarow" />
<input type="hidden" id="hdfdatanumber" />
<input type="hidden" id="hdfindicator_id" />
<input type="hidden" id="hdftheme" />



<!-- Summernote -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/summernote/summernote-bs4.min.js"></script>



<script>

$(document).ready(function (){
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};
     $('#modal-faq').on('hidden.bs.modal', function (e) {
        //$('input:radio[name="theme"]').val("");
         /*
        $("#step-1").removeClass("d-none");
        $("#step-2").addClass("d-none");
        $("#step-3").addClass("d-none");
        $("#step-4").addClass("d-none");
        $('input:radio[name="theme"]').prop('checked', false);
        $("#hdfid").val("");
        $("#btnSave").addClass("d-none");
        $("#btnNext").removeClass("d-none");
        */
         $("#exampleModalCenter").modal("show");
         window.location.reload();
     });

    
    // Summernote
    $('#txtHAI').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    $('#txtCAI').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    $('#txttitle_detail').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    $('#txttitle_detail2').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    
    
    $('#btnAddTitle').click(function () {
        $(".modal-title").html("เพิ่มข้อมูล");  
        $("#modal-title").modal('show');
    });
    $('#btnAddFAQ').click(function () {
        $(".modal-title").html("เลือกเทมเพลต");  
        $("#modal-faq").modal('show');
     });
});   

function getHAI(){
    var indicator_id = getUrlParameter('indicators');
    $('#hdfindicator_id').val(indicator_id);
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setHAI"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','indicator_id':indicator_id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {		
                $("#txtHAI").summernote("code", data.hai);
                $("#txtCAI").summernote("code", data.cai);
            }
            else{
                alert(data.msg);
            } 
        }
    });
}
function saveHai(){
    var hai =  $("#txtHAI").val();
    var cai =  $("#txtCAI").val();
    var id =  getUrlParameter('indicators');
    
    //alert(id+hai);return;
    //alert(hai);return;
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/saveHAI"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'hai':hai,'cai':cai},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {
                alert("บันทึกข้อมูลสำเร็จ !");
            }
            else{
                alert(data.msg);
            } 
        }
    });
}
    
function setUpdateTitle(el) 
{    var id = $(el).parent().attr("data-id"); 
    
     $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setUpdateTitle"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {     
                $("#txttitle").val(data.name);
                $("#txttitle_detail").summernote({
                    height: 150,   //set editable area's height
                    codemirror: { // codemirror options
                    theme: 'monokai'
                  }
                });
                
                $("#txttitle_detail").summernote("code", data.detail);
                $(".modal-title").html("แก้ไขข้อมูล");  
                $("#modal-title").modal('show');
            }else{
                alert(data.msg);
            } 
        }
    });	
}   
function setUpdateFAQ(el) 
{    
    var id = $(el).parent().attr("data-id");     
     $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setUpdateFAQ"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') { 
                $("#step-1").addClass("d-none");
                if(data.theme=="1"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").addClass("d-none");
                }
                if(data.theme=="2"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").removeClass("d-none");
                }
                if(data.theme=="3"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").removeClass("d-none");
                    $("#step-4").removeClass("d-none");
                }
                
                $("#btnNext").addClass("d-none");
                $("#btnSave").removeClass("d-none");
                
                $("#hdfid").val(data.id);
                $("#hdftheme").val(data.theme);
                //$('input:radio[name="theme"]').val(data.theme);
                $("#txttitle_detail").summernote("code", data.tooltip);
                $("#txttitle").val(data.name);
                $("#txtunit").val(data.unit);
                
                var html = "";
                $.each(data.question, function(i, item) {
                    //alert(data.question[i].id);
                    html+= '<div data-ids="'+ data.question[i].id +'" class="col-md-8 "><label for="txtfaq'+ data.question[i].id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txtfaq'+ data.question[i].id +'" value="'+ data.question[i].name +'" /></div><div class="col-md-4 "><label for="txtunit'+ data.question[i].id +'" class="form-label">หน่วย</label><input type="text" class="form-control" id="txtunit'+ data.question[i].id +'" value="'+ data.question[i].unit +'" /></div>';
    
                    $('#hdfdatarow').val(data.question[i].id);
                    
                });
                $('.q1').html(html);
                if(data.theme!="1"){
                    var html2 = "";
                    $("#txtfaq_sub").val(data.title);
                    $.each(data.question2, function(i, item) {
                        //alert(data.question[i].id);
                        html2+= '<div data-id="'+ data.question2[i].id +'" class="col-md-2 "><label for="txtnumber'+ data.question2[i].id +'" class="form-label">ลำดับ</label><input type="text" class="form-control text-center" id="txtnumber'+ data.question2[i].id +'" value="'+ data.question2[i].item_order +'" disabled /></div><div class="col-md-10"><label for="txtdetail'+ data.question2[i].id +'" class="form-label">รายละเอียด</label><input type="text" class="form-control" value="'+ data.question2[i].name +'" id="txtdetail'+ data.question2[i].id +'" /></div>';

                        $('#hdfdatanumber').val(data.question2[i].id);	

                    });
                    $('.q2').html(html2);
                }
                $(".modal-title").html("แก้ไขข้อมูล");  
                $("#modal-faq").modal('show');
            }else{
                alert(data.msg);
            } 
        }
    });	
}    
function next_step(){
    
    var theme = $('input:radio[name="theme"]:checked').val();
    //alert(theme)
    if(theme=="1"){
        $("#step-2").removeClass("d-none");
        $("#step-3").addClass("d-none");
    }else if(theme=="2"){
        $("#step-2").removeClass("d-none");
        $("#step-3").removeClass("d-none");
    }else if(theme=="3"){
         $("#step-2").removeClass("d-none");
        $("#step-3").removeClass("d-none");
        $("#step-4").removeClass("d-none");
    }else{
        alert("กรุณาเลือกเทมเพลต");
        return;
    }
    $(".modal-title").html("เพิ่มข้อมูล"); 
    $("#step-1").addClass("d-none");
    $("#btnSave").removeClass("d-none");
    $("#btnNext").addClass("d-none");
}   

function ajax_savedata(){
    var i = 0;
    var j = 0;
	var id = $('#hdfid').val();
	var data_row = $('#hdfdatarow').val();
	var data_num = $('#hdfdatanumber').val();
    var indicator_id = $('#hdfindicator_id').val();
    var theme = $('#hdftheme').val();
    if(theme==""){        
        theme = $('input:radio[name="theme"]:checked').val();
    }
    
	var name = $('#txttitle').val();
    var title_detail = $("#txttitle_detail").val();
    
	var faq_sub = $('#txtfaq_sub').val();
    
    
    var data_number = [];
    var data_detail = [];
    
    var data_faq = [];
    var data_unit = [];
   
    
    if(data_row==''){ data_row=1}
	while(i <= data_row)
	{
        var faq = '#txtfaq'+i;
        var unit = '#txtunit'+i;
        
        data_faq[i] = $(faq).val();
        data_unit[i] = $(unit).val();
        
		i++;
	}
	if(data_num==''){ data_num=1}
	while(j <= data_num)
	{
        var number = '#txtnumber'+j;
        var detail = '#txtdetail'+j;
        
        data_number[j] = $(number).val();
        data_detail[j] = $(detail).val();
        
		j++;
	}
    
	//return;
	$.ajax({
		type: "POST",
		url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/savedatadetail"); ?>",
		data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'name':name,'indicator_id':indicator_id,'theme':theme,'title_detail':title_detail,'faq_sub':faq_sub,'data_faq':data_faq,'data_unit':data_unit,'data_number':data_number,'data_detail':data_detail},
		dataType: "json",				
		success: function (data) {
			if (data.status=='success') {	
				//getSearch();	
				
				alert('success');
                window.location.reload();
				//showdata();	
				//$("#modaldetail").modal('hide'); 
				
				//fileimport();
			}else{
				//$('#pleaseWaitDialog').modal('hide');
				alert(data.msg);
			} 
		}
	});
    
}    
    
function addFAQRow()
{
	var count = $('#hdfdatarow').val();	
	if(count==''){
		count=1;
	}
	
	//return;
	var id = Number(count)+1;
	//alert(i);
    
    $('.q1').append('<div data-ids="'+ id +'" class="col-md-8 "><label for="txtfaq'+ id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txtfaq'+ id +'" /></div><div class="col-md-4 "><label for="txtunit'+ id +'" class="form-label">หน่วย</label><input type="text" class="form-control" id="txtunit'+ id +'" /></div> ');
    
    $('#hdfdatarow').val(id);
   
	
}    
function addNumberRow(){
    var count = $('#hdfdatanumber').val();	
	if(count==''){
		count=1;
	}
	
	//return;
	var id = Number(count)+1;
	//alert(i);
    
    $('.q2').append('<div data-id="'+ id +'" class="col-md-2 "><label for="txtnumber'+ id +'" class="form-label">ลำดับ</label><input type="text" class="form-control text-center" id="txtnumber'+ id +'" value="'+ id +'" disabled /></div><div class="col-md-10"><label for="txtdetail'+ id +'" class="form-label">รายละเอียด</label><input type="text" class="form-control" id="txtdetail'+ id +'" /></div>');
    
    $('#hdfdatanumber').val(id);	
}    
</script>