<?php
	$this->pageTitle = 'วิดีโอความรู้สำหรับเจ้าหน้าที่' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>

<!-- Main content -->
<section class="row content mt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <div class="card">
            <div class="card-header thsarabunnew">
            <label class="card-title">วิดีโอความรู้สำหรับเจ้าหน้าที่</label>

            <div class="card-tools">
              <ul class="pagination pagination-sm float-right">
                  <li class="page-item"><a class="page-link" id="btnAdd" href="javascript:void(0)">Add</a></li>
              </ul>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <div class="table-responsive">
                <table id="tbdata" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>ชื่อ</th>
                            <th>URL</th>
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table>
              </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>    

 <div class="modal fade" id="modal-lg" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header thsarabunnew">
          <label class="modal-title"></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body thsarabunnew">
          <div class="form-row">
              <div class="col-md-6"> 
                  <div class="position-relative form-group"> 
                      <span for="txtname">ชื่อ</span>
                      <input id="txtname" type="text" class="form-control">        
                  </div> 
              </div>
              <div class="col-md-6">
                  <div class="position-relative form-group"> 
                      <span for="txtdetail">URL</span>
                      <input id="txtdetail" type="text" class="form-control">
                  </div> 
              </div>
           </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onClick="ajax_savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>                     
<input id="hdfid" type="hidden" />   


<script type="text/javascript">
    jQuery(document).ready(function ($) { 
        $("#modal-lg").on('hidden.bs.modal', function (e) {
            $('#hdfid').val("");
            $('#txtname').val("");
            $('#txtdetail').val("");
        });
     
       
        $('#tbdata').DataTable( {
            "ajax": {
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/video/search"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':1},
                dataType: "json"
            },
            "columns": [
                { "data": "name" },
                { "data": "detail" },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="แก้ไข" onclick="setUpdate('+full.id+')" class="badge bg-primary cursor-pointer"><i class="fas fa-edit"></i></span>';
                     },
                    className: "dt-body-center"
                },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="แก้ไข" onclick="setDelete('+full.id+')" class="badge bg-danger cursor-pointer"><i class="fas fa-trash-alt"></i></span>';
                     },
                    className: "dt-body-center"
                }
            ],
           //pageLength: 10,
           //"bLengthChange": false
        });
        $('#btnAdd').click(function () {
            $(".modal-title").html("เพิ่มข้อมูล");  
            $("#modal-lg").modal('show');
        });
        
       
    });
    function setUpdate(id) {     
        $(".modal-title").html("แก้ไขข้อมูล");  
        $("#modal-lg").modal('show');
        $("#exampleModalCenter").modal('show');
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/video/getdata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {   
                    $('#hdfid').val(data.id);	                 
                    $('#txtname').val(data.name);
                    $('#txtdetail').val(data.detail);
                    $("#exampleModalCenter").modal('hide');
                    
                }else{
                    alert(data.msg);
                } 
            }
        });	
    }
    async function ajax_savedata() 
    {
        var id=$('#hdfid').val();
        var name=$('#txtname').val();
        var detail =$('#txtdetail').val();
        
        if(name=='')
        {
            alert('กรุณากรอกชื่อ');
            return;
        }    
        if(detail=='')
        {
            alert('กรุณากรอก URL');
            return;
        }
        $("#modal-lg").modal('hide');
        $("#exampleModalCenter").modal('show');
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/video/savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','detail':detail,'name':name,'id':id},
            dataType: "json",				
            success: function (data) 
            {
                if (data.status=='success') {                     
                    $('#tbdata').DataTable().ajax.reload();
                    $("#exampleModalCenter").modal('hide');
                }
                else
                {
                    alert(data.msg);
                    $("#exampleModalCenter").modal('hide');
                } 
            }
        });
    }	
    
    
    
    
    function btclick(){
	   $("#paperclip").click();
    }
    function setDelete(id) {   
        var r = confirm("คุณต้องการลบข้อมูลนี้ใช่หรือไม่ !");

        if (r == true) {
            $("#exampleModalCenter").modal('show');
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/video/deletedata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {	
                        $('#tbdata').DataTable().ajax.reload();
                        $("#exampleModalCenter").modal('hide');
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
        }
    }	
    
</script>
