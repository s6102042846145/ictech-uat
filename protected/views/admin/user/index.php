<?php
	$this->pageTitle = 'ข้อมูลผู้ใช้' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>


 <!-- Main content -->
<section class="row content mt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <div class="card">
            <div class="card-header thsarabunnew">
            <label class="card-title">ข้อมูลผู้ใช้</label>

            <div class="card-tools">
              <ul class="pagination pagination-sm float-right">
                  <li class="page-item"><a class="page-link" id="btnAdd" href="javascript:void(0)">Add</a></li>
              </ul>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <div class="table-responsive">
                <table id="tbdata" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>ชื่อ - นามสกุล</th>
                            <th>Username</th>
                            <!--th>Password</th>-->
                            <th>หน่วยงาน</th>
                            <th>เบอร์โทร</th>
                            <!--th>อีเมล์</th-->
                            <!--<th>ที่อยู่</th>-->
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table>
              </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>    

 <div class="modal fade" id="modal-lg" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header thsarabunnew">
          <label class="modal-title"></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body thsarabunnew">
          <div class="form-row">
              <div class="col-md-6"> 
                  <div class="position-relative form-group"> 
                      <span for="txtfname">ชื่อ</span><span class="pl-1 text-red font-weight-bold">*</span>
                      <input id="txtfname" type="text" class="form-control" autocomplete="off" maxlength="50" />        
                  </div> 
              </div>
              <div class="col-md-6"> 
                  <div class="position-relative form-group"> 
                      <span for="txtlname">นามสกุล</span><span class="pl-1 text-red font-weight-bold">*</span>
                      <input id="txtlname" type="text" class="form-control" autocomplete="off" maxlength="50" />        
                  </div> 
              </div>
              <div class="col-md-6"> 
                  <div class="position-relative form-group"> 
                      <span for="txtuser">Username</span><span class="pl-1 text-red font-weight-bold">*</span>
                      <input id="txtuser" type="text" class="form-control" autocomplete="off" maxlength="50" />        
                  </div> 
              </div>
              <div class="col-md-6"> 
                  <div class="position-relative form-group"> 
                      <span for="txtpass">Password</span><span class="pl-1 text-red font-weight-bold">*</span>
                      <input id="txtpass" type="password" class="form-control" autocomplete="off" maxlength="50" />        
                  </div> 
              </div>
              <div class="col-md-8"> 
                  <div class="position-relative form-group"> 
                      <span for="drpdepartment">หน่วยงาน</span><span class="pl-1 text-red font-weight-bold">*</span>
                       <select class="form-control dropdown" id="drpdepartment">
                            <option value="">--เลือก--</option>
                            <?php 					
                                $data=lookupdata::getDepartment();
                                foreach($data as $dataitem) 
                                {
                                    echo "<option value='".$dataitem['id']."'>".$dataitem['name']."</option>";
                                } 
                            ?>
                        </select>       
                  </div> 
              </div>
              <div class="col-md-4"> 
                  <div class="position-relative form-group"> 
                      <span for="txttel">เบอร์โทร</span>
                      <input id="txttel" type="text" class="form-control" autocomplete="off" maxlength="10" />        
                  </div> 
              </div>
              <div class="col-md-8"> 
                  <div class="position-relative form-group"> 
                      <span for="txtaddress">ที่อยู่</span>                      
                      <input id="txtaddress" type="text" class="form-control" autocomplete="off" maxlength="200" />        
                  </div> 
              </div>
              <div class="col-md-4"> 
                  <div class="position-relative form-group"> 
                      <span for="txtemail">อีเมล์</span>
                      <input id="txtemail" type="text" class="form-control" autocomplete="off" maxlength="100" />        
                  </div> 
              </div>
              <div class="col-md-12"> 
                  <div class="position-relative form-group"> 
                      <span for="txtline">User Line</span><span style="font-size: xx-small;" class="pl-1 text-red font-weight-bold"> (ได้จากการ Add Line ระบบ ICTech ไม่ใช่ IDLine)</span>
                      <textarea class="form-control" id="txtline" rows="3"  maxlength="200"></textarea>
                  </div> 
              </div>
              <div class="col-md-12"> 
                  <div class="position-relative form-group"> 
                      <span for="txtremark">หมายเหตุ</span>
                      <textarea class="form-control" id="txtremark" rows="3"  maxlength="200"></textarea>
                  </div> 
              </div>
           </div>
           
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onClick="ajax_savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>                     
<input id="hdfid" type="hidden" />                           
<input id="hdffilepath" type="hidden" />  


<script type="text/javascript">
    jQuery(document).ready(function ($) { 
       
       
        $("#modal-lg").on('hidden.bs.modal', function (e) {
            $('#hdfid').val("");
            $('#txtfname').val("");
            $('#txtlname').val("");
            $('#txtuser').val("");
            $('#txtpass').val("");
            $('#drpdepartment').val("");
            $('#txttel').val("");
            $('#txtaddress').val("");
            $('#txtemail').val("");
            $('#txtremark').val("");
            $('#txtline').val("");
        });
     
       
        $('#tbdata').DataTable( {
            "ajax": {
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/user/search"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>'},
                dataType: "json"
            },
            "columns": [
                { "data": "fullname" },
                { "data": "username" },
                //{ "data": "password" },
                { "data": "department" },{ "data": "mobile" },
                //{ "data": "email" },
                //{ "data": "address" },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="แก้ไข" onclick="setUpdate('+full.id+')" class="badge bg-primary cursor-pointer"><i class="fas fa-edit"></i></span>';
                     },
                    className: "dt-body-center"
                },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="แก้ไข" onclick="setDelete('+full.id+')" class="badge bg-danger cursor-pointer"><i class="fas fa-trash-alt"></i></span>';
                     },
                    className: "dt-body-center"
                }
            ],
            pageLength: 40,
           "bLengthChange": false
        });
        $('#btnAdd').click(function () {
            $(".modal-title").html("เพิ่มข้อมูล");  
            $("#modal-lg").modal('show');
        });
        
       
    });
    function setUpdate(id) {     
        $(".modal-title").html("แก้ไขข้อมูล");  
        $("#modal-lg").modal('show');
        //$("#exampleModalCenter").modal('show');
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/user/getdata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') { 
                    var param = data.data[0];
                    //console.log(data.data[0].fname);return;
                    $('#hdfid').val(param.id);	                 
                    $('#txtfname').val(param.fname);
                    $('#txtlname').val(param.lname);
                    $('#txtuser').val(param.username);
                    $('#txtpass').val(param.password);
                    $('#drpdepartment').val(param.department_id);
                    $('#txttel').val(param.mobile);
                    $('#txtaddress').val(param.address);
                    $('#txtemail').val(param.email);
                    $('#txtremark').val(param.remark);
                    $('#txtline').val(param.line);
                    //$("#exampleModalCenter").modal('hide');
                }else{
                    alert(data.msg);
                } 
            }
        });	
    }
    function ajax_savedata() 
    {
        var id=$('#hdfid').val();
        var fname=$('#txtfname').val();
        var lname=$('#txtlname').val();
        var username=$('#txtuser').val();
        var password=$('#txtpass').val();
        var department=$('#drpdepartment').val();
        var department_name=$("#drpdepartment option:selected").text();
        var tel=$('#txttel').val();
        var address=$('#txtaddress').val();
        var email=$('#txtemail').val();
        var remark=$('#txtremark').val();
        var line = $('#txtline').val();
        
        
        if(fname==''){ alert('กรุณากรอกชื่อ');return;}    
        if(lname==''){ alert('กรุณากรอกนามสกุล');return;}    
        if(username==''){ alert('กรุณากรอก username');return;}    
        if(password==''){ alert('กรุณากรอก password');return;}    
        if(department==''){ alert('กรุณาเลือกหน่วยงาน');return;}    
        //if(tel==''){ alert('กรุณากรอกเบอร์โทร');return;}    
        //if(address==''){ alert('กรุณากรอกที่อยู่');return;}    
        //if(email==''){ alert('กรุณากรอกอีเมล์');return;}  
        //if(remark==''){ alert('กรุณากรอกชื่อ');return;}  
        
        
            
        $("#modal-lg").modal('hide');
        //$("#exampleModalCenter").modal('show');
         
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/user/savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'fname':fname,'lname':lname,'username':username,'password':password,'department':department,'department_name':department_name,'tel':tel,'address':address,'email':email,'remark':remark,'line':line},
            dataType: "json",				
            success: function (data) 
            {
                if (data.status=='success') {                     
                    $('#tbdata').DataTable().ajax.reload();
                    //$("#exampleModalCenter").modal('hide');
                }
                else
                {
                    alert(data.msg);
                    //$("#exampleModalCenter").modal('hide');
                } 
            }
        });
    }	
    
    
    
    
    function btclick(){
	   $("#paperclip").click();
    }
    function setDelete(id) {   
        var r = confirm("คุณต้องการลบข้อมูลนี้ใช่หรือไม่ !");

        if (r == true) {
            $("#exampleModalCenter").modal('show');
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/annualplan/deletedata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {	
                        $('#tbdata').DataTable().ajax.reload();
                        $("#exampleModalCenter").modal('hide');
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
        }
    }	
    
</script>