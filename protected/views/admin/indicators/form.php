<?php
	$this->pageTitle = 'รายละเอียดตัวชี้วัด' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>

  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/summernote/summernote-bs4.min.css">

<!-- Main content -->
<section class="content mt-3 thsarabunnew">
   
    <script>
        $().ready(function (){
            
            $("#chkHai").change(function() {
                //alert(this.checked)
                if(this.checked) {
                    //Do stuff
                    //alert(12)
                    $("#divHai.card-body").removeClass("d-none").addClass("d-block");
                }else{
                     //alert(13)
                    $("#divHai.card-body").removeClass("d-block").addClass("d-none");
                }
                
            });
            
            $('input[type=radio][name=radioTheme]').change(function() {
                if (this.value == '1') {              
                    //$("#divHai").removeClass("d-none");
                    $("#divTheme1").removeClass("d-none");
                    $("#divTheme2").addClass("d-none");
                    $("#divTheme3").addClass("d-none");
                    $("#divTheme4").addClass("d-none");
                }else if (this.value == '2') {
                    //$("#divHai").removeClass("d-none");
                    $("#divTheme1").addClass("d-none");
                    $("#divTheme2").removeClass("d-none");
                    $("#divTheme3").addClass("d-none");
                    $("#divTheme4").addClass("d-none");
                }else if (this.value == '3') {
                    //$("#divHai").removeClass("d-none");
                    $("#divTheme1").addClass("d-none");
                    $("#divTheme2").addClass("d-none");
                    $("#divTheme3").removeClass("d-none");
                    $("#divTheme4").addClass("d-none");
                }else if (this.value == '4') {
                    //$("#divHai").addClass("d-none");
                    $("#divTheme1").addClass("d-none");
                    $("#divTheme2").addClass("d-none");
                    $("#divTheme3").addClass("d-none");
                    $("#divTheme4").removeClass("d-none");
                }
            });
           //getHAI(); 
        });
    </script>
    <div id="divHaiCard" class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                <input type="checkbox" id="chkHai" />
                  <label for="chkHai">HAI/CAI</label>
              </h3>
            </div>
            <!-- /.card-header -->
            <div id="divHai" class="card-body d-none">
              <label>การติดเชื้อในรพ.(HAI)</label>
              <textarea id="txtHAI"></textarea>
              <label>การติดเชื้อในชุมชน (CAI)</label>
              <textarea id="txtCAI"></textarea>
            </div>
          </div>
        </div>
    <!-- /.col-->
    </div>
    
    <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                  <input type="radio" id="chkTheme1" name="radioTheme" value="1">
                  <label for="chkTheme1">รูปแบบที่ 1</label> 
                  <input type="radio" id="chkTheme2" name="radioTheme" value="2">
                  <label for="chkTheme2">รูปแบบที่ 2</label> 
                  <input type="radio" id="chkTheme3" name="radioTheme" value="3">
                  <label for="chkTheme3">รูปแบบที่ 3</label>
                   <input type="radio" id="chkTheme4" name="radioTheme" value="4">
                  <label for="chkTheme4">รูปแบบที่ 4</label>
              </h3>
            </div>
            <!-- /.card-header -->
            <div id="divTheme1" class="card-body d-none">
                <div class="row">
                   <div class="col-md-8">
                        <label for="txttheme1_faq0" class="form-label">ตัวชี้วัด</label>
                        <input type="text" class="form-control" id="txttheme1_faq0" />
                    </div>
                    <div class="col-md-4">
                        <label for="txttheme1_unit0" class="form-label">หน่วย</label>
                        <input type="text" class="form-control" id="txttheme1_unit0" />
                   </div> 
                    <div class="col-md-12" >
                        <label for="txttheme1_detail" class="form-label">Tooltip</label>
                        <textarea rows="4" cols="50" id="txttheme1_detail"></textarea>
                </div>
                </div>
            </div>
            <div id="divTheme2" class="card-body d-none">
                <div class="col-md-12" >
                    <label for="txttheme2_title" class="form-label">ตัวชี้วัด</label>
                    <input type="text" class="form-control" id="txttheme2_title" >
                    <label for="txttheme2_detail" class="form-label">Popup</label>
                    <textarea rows="4" cols="50" id="txttheme2_detail"></textarea>
                </div>
                <div class="col-md-12 text-right">
                    <button onClick="addFAQRowT2()" class="btn btn-warning">
                        เพิ่มคำถาม
                    </button>
                </div>
                <div class="t2 row">
                    <div class="col-md-8">
                        <label for="txttheme2_faq0" class="form-label">คำถาม</label>
                        <input type="text" class="form-control" id="txttheme2_faq0" />
                    </div>
                    <div class="col-md-4">
                        <label for="txttheme2_unit0" class="form-label">หน่วย</label>
                        <input type="text" class="form-control" id="txttheme2_unit0" />
                   </div> 
               </div>
            </div>
            <div id="divTheme3" class="card-body d-none">
                <div class="col-md-12" >
                    <label for="txttheme3_title" class="form-label">ตัวชี้วัด</label>
                    <input type="text" class="form-control" id="txttheme3_title" >
                    <label for="txttheme3_detail" class="form-label">Popup</label>
                    <textarea rows="4" cols="50" id="txttheme3_detail"></textarea>
                </div>
                <div class="col-md-12 text-right">
                    <button onClick="addFAQRowT3()" class="btn btn-warning">
                        เพิ่มคำถาม
                    </button>
                </div>
                <div class="t3 row">
                    <div class="col-md-8">
                        <label for="txttheme3_faq0" class="form-label">คำถาม</label>
                        <input type="text" class="form-control" id="txttheme3_faq0" />
                    </div>
                    <div class="col-md-4">
                        <label for="txttheme3_unit0" class="form-label">หน่วย</label>
                        <input type="text" class="form-control txtunit" id="txttheme3_unit0" />
                   </div> 
               </div>
                <div class="row mt-4">
                    <div class="col-md-8">
                        <label for="txtfaq_sub" class="form-label">หัวข้อคำถาม</label>
                        <input type="text" class="form-control" id="txtfaq_sub" />
                    </div>
                    <div class="col-md-4">
                        <!--
                        <label for="txtamount" class="form-label">จำนวน</label>
                        <input type="text" class="form-control" id="txtamount" />
                        -->
                   </div>
               </div>
               <div class="col-md-12 text-right">
                    <button onClick="addNumberRow()" class="btn btn-warning">
                        เพิ่มลำดับ
                    </button>
                </div>
                <div class="q2 row">
                    <div class="col-md-2">
                        <label for="txtnumber0" class="form-label">ลำดับ</label>
                        <input type="text" class="form-control text-center" id="txtnumber0" value="1" disabled />
                    </div>
                    <div class="col-md-10">
                        <label for="txtdetail0" class="form-label">รายละเอียด</label>
                        <input type="text" class="form-control" id="txtdetail0" />
                   </div>
               </div>
            </div>
            <div id="divTheme4" class="card-body d-none">
                <div class="col-md-12" >
                    <label for="txttheme4_title" class="form-label">ตัวชี้วัด</label>
                    <input type="text" class="form-control" id="txttheme4_title" >
                </div>
          </div>
             <div class="p-2 text-center">
                <button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">ย้อนกลับ</button>
                <button type="button" class="btn btn-primary" onClick="ajax_savedata();">บันทึก</button>
               
            </div>
        </div>
    <!-- /.col-->
    </div>
</section>


<!-- /.content -->
<input type="hidden" id="hdfid" />

<input type="hidden" id="hdfdatarow" />
<input type="hidden" id="hdfdatarow2" />
<input type="hidden" id="hdfdatarow3" />

<input type="hidden" id="hdfdatanumber" />
<input type="hidden" id="hdfindicator_id" />
<input type="hidden" id="hdftheme" />



<!-- Summernote -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/summernote/summernote-bs4.min.js"></script>



<script>
function ajax_savedata(){
                        
    var hai="";
    var cai="";
    var chkHai=0;
    if($('#chkHai').is(":checked")){
        hai =  $("#txtHAI").val();
        cai =  $("#txtCAI").val();
        chkHai=1;
    }
    var theme = $('input:radio[name="radioTheme"]:checked').val();




    var i = 0;
    var j = 0;
    var id = $('#hdfid').val();
   // var data_row = $('#hdfdatarow').val();

    var data_num = $('#hdfdatanumber').val();
    var indicator_id = $('#hdfindicator_id').val();




    var faq_sub = $('#txtfaq_sub').val();


    var data_number = [];
    var data_detail = [];

    var data_faq = [];
    var data_unit = [];

    var data_row ="";
    if(theme!=1){
        data_row = $("#hdfdatarow"+ theme).val();
    }

    var name = $('#txttheme2_title').val();
    var title_detail = $("#txttheme2_detail").val();
    if(theme==1){
        name = $('#txttheme1_title').val();
        title_detail = $("#txttheme1_detail").val();
    }
    if(theme==3){
        name = $('#txttheme3_title').val();
        title_detail = $("#txttheme3_detail").val();
    }
    if(theme==4){
        name = $('#txttheme4_title').val();
    }
    if(theme!=4){
        if(data_row==''){ data_row=1}
        while(i <= data_row)
        {          

            var faq = '#txttheme'+theme+'_faq'+i;
            var unit = '#txttheme'+theme+'_unit'+i;
            //console.log(faq);return;
            data_faq[i] = $(faq).val();
            data_unit[i] = $(unit).val();

            i++;
        }


        if(data_num==''){ data_num=1}
        while(j <= data_num)
        {
            var number = '#txtnumber'+j;
            var detail = '#txtdetail'+j;

            data_number[j] = $(number).val();
            data_detail[j] = $(detail).val();

            j++;
        }
    }
    //return;
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/indicators/savedatadetail"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'name':name,'indicator_id':indicator_id,'theme':theme,'title_detail':title_detail,'faq_sub':faq_sub,'data_faq':data_faq,'data_unit':data_unit,'data_number':data_number,'data_detail':data_detail,'chkHai':chkHai,'hai':hai,'cai':cai},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {	
                //getSearch();	

                alert('success');
                window.location.href="/admin/indicators/";
                //showdata();	
                //$("#modaldetail").modal('hide'); 

                //fileimport();
            }else{
                //$('#pleaseWaitDialog').modal('hide');
                alert(data.msg);
            } 
        }
    });

}    
$().ready(function (){
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    };
    
    var id = getUrlParameter('id');
    var hai = getUrlParameter('hai');
    var theme = getUrlParameter('theme');
    
    //$("input[name=radioTheme][value=" + theme + "]").attr('checked', 'checked');
    if(hai=="true"){  
        $("#divHai.card-body.d-none").removeClass("d-none").addClass("d-block");
        $("#chkHai").click();                        
    }

    
    if(theme=="1"){        
         $("#chkTheme1").click()         
    }
    if(theme=="2"){        
        $("#chkTheme2").click()
    }
    if(theme=="3"){        
        $("#chkTheme3").click()
    }
    if(theme=="4"){        
        $("#chkTheme4").click()
    }
    if(id){
        $("#hdfid").val(id);
        $("input[name=radioTheme]").attr("disabled",true);
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/indicators/SetData"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') { 
                    if(hai=="true"){  
                        $("#txtHAI").summernote("code", data.d_hai);
                        $("#txtCAI").summernote("code", data.d_cai);
                    }
                    
                    $("#txttheme"+data.theme+"_detail").summernote("code",data.tooltip);
                    if(data.theme==1){
                        $("#txttheme1_faq0").val(data.name);
                        $("#txttheme1_unit0").val(data.unit);
                    }
                    //console.log(data.data_theme2);return;
                    if(data.theme==2){
                        $("#txttheme2_title").val(data.name);
                        var html = "";
                        $.each(data.data_theme, function(i, item) {
                            //alert(data.question[i].id);
                            html+= '<div class="col-md-8"><label for="txttheme2_faq'+ data.data_theme[i].id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txttheme2_faq'+ data.data_theme[i].id +'" value="'+ data.data_theme[i].name +'" /></div><div class="col-md-4"><label for="txttheme2_unit'+ data.data_theme[i].id +'" class="form-label">หน่วย</label><input type="text" class="form-control" id="txttheme2_unit'+ data.data_theme[i].id +'" value="'+ data.data_theme[i].unit +'" /></div> ';
                            
                            $('#hdfdatarow2').val(data.data_theme[i].id);

                        });
                        $('.t2').html(html);
                    }
                    if(data.theme==3)
                    {
                        $("#txttheme3_title").val(data.name);
                        var html = "";
                        $.each(data.data_theme, function(i, item) {
                            //alert(data.question[i].id);
                            html+= '<div class="col-md-8"><label for="txttheme3_faq'+ data.data_theme[i].id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txttheme3_faq'+ data.data_theme[i].id +'" value="'+ data.data_theme[i].name +'" /></div><div class="col-md-4"><label for="txttheme3_unit'+ data.data_theme[i].id +'" class="form-label">หน่วย</label><input type="text" class="form-control" id="txttheme3_unit'+ data.data_theme[i].id +'" value="'+ data.data_theme[i].unit +'" /></div> ';
                            
                            $('#hdfdatarow3').val(data.data_theme.length);

                        });
                        $('.t3').html(html);
                        
                        var html2 = "";
                        $("#txtfaq_sub").val(data.title);
                        //console.log(data.data_number.length);
                        $.each(data.data_number, function(i, item) {
                            //alert(data.question[i].id);
                            html2+= '<div class="col-md-2 "><label for="txtnumber'+ data.data_number[i].id +'" class="form-label">ลำดับ</label><input type="text" class="form-control text-center" id="txtnumber'+ data.data_number[i].id +'" value="'+ data.data_number[i].item_order +'" disabled /></div><div class="col-md-10"><label for="txtdetail'+ data.data_number[i].id +'" class="form-label">รายละเอียด</label><input type="text" class="form-control" value="'+ data.data_number[i].name +'" id="txtdetail'+ data.data_number[i].id +'" /></div>';

                            $('#hdfdatanumber').val(data.data_number.length);	

                        });
                        $('.q2').html(html2);
                        
                    }
                    if(data.theme==4){
                        $("#txttheme4_title").val(data.name);
                    }
                    
                    //$("#txtfaq0").val(data.name);
                    //$("#txtunit0").val(data.unit);
                }
            }
         });
    }
    //$('#chkHai').prop('checked', hai);
   
    
   
     $('#modal-faq').on('hidden.bs.modal', function (e) {
        //$('input:radio[name="theme"]').val("");
         /*
        $("#step-1").removeClass("d-none");
        $("#step-2").addClass("d-none");
        $("#step-3").addClass("d-none");
        $("#step-4").addClass("d-none");
        $('input:radio[name="theme"]').prop('checked', false);
        $("#hdfid").val("");
        $("#btnSave").addClass("d-none");
        $("#btnNext").removeClass("d-none");
        */
         $("#exampleModalCenter").modal("show");
         window.location.reload();
     });

    
    // Summernote
    $('#txtHAI').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    $('#txtCAI').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    $('#txttheme1_detail').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    $('#txttheme2_detail').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    $('#txttheme3_detail').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        }
    });
    
    
    $('#btnAddTitle').click(function () {
        $(".modal-title").html("เพิ่มข้อมูล");  
        $("#modal-title").modal('show');
    });
    $('#btnAddFAQ').click(function () {
        $(".modal-title").html("เลือกเทมเพลต");  
        $("#modal-faq").modal('show');
     });
});   

function getHAI(){
    var indicator_id = getUrlParameter('indicators');
    $('#hdfindicator_id').val(indicator_id);
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setHAI"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','indicator_id':indicator_id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {		
                $("#txtHAI").summernote("code", data.hai);
                $("#txtCAI").summernote("code", data.cai);
            }
            else{
                alert(data.msg);
            } 
        }
    });
}
function saveHai(){
    var hai =  $("#txtHAI").val();
    var cai =  $("#txtCAI").val();
    var id =  getUrlParameter('indicators');
    
    //alert(id+hai);return;
    //alert(hai);return;
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/saveHAI"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'hai':hai,'cai':cai},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {
                alert("บันทึกข้อมูลสำเร็จ !");
            }
            else{
                alert(data.msg);
            } 
        }
    });
}
    
function setUpdateTitle(el) 
{    var id = $(el).parent().attr("data-id"); 
    
     $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setUpdateTitle"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') {     
                $("#txttitle").val(data.name);
                $("#txttitle_detail").summernote({
                    height: 150,   //set editable area's height
                    codemirror: { // codemirror options
                    theme: 'monokai'
                  }
                });
                
                $("#txttitle_detail").summernote("code", data.detail);
                $(".modal-title").html("แก้ไขข้อมูล");  
                $("#modal-title").modal('show');
            }else{
                alert(data.msg);
            } 
        }
    });	
}   
function setUpdateFAQ(el) 
{    
    var id = $(el).parent().attr("data-id");     
     $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setUpdateFAQ"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
        dataType: "json",				
        success: function (data) {
            if (data.status=='success') { 
                $("#step-1").addClass("d-none");
                if(data.theme=="1"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").addClass("d-none");
                }
                if(data.theme=="2"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").removeClass("d-none");
                }
                if(data.theme=="3"){
                    $("#step-2").removeClass("d-none");
                    $("#step-3").removeClass("d-none");
                    $("#step-4").removeClass("d-none");
                }
                
                $("#btnNext").addClass("d-none");
                $("#btnSave").removeClass("d-none");
                
                $("#hdfid").val(data.id);
                $("#hdftheme").val(data.theme);
                //$('input:radio[name="theme"]').val(data.theme);
                $("#txttitle_detail").summernote("code", data.tooltip);
                $("#txttitle").val(data.name);
                $("#txtunit").val(data.unit);
                
                var html = "";
                $.each(data.question, function(i, item) {
                    //alert(data.question[i].id);
                    html+= '<div data-ids="'+ data.question[i].id +'" class="col-md-8 "><label for="txtfaq'+ data.question[i].id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txtfaq'+ data.question[i].id +'" value="'+ data.question[i].name +'" /></div><div class="col-md-4 "><label for="txtunit'+ data.question[i].id +'" class="form-label">หน่วย</label><input type="text" class="form-control" id="txtunit'+ data.question[i].id +'" value="'+ data.question[i].unit +'" /></div>';
    
                    $('#hdfdatarow').val(data.question[i].id);
                    
                });
                $('.q1').html(html);
                if(data.theme!="1"){
                    var html2 = "";
                    $("#txtfaq_sub").val(data.title);
                    $.each(data.question2, function(i, item) {
                        //alert(data.question[i].id);
                        html2+= '<div data-id="'+ data.question2[i].id +'" class="col-md-2 "><label for="txtnumber'+ data.question2[i].id +'" class="form-label">ลำดับ</label><input type="text" class="form-control text-center" id="txtnumber'+ data.question2[i].id +'" value="'+ data.question2[i].item_order +'" disabled /></div><div class="col-md-10"><label for="txtdetail'+ data.question2[i].id +'" class="form-label">รายละเอียด</label><input type="text" class="form-control" value="'+ data.question2[i].name +'" id="txtdetail'+ data.question2[i].id +'" /></div>';

                        $('#hdfdatanumber').val(data.question2[i].id);	

                    });
                    $('.q2').html(html2);
                }
                $(".modal-title").html("แก้ไขข้อมูล");  
                $("#modal-faq").modal('show');
            }else{
                alert(data.msg);
            } 
        }
    });	
}    
function next_step(){
    
    var theme = $('input:radio[name="theme"]:checked').val();
    //alert(theme)
    if(theme=="1"){
        $("#step-2").removeClass("d-none");
        $("#step-3").addClass("d-none");
    }else if(theme=="2"){
        $("#step-2").removeClass("d-none");
        $("#step-3").removeClass("d-none");
    }else if(theme=="3"){
         $("#step-2").removeClass("d-none");
        $("#step-3").removeClass("d-none");
        $("#step-4").removeClass("d-none");
    }else{
        alert("กรุณาเลือกเทมเพลต");
        return;
    }
    $(".modal-title").html("เพิ่มข้อมูล"); 
    $("#step-1").addClass("d-none");
    $("#btnSave").removeClass("d-none");
    $("#btnNext").addClass("d-none");
}   


    
function addFAQRowT2()
{
	var count = $('#hdfdatarow2').val();	
	if(count==''){
		count=1;
	}
	
	//return;
	var id = Number(count)+1;
	//alert(i);
    
    $('.t2').append('<div class="col-md-8 "><label for="txttheme2_faq'+ id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txttheme2_faq'+ id +'" /></div><div class="col-md-4 "><label for="txttheme2_unit'+ id +'" class="form-label">หน่วย</label><input type="text" class="form-control" id="txttheme2_unit'+ id +'" /></div> ');
    
    $('#hdfdatarow2').val(id);
   
	
}   
function addFAQRowT3()
{
	var count = $('#hdfdatarow3').val();	
	if(count==''){
		count=1;
	}
	
	//return;
	var id = Number(count)+1;
    $('.t3').append('<div class="col-md-8 "><label for="txttheme3_faq'+ id +'" class="form-label">คำถาม</label><input type="text" class="form-control" id="txttheme3_faq'+ id +'" /></div><div class="col-md-4 "><label for="txttheme3_unit'+ id +'" class="form-label">หน่วย</label><input type="text" class="form-control" id="txttheme3_unit'+ id +'" /></div> ');
    
    $('#hdfdatarow3').val(id);
}    
function addNumberRow(){
    var count = $('#hdfdatanumber').val();	
	if(count==''){
		count=1;
	}
	
	//return;
	var id = Number(count)+1;
	//alert(i);
    
    $('.q2').append('<div data-id="'+ id +'" class="col-md-2 "><label for="txtnumber'+ id +'" class="form-label">ลำดับ</label><input type="text" class="form-control text-center" id="txtnumber'+ id +'" value="'+ id +'" disabled /></div><div class="col-md-10"><label for="txtdetail'+ id +'" class="form-label">รายละเอียด</label><input type="text" class="form-control" id="txtdetail'+ id +'" /></div>');
    
    $('#hdfdatanumber').val(id);	
}    
</script>