<?php
	$this->pageTitle = 'สรุปข้อมูลตัวชี้วัด' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>


 <!-- Main content -->
<section class="row content mt-3">
    <style>


table {
  table-layout: fixed;
  width:100%;
}
td:first-child, th:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:white;
     width: 300px;
}
        
td:last-child, th:last-child {
  position:sticky;
  right:0;
  z-index:1;
  background-color:white;
}
thead tr th {
  position:sticky;
  top:0;
}
th:first-child, th:last-child {
    z-index:2;
    /*background-color:red;*/
}
    </style>
    <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header thsarabunnew">
                <label class="card-title">สรุปข้อมูลตัวชี้วัดรายวัน รายเดือน รายปี</label>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <div class="position-relative form-group"> 
                  <label class="card-title thsarabunnew">ตัวชี้วัด</label>
                   <select class="form-control dropdown thsarabunnew" id="drpindicators">
                       <option value="0">--ทั้งหมด--</option>
                        <?php 					
                            $data=lookupdata::getReportIndicators();
                            foreach($data as $dataitem) 
                            {
                                echo "<option value='".$dataitem['id']."'>".$dataitem['name']."</option>";
                            } 
                        ?>
                    </select>       
              </div>               
              <label class="card-title thsarabunnew">สรุปรายงานตัวชี้วัด (รายวัน)</label>
              <!--div class="input-group input-group-custom input-group-sm no-wrap mb-2 ">
                   <input id="txtdate" autocomplete="off" class="form-input mr-2 form-control-has-validation datepicker" data-provide="datepicker"> 
                  <button onClick="Search('')" class="btn btn-dark btn-darkest btn-sm" type="button">Search</button>
              </div--> 
              <div class="table-responsive">
                <table id="tbday" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        
                    </thead>
                    <tbody></tbody>
                </table>
                  
              </div>
              <label class="card-title thsarabunnew">สรุปรายงานตัวชี้วัด (รายเดือน)</label>
              <div class="table-responsive">
                <table id="tbmouth" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>ชื่อ - นามสกุล</th>
                            <th>Username</th>
                            <!--th>Password</th>-->
                            <th>หน่วยงาน</th>
                            <th>เบอร์โทร</th>
                            <!--th>อีเมล์</th-->
                            <!--<th>ที่อยู่</th>-->
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table>
              </div>
              <label class="card-title thsarabunnew">สรุปรายงานตัวชี้วัด (รายปี)</label>
              <div class="table-responsive">
                <table id="tbyear" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>ชื่อ - นามสกุล</th>
                            <th>Username</th>
                            <!--th>Password</th>-->
                            <th>หน่วยงาน</th>
                            <th>เบอร์โทร</th>
                            <!--th>อีเมล์</th-->
                            <!--<th>ที่อยู่</th>-->
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table>
              </div>
              
              
              
              <div class="table-responsive">
                <table id="tbdata" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>ชื่อ - นามสกุล</th>
                            <th>Username</th>
                            <!--th>Password</th>-->
                            <th>หน่วยงาน</th>
                            <th>เบอร์โทร</th>
                            <!--th>อีเมล์</th-->
                            <!--<th>ที่อยู่</th>-->
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table>
              </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>    

                     
<input id="hdfid" type="hidden" />                           
<input id="hdffilepath" type="hidden" />  


<script type="text/javascript">
    jQuery(document).ready(function ($) { 
        getData("");
        $("#drpindicators").change(function () {
            var id = this.value;
            getData(id);
        });
    });
    function getData(el) { 
        var id="";
        if(el==""){
            id = $("#drpindicators").val();
        }else{
            id = el;
        }
        //alert(id);
        var titleday= $("#drpindicators option:selected" ).text();
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/indicatorsummary/getdata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') { 
                    var day = data.data;
                    var cnt = data.data.length;
                    var amount = 0;
                    var body = "<tr>";
                    body+= "<td class='text-right'>จำนวนครั้งของ<span id='sptitleday'>"+titleday+"</span></td>";
                    
                    var head = "<tr>";
                    head+= "<th width='300px' class='text-right'>วันที่</th>";
                    for (let i = 0; i < cnt; i++) {   
                        amount = amount + parseInt(day[i]['num']);
                        head+= "<th style='width: 50px;' class='text-center'>"+ parseInt(day[i]['date']) + "</th>";
                        body+= "<td style='width: 50px;' class='text-center'>"+ day[i]['num'] + "</td>";
                        /*
                        if(i<5){
                           head+= "<th style='width: 30px;' class='text-center'>"+ parseInt(day[i]['date']) + "</th>";
                            body+= "<td style='width: 30px;' class='text-center'>"+ day[i]['num'] + "</td>";
                        }
                        if(i==6){
                            head+= "<th style='width: 50px;' class='text-center'>...</th>";
                             body+= "<td style='width: 50px;' class='text-center'>...</td>";
                        }
                        if((i+1)==cnt){
                           head+= "<th style='width: 50px;' class='text-center'>"+ parseInt(day[i]['date']) + "</th>";
                           body+= "<td style='width: 50px;' class='text-center'>"+ day[i]['num'] +"</td>";
                        }
                        */
                    }
                    
                    
                    head+="<th style='width: 60px;' class='text-center'>รวม</th></tr>";
                    body+="<td style='width: 60px;' class='text-center'>"+amount+"</td></tr>";
                    body+="<tr><td style='border-right: 1px solid white;' class='text-right'>คิดเป็นร้อยละ</td><td colspan="+parseInt(cnt)+" class='text-right'></td><td class='text-center'>20</td></tr>";
                    $("#tbday thead").html(head);
                    body+="</tr>";
                    $("#tbday tbody").html(body);
                   
                }else{
                    alert(data.msg);
                } 
            }
        });	
    }
    function setUpdate(id) {     
        $(".modal-title").html("แก้ไขข้อมูล");  
        $("#modal-lg").modal('show');
        //$("#exampleModalCenter").modal('show');
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/user/getdata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') { 
                    var param = data.data[0];
                    //console.log(data.data[0].fname);return;
                    $('#hdfid').val(param.id);	                 
                    $('#txtfname').val(param.fname);
                    $('#txtlname').val(param.lname);
                    $('#txtuser').val(param.username);
                    $('#txtpass').val(param.password);
                    $('#drpdepartment').val(param.department_id);
                    $('#txttel').val(param.mobile);
                    $('#txtaddress').val(param.address);
                    $('#txtemail').val(param.email);
                    $('#txtremark').val(param.remark);
                    //$("#exampleModalCenter").modal('hide');
                }else{
                    alert(data.msg);
                } 
            }
        });	
    }
    function ajax_savedata() 
    {
        var id=$('#hdfid').val();
        var fname=$('#txtfname').val();
        var lname=$('#txtlname').val();
        var username=$('#txtuser').val();
        var password=$('#txtpass').val();
        var department=$('#drpdepartment').val();
        var department_name=$("#drpdepartment option:selected").text();
        var tel=$('#txttel').val();
        var address=$('#txtaddress').val();
        var email=$('#txtemail').val();
        var remark=$('#txtremark').val();
        
        
        
        if(fname==''){ alert('กรุณากรอกชื่อ');return;}    
        if(lname==''){ alert('กรุณากรอกนามสกุล');return;}    
        if(username==''){ alert('กรุณากรอก username');return;}    
        if(password==''){ alert('กรุณากรอก password');return;}    
        if(department==''){ alert('กรุณาเลือกหน่วยงาน');return;}    
        //if(tel==''){ alert('กรุณากรอกเบอร์โทร');return;}    
        //if(address==''){ alert('กรุณากรอกที่อยู่');return;}    
        //if(email==''){ alert('กรุณากรอกอีเมล์');return;}  
        //if(remark==''){ alert('กรุณากรอกชื่อ');return;}  
        
        
            
        $("#modal-lg").modal('hide');
        $("#exampleModalCenter").modal('show');
         
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/user/savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'fname':fname,'lname':lname,'username':username,'password':password,'department':department,'department_name':department_name,'tel':tel,'address':address,'email':email,'remark':remark},
            dataType: "json",				
            success: function (data) 
            {
                if (data.status=='success') {                     
                    $('#tbdata').DataTable().ajax.reload();
                    $("#exampleModalCenter").modal('hide');
                }
                else
                {
                    alert(data.msg);
                    $("#exampleModalCenter").modal('hide');
                } 
            }
        });
    }	
    
    
    
    
    function btclick(){
	   $("#paperclip").click();
    }
    function setDelete(id) {   
        var r = confirm("คุณต้องการลบข้อมูลนี้ใช่หรือไม่ !");

        if (r == true) {
            $("#exampleModalCenter").modal('show');
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/annualplan/deletedata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {	
                        $('#tbdata').DataTable().ajax.reload();
                        $("#exampleModalCenter").modal('hide');
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
        }
    }	
    
</script>