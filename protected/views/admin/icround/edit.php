<?php
	$this->pageTitle = 'Edit Infection Control Checklist' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<style>
th {
  vertical-align: inherit !important;
}
 #footer {
        position: fixed;
        bottom: 0px;
        left: 0px;
        
        padding: 10px;
        text-align: center;
        width: 100%;
        color: white;
    }
</style>
<script>
     jQuery(document).ready(function ($) {    
         
         $("div.container").eq(1).removeClass("container");
     });
</script>
 <!-- Main content -->
<section class="content mt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <div class="card">
            <div class="card-header thsarabunnew">
            <label class="card-title">แบบตรวจสอบคุณภาพงาน IC (INFECTION CONTROL CHECKLIST)</label>

            <div class="card-tools">
              <ul class="pagination pagination-sm float-right">
                  <li class="page-item"></li>
              </ul>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <div class="table-responsive thsarabunnew">
                <div class="mt-1">
                    <label class="text-bold">หน่วยงาน/หอผู้ป่วย : <span class="text-blue" id="spdepartment"></span></label>
                    <br/>
                    <label class="text-bold">ประจำเดือน/ปี : <span class="text-blue" id="spdate"></span></label>
                </div>
                <table id="tbdata" class="table table-bordered">
                    <thead class="" style="background-color: #ccc">
                        <tr>
                            <th rowspan="2" colspan="2" class="text-center">หัวข้อการประเมิน</th>
                            <th colspan="3" class="text-center">
                                <label>วันที่ <span class="text-blue" id="spday"></span></label>
                                <label>เดือน <span class="text-blue" id="spmonth"></span></label>
                                <label>ปี <span class="text-blue" id="spyear"></span></label>
                            </th>
                        </tr>
                        <tr>
                            <th style="width: 100px;" class="bg-gradient-green text-center">ผ่าน</th>
                            <th style="width: 100px;" class="bg-gradient-red text-center">ไม่ผ่าน</th>
                            <th class="text-center">คำแนะนำ</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
                  <div id="footer">
                      <a class="ml-2 btn btn-success" id="btnAdd" href="javascript:void(0)">บันทึกรายการ</a>
                  </div>
                <!--table id="tbdata" class="w-100 table table-bordered table-striped ">
                    <thead>
                        <tr>
                            <th>วันที่</th>
                            <th>หน่วยงาน</th>
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table-->
              </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>    

 
<input id="hdfid" type="hidden" />                           
<input id="hdfsave_date" type="hidden" />                           
<input id="hdfdatarow" type="hidden" />  
<input id="hdfdatarow2" type="hidden" />  

<script type="text/javascript">
    jQuery(document).ready(function ($) { 
       //$("#modal-md").modal('show');
         
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
        var id = getUrlParameter("id");
        var save_date = getUrlParameter("code");
        selectdepartment(id,save_date);
        
        $('#btnAdd').click(function () {
            ajax_savedata();
        });
    });
    function gotodepartment(){
        $("#modal-message2").modal("hide");
        $("#modal-md").modal("show");
    }
    function gotoic(){
        var department = $("#drpdepartment").val();
        window.location.href="/admin/department/ic?id="+department;        
    }
    function selectdepartment(department,save_date){
        //var department = $("#drpdepartment").val();
        $("#hdfid").val(department);
        $("#hdfsave_date").val(save_date);
        var monthNamesThai = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน",
"กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤษจิกายน","ธันวาคม"];

       
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/icround/SetData"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':department,'save_date':save_date},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') 
                { 
                    var dd = new Date(data.chk_data[0].create_date) ;
                    dd.setYear(dd.getFullYear() + 543);
                    
                    $("#spdepartment").text(data.department_data[0].name);
                   
                    $("#spdate").text(monthNamesThai[dd.getMonth()]+"  "+dd.getFullYear());
                    $("#spday").text(dd.getDate());
                    $("#spmonth").text(monthNamesThai[dd.getMonth()]);
                    $("#spyear").text(dd.getFullYear());
                    var html="";
                    let k=0;
                    let x=0;
                    let cnt=0;
                    for (let i = 0; i < data.data.length; i++) 
                    {
                         html+= '<tr><td colspan="5" class="text-bold">'+ data.data[i].title +'</td></tr>';
                        for(let j=0; j< data.data2.length; j++)
                        {
                            if(data.data[i].group_id == data.data2[j].group_id)
                            {
                                if(data.data2[j].flx==1)
                                {
                                    x++;
                                    var html2 = "";
                                    let xxx=0;
                                    let num=0;
                                    //console.log("daaa",data.data3.data.filter(elem => elem.icround_id === 25).length)
                                    num = data.data3.filter(v => v.icround_id === data.data2[j].id ).length; 
                                    cnt = cnt + num;         
                                        //console.log("data",k,"-",cnt)

                                    for(;k< cnt;)
                                    {
                                        if(data.data3[k].icround_id==data.data2[j].id){
                                            var chk_true =data.chk_sub_data[k].chk!=0? "checked":"";
                                            var chk_false =data.chk_sub_data[k].chk!=1? "checked":"";
                                            var remark =data.chk_sub_data[k].remark;
                                            if(xxx==0){
                                               
                                                html+= '<tr>';
                                                html+= '<td style="vertical-align: inherit;" rowspan="'+ num +'">'+ data.data2[j].name +'</td>';
                                                html+= '<td>'+ data.data3[k].name+'</td>';
                                                html+= '<td><input onchange="setUpdateICRoundListSub(this)" data-id="'+ data.data3[k].id +'" '+chk_true+' value="1" type="radio" name="radiosubcheck'+ data.data3[k].id+'" class="form-control" /></td>';
                                                html+= '<td><input onchange="setUpdateICRoundListSub(this)" data-id="'+ data.data3[k].id +'" '+chk_false+' value="0" type="radio" name="radiosubcheck'+ data.data3[k].id+'" class="form-control " /></td>';
                                                html+= '<td><input data-id="'+ data.data3[k].id +'" type="text" class="form-control" id="txtradiosubcheck'+ data.data3[k].id+'" value="'+remark+'" /></td>';
                                                html+= '</tr>';
                                            }else{
                                                html+= '<tr>';
                                                html+= '<td>'+ data.data3[k].name+'</td>';
                                                html+= '<td><input onchange="setUpdateICRoundListSub(this)" data-id="'+ data.data3[k].id +'" '+chk_true+' value="1" type="radio" name="radiosubcheck'+ data.data3[k].id+'" class="form-control" /></td>';
                                                html+= '<td><input onchange="setUpdateICRoundListSub(this)" data-id="'+ data.data3[k].id +'" '+chk_false+' value="0" type="radio" name="radiosubcheck'+ data.data3[k].id+'" class="form-control" /></td>';
                                                html+= '<td><input data-id="'+ data.data3[k].id +'" type="text" class="form-control" id="txtradiosubcheck'+ data.data3[k].id+'" value="'+remark+'" /></td>';
                                                html+= '</tr>';
                                            }
                                            xxx++;
                                       }
                                        k++;
                                    }
                                   // html+=html2;
                                }else{
                                   
                                     
                                    
                                     html+= '<tr><td colspan="2">'+ data.data2[j].name +'</td><td><input onchange="setUpdateICRoundList(this)" value="1" type="radio" name="radiocheck'+ data.data2[j].id+'" class="form-control" /></td><td><input onchange="setUpdateICRoundList(this)" value="0" type="radio" name="radiocheck'+ data.data2[j].id+'" class="form-control" /></td><td><input type="text" data-id="'+ data.data2[j].id +'" class="form-control" id="txtradiocheck'+ data.data2[j].id+'" /></td></tr>';
                                    x++;
                                }
                            }
                        }


                          //text += data.data[i] + "<br>";
                          //$("#txtname"+data.data[i].question_id).val(data.data[i].name);

                    }


                    $("#hdfdatarow").val(x);
                    $("#hdfdatarow2").val(cnt);  
                    $("#tbdata tbody").append(html);
                    
                     $.each(data.chk_data, function(key, value) {
                         $('#txtradiocheck'+value.icround_id).val(value.remark);
                         $('input[name=radiocheck'+value.icround_id+']').attr("data-id",value.id);
                         //$('#mydiv').attr("data-id","20");
                         if(value.chk!=0){                             
                             $('input[name=radiocheck'+value.icround_id+']').eq(0).attr('checked', true);
                         }else{
                             $('input[name=radiocheck'+value.icround_id+']').eq(1).attr('checked', true);
                         }

                    });
                }
            }
         });
              
        
            
    }
    function ajax_savedata() {
        var i = 1;
        var j = 1;
        var id = $('#hdfid').val();
        var save_date = $('#hdfsave_date').val();
        var data_check_list = [];
        var id_check_list = [];
        var data_sub_check_list = [];
        var id_sub_check_list = [];
        var data_comment_list = [];
        var data_sub_comment_list = [];
        
        
        var data_row_list = $("#hdfdatarow").val();
        var data_row_sub_list = $("#hdfdatarow2").val();
        //var department_id = $("#hdfdepartment").val();
        if(data_row_list==''){ data_row_list=1}
        while(i <= data_row_list)
        {       
            var list = $('input[name=radiocheck'+i+']:checked').val();
            data_check_list[i] = list==undefined ? null : list;
            id_check_list[i] = $('input[name=radiocheck'+i+']').attr("data-id");
            data_comment_list[i] = $('#txtradiocheck'+i).val();
            i++;
        }
        if(data_row_sub_list==''){ data_row_sub_list=1}
        while(j <= data_row_sub_list)
        {       
            var list = $('input[name=radiosubcheck'+j+']:checked').val();
            data_sub_check_list[j] = list==undefined ? null : list;
            id_sub_check_list[j] = $('input[name=radiosubcheck'+j+']').attr("data-id");
            data_sub_comment_list[j] = $('#txtradiosubcheck'+j).val();
            j++;
        }
        
   //console.log(id_check_list);return;
        
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/icround/Savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'data_check_list':data_check_list,'id_check_list':id_check_list,'data_sub_check_list':data_sub_check_list,'id_sub_check_list':id_sub_check_list,'data_comment_list':data_comment_list,'data_sub_comment_list':data_sub_comment_list,'status':"update"},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {	
                   // $('#tbdata').DataTable().ajax.reload();
                    $("#exampleModalCenter").modal('hide');
                }else{
                    $("#exampleModalCenter").modal('hide');
                    alert(data.msg);
                } 
            }
        });

    } 
    function setUpdateICRoundList(el){
        var id =$(el).attr('data-id');
        var chk = el.value;
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/icround/updatelist"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'chk':chk},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {	
                   // $('#tbdata').DataTable().ajax.reload();
                    //$("#exampleModalCenter").modal('hide');
                    //alert(data.status);
                }else{
                    //$("#exampleModalCenter").modal('hide');
                    alert(data.msg);
                } 
            }
        });

        
    }
    function setUpdateICRoundListSub(el){
        var id =$(el).attr('data-id');
        var chk = el.value;
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/icround/updatelistsub"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'chk':chk},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {	
                   // $('#tbdata').DataTable().ajax.reload();
                    //$("#exampleModalCenter").modal('hide');
                    //alert(data.status);
                }else{
                    //$("#exampleModalCenter").modal('hide');
                    alert(data.msg);
                } 
            }
        });

        
    }
</script>