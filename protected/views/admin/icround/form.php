<?php
	$this->pageTitle = 'แบบตรวจสอบคุณภาพงาน IC (INFECTION CONTROL CHECKLIST)' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>

<!-- Main content -->
<section class="content mt-3 thsarabunnew">
   
   
    
  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">
              <label>รายการตรวจสอบคุณภาพงาน IC (INFECTION CONTROL CHECKLIST)</label>
          </h3>
        </div>
        <!-- /.card-header -->
         <div class="card-body">
              <div class="table-responsive">
                <table id="tbdata" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>ประจำเดือน</th>
                            <th>หน่วยงาน</th>
                            <th>วันที่ทำรายการ</th>
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <!--th style="width:50px;" class="text-center">ลบ</th-->
                        </tr>
                    </thead>

                </table>
              </div>
          </div>
        <div class="card-footer">
        </div>
      </div>
    </div>
    <!-- /.col-->
  </div>
    
    
</section>


<!-- /.content -->
<input type="hidden" id="hdfid" />


<script>
    jQuery(document).ready(function ($) {         
        $('#tbdata').DataTable( {
            "ajax": {
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/icround/SearchData"); ?>",
                //url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/user/search"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>'},
                dataType: "json"
            },
            "columns": [
                { "data": "month_name" },
                { "data": "department_name" },
                { "data": "create_date" },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="แก้ไข" onclick=setUpdate('+full.id+',"'+full.code+'") class="badge bg-primary cursor-pointer"><i class="fas fa-edit"></i></span>';
                     },
                    className: "dt-body-center"
                },
                /*
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="ลบ" onclick="setDelete('+full.id+')" class="badge bg-danger cursor-pointer"><i class="fas fa-trash-alt"></i></span>';
                     },
                    className: "dt-body-center"
                }
                */
            ],
            pageLength: 40,
           "bLengthChange": false
        });
    });
    function setUpdate(id,code){
        window.location.href="/admin/icround/edit?id="+id+"&code="+code;    
    }
</script>