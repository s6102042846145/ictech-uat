<?php
	$this->pageTitle = 'Infection Control Checklist' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<style>
th {
  vertical-align: inherit !important;
}
 #footer {
        position: fixed;
        bottom: 0px;
        left: 0px;
        
        padding: 10px;
        text-align: center;
        width: 100%;
        color: white;
    }
</style>
<script>
     jQuery(document).ready(function ($) {    
         
         $("div.container").eq(1).removeClass("container");
     });
</script>
 <!-- Main content -->
<section class="content mt-3">
  <div class="container-fluid">
    <div class="row">
      <div id="get-risk" class="col-12">

        <div class="card">
            <div class="card-header thsarabunnew">
            <label class="card-title">แบบตรวจสอบคุณภาพงาน IC (INFECTION CONTROL CHECKLIST)</label>

            <div class="card-tools">
              <ul class="pagination pagination-sm float-right">
                  <li class="page-item"></li>
              </ul>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <div class="table-responsive thsarabunnew">
                <div class="mt-1">
                    <label class="text-bold">หน่วยงาน/หอผู้ป่วย : <span class="text-blue" id="spdepartment"></span></label>
                    <br/>
                    <label class="text-bold">ประจำเดือน/ปี : <span class="text-blue" id="spdate"></span></label>
                </div>
                <table id="tbdata" class="table table-bordered">
                    <thead class="" style="background-color: #ccc">
                        <tr>
                            <th rowspan="2" colspan="2" class="text-center">หัวข้อการประเมิน</th>
                            <th colspan="3" class="text-center">
                                <label>วันที่ <span class="text-blue" id="spday"></span></label>
                                <label>เดือน <span class="text-blue" id="spmonth"></span></label>
                                <label>ปี <span class="text-blue" id="spyear"></span></label>
                            </th>
                        </tr>
                        <tr>
                            <th style="width: 100px;" class="bg-gradient-green text-center">ผ่าน</th>
                            <th style="width: 100px;" class="bg-gradient-red text-center">ไม่ผ่าน</th>
                            <th class="text-center">คำแนะนำ</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
                  <div id="footer">
                      <button type="submit" onclick="location.reload();" class="btn btn-default" >ทำรายการใหม่</button>
                      <a class="ml-2 btn btn-success" id="btnAdd" href="javascript:void(0)">บันทึกรายการ</a>
                  </div>
                <!--table id="tbdata" class="w-100 table table-bordered table-striped ">
                    <thead>
                        <tr>
                            <th>วันที่</th>
                            <th>หน่วยงาน</th>
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table-->
              </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>    

 <div class="modal fade" id="modal-md" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header thsarabunnew">
          <label class="modal-title">เลือกหน่วยงาน</label>          
        </div>
        <div class="modal-body thsarabunnew">
          <div class="form-row">
              
              <div class="col-md-12"> 
                  <div class="position-relative form-group"> 
                      <span for="drpdepartment">หน่วยงาน</span><span class="pl-1 text-red font-weight-bold">*</span>
                       <select class="form-control dropdown" id="drpdepartment">
                            <option value="">--เลือก--</option>
                            <?php 					
                                $data=lookupdata::getDepartment();
                                foreach($data as $dataitem) 
                                {
                                    echo "<option value='".$dataitem['id']."'>".$dataitem['name']."</option>";
                                } 
                            ?>
                        </select>       
                  </div> 
              </div>
           </div>
           
        </div>
        <div class="modal-footer justify-content-end">
              <button type="button" class="btn btn-primary" onClick="selectdepartment();">Select</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>      
<div class="modal fade" id="modal-message" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header thsarabunnew">
          <label class="modal-title">แจ้งเตือน</label>          
        </div>
        <div class="modal-body thsarabunnew text-center">
           <i class="fa-6x fa-exclamation-circle fa-fw fas form-control-file text-warning"></i>
           <label>ไม่พบแบบตรวจสอบคุณภาพงานสำหรับหน่วยงาน <span class="text-blue" id="spdepartment_msg"></span></label>
        </div>
        <div class="modal-footer justify-content-end">
              <button class="btn btn-default" onclick="gotoic();">ตั้งค่า IC Round</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>   

<div class="modal fade" id="modal-message2" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header thsarabunnew">
          <label class="modal-title">แจ้งเตือน</label>          
        </div>
        <div class="modal-body thsarabunnew text-center">
           <i class="fa-6x fa-exclamation-circle fa-fw fas form-control-file text-warning"></i>
           <label>ท่านได้ทำการบันทึกข้อมูลของหน่วยงาน <span class="text-blue" id="spdepartment_msg2"></span> ประจำเดือน
            <span class="text-blue" id="spmouth"></span> ไปแล้วเมื่อวันที่ <span class="text-blue" id="spchkday"></span> โปรดเลือกหน่วยงานอื่น !
            </label>
        </div>
        <div class="modal-footer justify-content-end">
              <button class="btn btn-default" onclick="gotodepartment();">เลือกหน่วยงาน</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 
<input id="hdfid" type="hidden" />                           
<input id="hdfdatarow" type="hidden" />  
<input id="hdfdatarow2" type="hidden" />  
<input id="hdfdata_id" type="hidden" />   
<input id="hdfsub_data_id" type="hidden" />  
<script type="text/javascript">
    jQuery(document).ready(function ($) { 
       $("#modal-md").modal('show');
        
        
        $('#btnAdd').click(function () {
            ajax_savedata();
        });
    });
    function gotodepartment(){
        $("#modal-message2").modal("hide");
        $("#modal-md").modal("show");
    }
    function gotoic(){
        var department = $("#drpdepartment").val();
        window.location.href="/admin/department/ic?id="+department;        
    }
    function selectdepartment(){
        
        
        var department = $("#drpdepartment").val();
        $("#hdfid").val(department);
        var department_text = $("#drpdepartment option:selected").text();
        if(department==""){alert("กรุณาเลือกหน่วยงาน ! "); return;}
        $("#modal-md").modal('hide');
        $("#exampleModalCenter").modal('show');
        var monthNamesThai = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน",
"กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤษจิกายน","ธันวาคม"];

        var d = new Date();
        d.setYear(d.getFullYear() + 543);
        
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/icround/ChecksaveIC"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':department},
            dataType: "json",				
            success: function (data) {
                  //$("#modal-md").modal('hide');
                if(data.data[0].cnt>0){
                    
                    var dd = new Date(data.data[0].create_date) ;
                    //console.log("dd",dd);return;
                    dd.setYear(dd.getFullYear() + 543);
                    
                    $("#modal-message2").modal("show");
                    $("#exampleModalCenter").modal('hide');
                    $("#spdepartment_msg2").text(department_text);
                    $("#spmouth").text(monthNamesThai[dd.getMonth()]+"  "+dd.getFullYear());
                    $("#spchkday").text(dd.getDate()+ " " +monthNamesThai[dd.getMonth()]+"  "+dd.getFullYear());
                    return;
                    
                }else{
                   
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/icround/SetData"); ?>",
                        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':department},
                        dataType: "json",				
                        success: function (data) {
                            if (data.status=='success') 
                            { 
                                $("#modal-md").modal('hide');
                                if(data.data.length==0){           
                                    $("#exampleModalCenter").modal('hide');
                                    $("#spdepartment_msg").text(department_text);
                                    $("#modal-message").modal("show");
                                    
                                    return;
                                }
 
                                $("#spdepartment").text(department_text);
                                //return;

                                $("#spdate").text(monthNamesThai[d.getMonth()]+"  "+d.getFullYear());
                                $("#spday").text(d.getDate());
                                $("#spmonth").text(monthNamesThai[d.getMonth()]);
                                $("#spyear").text(d.getFullYear());
                                var html="";
                                let k=0;
                                let x=0;
                                let cnt=0;
                                var data_id=[];
                                var sub_data_id="";
                                for (let i = 0; i < data.data.length; i++) 
                                {
                                     html+= '<tr><td colspan="5" class="text-bold">'+ data.data[i].title +'</td></tr>';
                                    for(let j=0; j< data.data2.length; j++)
                                    {
                                        if(data.data[i].group_id == data.data2[j].group_id)
                                        {
                                            if(data.data2[j].flx==1)
                                            {
                                                x++;
                                                var html2 = "";
                                                let xxx=0;
                                                let num=0;
                                                //console.log("daaa",data.data3.data.filter(elem => elem.icround_id === 25).length)
                                                num = data.data3.filter(v => v.icround_id === data.data2[j].id ).length; 
                                                cnt = cnt + num;         
                                                    //console.log("data",k,"-",cnt)

                                                for(;k< cnt;)
                                                {
                                                    if(data.data3[k].icround_id==data.data2[j].id){
                                                        if(xxx==0){
                                                            html+= '<tr class="subdata">';
                                                            html+= '<td style="vertical-align: inherit;" rowspan="'+ num +'">'+ data.data2[j].name +'</td>';
                                                            html+= '<td>'+ data.data3[k].name+'</td>';
                                                            html+= '<td><input data-id="'+ data.data3[k].id +'" value="1" type="radio" name="radiosubcheck'+ data.data3[k].id+'" class="form-control" /></td>';
                                                            html+= '<td><input data-id="'+ data.data3[k].id +'" value="0" type="radio" name="radiosubcheck'+ data.data3[k].id+'" class="form-control " /></td>';
                                                            html+= '<td><input data-id="'+ data.data3[k].id +'" type="text" class="form-control" id="txtradiosubcheck'+ data.data3[k].id+'" /></td>';
                                                            html+= '</tr>';
                                                            //sub_data_id[xxx] = data.data3[k].id;
                                                        }else{
                                                            html+= '<tr class="subdata">';
                                                            html+= '<td>'+ data.data3[k].name+'</td>';
                                                            html+= '<td><input data-id="'+ data.data3[k].id +'" value="1" type="radio" name="radiosubcheck'+ data.data3[k].id+'" class="form-control" /></td>';
                                                            html+= '<td><input data-id="'+ data.data3[k].id +'" value="0" type="radio" name="radiosubcheck'+ data.data3[k].id+'" class="form-control" /></td>';
                                                            html+= '<td><input data-id="'+ data.data3[k].id +'" type="text" class="form-control" id="txtradiosubcheck'+ data.data3[k].id+'" /></td>';
                                                            html+= '</tr>';
                                                            //sub_data_id[xxx] = data.data3[k].id;
                                                        }
                                                        sub_data_id += data.data3[k].id+",";
                                                        xxx++;
                                                   }
                                                    k++;
                                                }
                                               // html+=html2;
                                            }else{
                                                 html+= '<tr  class="headdata"><td colspan="2">'+ data.data2[j].name +'</td><td><input data-id="'+ data.data2[j].id +'" value="1" type="radio" name="radiocheck'+ data.data2[j].id +'" class="form-control" /></td><td><input data-id="'+ data.data2[j].id +'" value="0" type="radio" name="radiocheck'+ data.data2[j].id +'" class="form-control" /></td><td><input type="text" data-id="'+ data.data2[j].id +'" class="form-control" id="txtradiocheck'+ data.data2[j].id +'"/></td></tr>';
                                                 data_id[x] = data.data2[j].id;
                                                x++;
                                            }
                                        }
                                    }


                                      //text += data.data[i] + "<br>";
                                      //$("#txtname"+data.data[i].question_id).val(data.data[i].name);

                                }


                                $("#hdfdata_id").val(data_id);
                                $("#hdfsub_data_id").val(sub_data_id);
                                $("#hdfdatarow").val(x);
                                $("#hdfdatarow2").val(cnt);  
                                $("#tbdata tbody").append(html);
                                $("#exampleModalCenter").modal('hide');
                            }
                        }
                     });
                }
               
            }
         });
        
        
            
    }
    function ajax_savedata() {
        var i = 0;
        var j = 0;
        var id = $('#hdfid').val();
        var data_check_list = [];
        var id_check_list = [];
        var data_sub_check_list = [];
        var id_sub_check_list = [];
        var data_comment_list = [];
        var data_sub_comment_list = [];
        
        
        var data_row_list = $("#hdfdatarow").val();
        var data_row_sub_list = $("#hdfdatarow2").val();
        //var department_id = $("#hdfdepartment").val();
        if(data_row_list==''){ data_row_list=1}
        /*
        while(i <= data_row_list)
        {       
            //var list = $('input[name=radiocheck'+i+']:checked').val();
            var list = $('.headdata input[type="radio"]:checked').eq(i);
            var comment_list = $('.headdata input[type="text"]').eq(i).val();
            data_check_list[i] = list==undefined ? null : list.val();
            id_check_list[i] = list.attr("data-id");
            data_comment_list[i] = comment_list;
            i++;
        }
        if(data_row_sub_list==''){ data_row_sub_list=1}
        while(j <= data_row_sub_list)
        {       
            var list = $('.subdata input[type="radio"]:checked').eq(j);
            var comment_list = $('.subdata input[type="text"]').eq(j).val();
            
            data_sub_check_list[j] = list==undefined ? null : list.val();
            id_sub_check_list[j] = list.attr("data-id");
            data_sub_comment_list[j] = comment_list;
            j++;
        }
        
   console.log("data",id_check_list);return;
        for (let z = 0; z < data_check_list.length; z++) {
            var chkNull = data_check_list[z];  
            var chkPoint = id_check_list[z];
            console.log("chk",chkNull);
            if(chkNull== null){
                console.log("111",chkPoint,chkNull);
                //return;
                var list = $('.headdata input[type="radio"]').eq(z);
                if(chkPoint!=undefined){
                    $('html, body').animate({
                        scrollTop: list.offset().top
                    }, 2000);               
                    return;
                }
            }
            
        }
return;
*/
        while(i < data_row_list)
        {    
            var array = $("#hdfdata_id").val().split(',');         
            var list = $('input[name=radiocheck'+array[i]+']:checked').val();
            data_check_list[i] = list==undefined ? null : list;
            id_check_list[i] = $('input[name=radiocheck'+array[i]+']').attr("data-id");
            data_comment_list[i] = $('#txtradiocheck'+array[i]).val();
            i++;
        }
        if(data_row_sub_list==''){ data_row_sub_list=1}
        while(j < data_row_sub_list)
        {
            var array = $("#hdfsub_data_id").val().slice(0,-1).split(','); 
            var list = $('input[name=radiosubcheck'+array[j]+']:checked').val();
            data_sub_check_list[j] = list==undefined ? null : list;
            id_sub_check_list[j] = $('input[name=radiosubcheck'+array[j]+']').attr("data-id");
            data_sub_comment_list[j] = $('#txtradiosubcheck'+array[j]).val();
            j++;

        }
        for (let z = 0; z < data_check_list.length; z++) {
            var chkNull = data_check_list[z];  
            var chkPoint = id_check_list[z];
            if(chkNull==null){
                if(chkPoint!=undefined){
 //console.log(chkPoint,chkNull);
                //return;
                    $('html, body').animate({
                        scrollTop: $("input[name='radiocheck"+chkPoint+"']").offset().top
                    }, 2000);  
                    return;
                }
            }
        }

        //$("#modal-faq").modal('hide');
        //$("#exampleModalCenter").modal('show');
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/icround/Savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'data_check_list':data_check_list,'id_check_list':id_check_list,'data_sub_check_list':data_sub_check_list,'id_sub_check_list':id_sub_check_list,'data_comment_list':data_comment_list,'data_sub_comment_list':data_sub_comment_list,'status':""},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {	
                   // $('#tbdata').DataTable().ajax.reload();
                    alert("บันทึกสำเร็จ");
                    //$("#exampleModalCenter").modal('hide');
                    location.reload();
                    
                }else{
                    //$("#exampleModalCenter").modal('hide');
                    alert(data.msg);
                } 
            }
        });

    } 
    
</script>