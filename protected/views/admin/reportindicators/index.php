<?php
	$this->pageTitle = 'ข้อมูลผู้ใช้' . Yii::app()->params['prg_ctrl']['pagetitle'];
$dataPoints = array(
	array("y" => 25, "label" => "1"),
	array("y" => 15, "label" => "2"),
	array("y" => 25, "label" => "3"),
	array("y" => 5, "label" => "4"),
	array("y" => 10, "label" => "5"),
	array("y" => 1, "label" => "6"),
	array("y" => 20, "label" => "7"),
    array("y" => 20, "label" => "8"),
    array("y" => 22, "label" => "9"),
    array("y" => 20, "label" => "10"),
    array("y" => 20, "label" => "11"),
    array("y" => 3, "label" => "12"),
    array("y" => 5, "label" => "13"),
    array("y" => 11, "label" => "14"),
    array("y" => 30, "label" => "15"),
    array("y" => 28, "label" => "16"),
    array("y" => 24, "label" => "17"),
    array("y" => 21, "label" => "18"),
    array("y" => 23, "label" => "19"),
    array("y" => 13, "label" => "20"),
    array("y" => 13, "label" => "21"),
    array("y" => 14, "label" => "22"),
    array("y" => 16, "label" => "23"),
    array("y" => 14, "label" => "24"),
    array("y" => 15, "label" => "25"),
    array("y" => 18, "label" => "26"),
    array("y" => 19, "label" => "27"),
    array("y" => 11, "label" => "28"),
    array("y" => 1, "label" => "29"),
    array("y" => 3, "label" => "30"),
    array("y" => 3, "label" => "31"),
);
$dataPoints2 = array(
	array("y" => 2, "label" => "1"),
	array("y" => 4, "label" => "2"),
	array("y" => 6, "label" => "3"),
	array("y" => 5, "label" => "4"),
	array("y" => 10, "label" => "5"),
	array("y" => 12, "label" => "6"),
	array("y" => 21, "label" => "7"),
    array("y" => 24, "label" => "8"),
    array("y" => 22, "label" => "9"),
    array("y" => 27, "label" => "10"),
    array("y" => 23, "label" => "11"),
    array("y" => 13, "label" => "12"),
   
);
$dataPoints3 = array(
    array("y" => 5, "label" => "2564" ),
    array("y" => 10, "label" => "2565" ),
   
);
?>


 <!-- Main content -->
<section class="row content mt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <div class="card">
            <div class="card-header thsarabunnew">
            <label class="card-title"></label>

            <div class="card-tools">
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
             <!--div id="chartContainer2" style="height: 370px; width: 100%;"></div-->
              <div class="row">
              <div class="col-md-4">
                  <div id="chartContainer" style="height: 370px; width: 100%;"></div>
              </div>
              <div class="col-md-4">
                  <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
              </div>
              <div class="col-md-4">
                  <div id="chartContainer3" style="height: 370px; width: 100%;"></div>              
              </div>
                  </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>    
<input type="hidden" id="hdfcode" />
<input type="hidden" id="hdfname" />
 
<script>
     var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
    $("#hdfcode").val(getUrlParameter("code"));
    
window.onload = function () {
    var code = $("#hdfcode").val();
    var name = "";
    $.ajax({
        type: "POST",
        url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/reportindicators/Search"); ?>",
        data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','code':code},
        dataType: "json",				
        success: function (data) {
            //console.log(data.data)
            /*
            
            if(data.data[0].cnt>0){
                $("#btnAddFAQ").addClass("d-none");
                $("button.btn.btn-warning.float-right").addClass("d-none");
                $("#btnSave").addClass("d-none");
                //$(".btndelete").addClass("d-none");
                $("#exampleModalCenter").modal('hide');  
                $("#hdfstatus").val(1);//1=มีการใช้แบบฟอร์มแล้ว
            }else{
                $("#exampleModalCenter").modal('hide');  
            }
            */
            $(".card-title").text(data.data[0].name);
            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "วิเคราะห์แบบรายวัน"
                },
                axisY: {
                    title: "ร้อยละ"
                },
                data: [{
                    type: "line",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();
            var chart2 = new CanvasJS.Chart("chartContainer2", {
                title: {
                    text: "วิเคราะห์แบบรายเดือน"
                },
                axisY: {
                    title: "ร้อยละ"
                },
                data: [{
                    type: "line",
                    dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart2.render();
            var chart3 = new CanvasJS.Chart("chartContainer3", {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "วิเคราะห์แบบรายปี"
                },
                axisY: {
                    title: "ร้อยละ"
                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.## tonnes",
                    dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart3.render();
        }
    });
}
//$(".canvasjs-chart-credit").remove();
</script>

   <style>
       .canvasjs-chart-credit{
           display: none !important;
       }
</style> 
 
  
    
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
       