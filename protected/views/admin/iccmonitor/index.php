<?php
	$this->pageTitle = 'ICC Monitor' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<style>
th 
{
  vertical-align: bottom !important;
  text-align: center;
}

th span 
{
  -ms-writing-mode: tb-rl;
  -webkit-writing-mode: vertical-rl;
  writing-mode: vertical-rl;
  transform: rotate(180deg);
  white-space: nowrap;
}
.div-center {
  margin: auto;
  width: 50%;
  padding: 10px;
}
    
</style>


<section class="content">
  <div class="container-fluid">
    <div class="row mt-3">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">ICCMonitor</h3>

            <div class="card-tools">
             <div class="rd-mailform" data-form-output="form-output-global"  novalidate="novalidate">
              <div class="input-group input-group-custom input-group-sm no-wrap">
               <!-- <label class="form-label rd-input-label" for="txtkeyword">Search...</label>-->
                  
                  
                  
                <input id="txtkeyword" autocomplete="off" class="form-input mr-2 form-control-has-validation datepicker" data-provide="datepicker">  
                <button onClick="Search('')" class="btn btn-dark btn-darkest btn-sm" type="button">Search</button>
              </div>
            </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table id="tbdata" class="table-bordered table table-hover text-nowrap thsarabunnew">
                    <?php 
                       /*

                        $rowstr = "<tr>";
                        $rowsth = "";
                        $rows2th = "";
                        $rowsendtr = "</tr>";
                        $rowBody="";
                        $rowBody2="";

                        $i=0;

                        foreach($data as $dataitem)
                        {
                            if($dataitem['aa']!=1){
                                 if($dataitem['hai']==0){ 
                                     $i++;
                                    $rowsth.=' <th rowspan="2" ><span>'.$dataitem['name'].'</span></th>';
                                }else{      
                                     $i=$i+2;
                                    $rowsth.=' <th colspan="2"><span>'.$dataitem['name'].'</span></th>';
                                    $rows2th.=' <th>HAI</th><th>CAI</th>';
                                }                     
                            }            
                        } 

                        $id=0;
                        for ($xx = 0; $xx < count($data2); $xx++) 
                        { 
                            $chk = $data2[$xx]['status'];                            
                            $param = $chk!="nolist"? $chk!="nocheck"?'<i class="fa-check-circle fas text-success" title="ลงข้อมูลแล้ว"></i>':'<i class="fas fa-times-circle text-danger" title="ยังไม่ลงข้อมูลรายวัน"></i>' : '<i title="ไม่มีตัวชี้วัดที่ต้องลงข้อมูล" class="fas fa-question-circle text-secondary"></i>';
                            
                            //$param = $data2[$xx]['status']=="nolist" ? '<i title="ไม่มีตัวชี้วัดที่ต้องลงข้อมูล" class="fas fa-question-circle text-secondary"></i>':
                            //$data2[$xx]['status']=="nocheck" ? '<i class="fas fa-times-circle text-danger" title="ยังไม่ลงข้อมูลรายวัน"></i>': '<i class="fa-check-circle fas text-success" title="ลงข้อมูลแล้ว"></i>';
                            if($id!=$data2[$xx]["department_id"]){
                                $id=$data2[$xx]["department_id"];
                                 $rowBody.='<tr class="text-center"><td class="text-left">'.$data2[$xx]['dept_name'].'</td><td>'.$param.'</td>';
                                if($data2[$xx]['hai']==1){   
                                   
                                    $rowBody.='<td>'.$param.'</td>';
                                }
                            }else{
                                if($data2[$xx]['hai']==1){   
                                    
                                    $rowBody.='<td>'.$param.'</td>';
                                }                    
                                $rowBody.='<td>'.$param.'</td>';
                                //$rowBody.='</tr>';
                            }     
                        }

                        echo "<thead>".$rowstr."<th rowspan='2'><div class='float-left mb-5'>หน่วยงาน </div><div class='float-right mt-5'>ตัวชี้วัด</div></th>".$rowsth.$rowsendtr.$rowstr.$rows2th.$rowsendtr."</thead>";
                        echo $rowBody;
                        */
                    ?>
                </table>   
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <!-- /.row -->

  </div><!-- /.container-fluid -->
</section>
<div class="col-md-4 thsarabunnew">
    <!-- Bootstrap Switch -->
    <div class="card card-secondary">
      <div class="card-header">
        <h3 class="card-title">หมายเหตุ</h3>
      </div>
      <div class="card-body">
          <label class="col-form-label"><i class="fa-check-circle fas text-success"></i> ลงข้อมูลแล้ว</label><br/>
          <label class="col-form-label"><i class="fas fa-times-circle text-danger"></i> ยังไม่ลงข้อมูลรายวัน</label><br/>
          <label class="col-form-label"><i class="fas fa-question-circle text-secondary"></i> ไม่มีตัวชี้วัดที่ต้องลงข้อมูล</label><br/>
          <label class="col-form-label"><i class="fas fa-radiation-alt text-yellow"></i> พบการติดเชื้อ/การปฏิบัติที่ไม่ถูกต้อง</label>
          

      </div>
    </div>
    <!-- /.card -->
</div>

<script>
     jQuery(document).ready(function ($) {    
         $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            //startDate: '-3d'
        });
         
         
         $("div.container").eq(1).removeClass("container");
            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();

            var keyword = d.getFullYear() + '-' +
                (month<10 ? '0' : '') + month + '-' +
                (day<10 ? '0' : '') + day;
         Search(keyword);
         
         
         
     });
    function Search(el){
        var keyword = $("#txtkeyword").val()
        if(el!=""){
            keyword=el;
        }
        //alert(keyword);return;
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/iccmonitor/search"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','keyword':keyword},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {		
                    var html=""
                    var rowstr = "<tr>";
                    var rowsth = "";
                    var rows2th = "";
                    var rowsendtr = "</tr>";
                    var rowBody="";
                    var rowBody2="";

                    var xx=0;
                    
                   
                    $.each(data.data, function(i, item) {
                        if(data.data[i].aa!=1){
                            if(data.data[i].hai==0){ 
                                 xx++;
                                 rowsth+=' <th rowspan="2" ><span>'+ data.data[i].name +'</span></th>';
                            }else{      
                                 xx=xx+2;
                                 rowsth+=' <th colspan="2"><span>'+ data.data[i].name +'</span></th>';
                                 rows2th+=' <th>HAI</th><th>CAI</th>';
                            }     
                        }
                    });
                    var id=0;
                    /*
                    for (let xx = 0; xx < data.data2.length; xx++) {
                      var chk = $data2[$xx]['status'];    
                    }
                    */
                     $.each(data.data2, function(i, item) {
                         var chk = data.data2[i].status; 
                         var param = chk!="nolist"? chk!="nocheck"?'<i class="fa-check-circle fas text-success" title="ลงข้อมูลแล้ว"></i>':'<i class="fas fa-times-circle text-danger" title="ยังไม่ลงข้อมูลรายวัน"></i>' : '<i title="ไม่มีตัวชี้วัดที่ต้องลงข้อมูล" class="fas fa-question-circle text-secondary"></i>';
                        if(id!=data.data2[i].department_id){
                             id=data.data2[i].department_id;
                             rowBody+='<tr class="text-center"><td class="text-left">'+ data.data2[i].dept_name+'</td><td>'+ param +'</td>';
                            if(data.data2[i].hai==1){   

                                rowBody+='<td>'+ param +'</td>';
                            }
                        }else{
                            if(data.data2[i].hai==1){   

                                rowBody+='<td>'+ param +'</td>';
                            }                    
                            rowBody+='<td>'+ param + '</td>';
                            //$rowBody.='</tr>';
                        }     
                       
                    });
                    
                    

                    html+= "<thead>"+ rowstr +"<th rowspan='2'><div class='float-left mb-5'>หน่วยงาน </div><div class='float-right mt-5'>ตัวชี้วัด</div></th>"+ rowsth + rowsendtr + rowstr + rows2th + rowsendtr +"</thead>";
                    html+=rowBody;
                   
                   $("#tbdata").html(html);
                     $('html, body').animate({
                        scrollTop: $("#tbdata tbody").offset().top
                    }, 2000); 
                }
                else{
                    alert(data.msg);
                } 
            }
        });
    }
    
</script>
