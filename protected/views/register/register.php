<?php
	$this->pageTitle = 'Register' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>


      <!-- Breadcrumbs-->
      <section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(../images/background-breadcrumbs-01-1920x345.jpg);" data-preset="{&quot;title&quot;:&quot;Breadcrumbs&quot;,&quot;category&quot;:&quot;header&quot;,&quot;reload&quot;:false,&quot;id&quot;:&quot;breadcrumbs&quot;}">
        <div class="container">
          <h2 class="breadcrumbs-custom-title">Login</h2>
          <ul class="breadcrumbs-custom-path">
            <!--li><a href="#">Home</a></li>
            <li class="active">Login</li-->
          </ul>
        </div>
      </section>
      <!-- Our Customers-->
      <section class="section-lg section bg-default">
        <div class="container">
          <div class="row justify-content-sm-center justify-content-lg-start">
            <div class="col-md-8 col-lg-6 col-xl-5">
              <!-- Responsive-tabs-->
              <div class="responsive-tabs responsive-tabs-classic tabs-custom" data-type="horizontal" style="width: 100%;">
                <ul class="resp-tabs-list tabs-1 text-center tabs-group-default" data-group="tabs-group-default">
                  <li class="resp-tab-item tabs-group-default resp-tab-active" role="tab">Register</li>
                </ul>
                <div class="resp-tabs-container text-left tabs-group-default" data-group="tabs-group-default">
              <div class="resp-tab-content tabs-group-default resp-tab-content-active" aria-labelledby="tabs-group-default_tab_item-0" style="display:block">
                    <div class="responsive-tabs-padding-none">
                      <!-- RD Mailform-->
                      <div class="text-left">
                        <div class="form-wrap">
                          <label class="form-label form-label-outside rd-input-label" for="name">Name</label>
                          <input class="form-input" id="name" type="text" name="name">
                        </div>
                        <div class="form-wrap">
                          <label class="form-label form-label-outside rd-input-label" for="contact-email1">E-mail</label>
                          <input class="form-input" id="contact-email1" type="email" name="email">
                        </div>
                        <div class="form-wrap">
                          <label class="form-label form-label-outside rd-input-label" for="password1">Password</label>
                          <input class="form-input" id="password1" name="password" type="password">
                        </div>
                        <div class="form-wrap">
                          <label class="form-label form-label-outside rd-input-label" for="password2">Confirm Password</label>
                          <input class="form-input" id="password2" name="password" type="password">
                        </div>
                        <div class="offset-top-20">
                          <div class="d-md-flex offset-none align-items-md-center text-center text-md-left">
                            <button class="btn btn-ellipse btn-primary" type="submit">Register</button>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>