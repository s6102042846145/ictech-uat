
    <?php /*
<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
          ['1',  165,      938,         522,             998,           450,      800],
          ['2',  135,      1120,        599,             1268,          288,      800],
          ['3',  157,      1167,        587,             807,           397,      800],
          ['4',  139,      1110,        615,             968,           215,      800],
          ['5',  136,      691,         629,             1026,          366,      800],
          ['6',  100,      200,         300,             400,           500,      800]
        ]);

        var options = {
          title : 'Monthly Coffee Production by Country',
          vAxis: {title: 'Cups'},
          hAxis: {title: 'Month'},
          seriesType: 'bars',
          series: {5: {type: 'line'}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
        
        
        
        
        
         google.charts.load('current', {'packages':['corechart', 'line']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var button = document.getElementById('change-chart');
      var chartDiv = document.getElementById('chart_divs');

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Index');
      data.addColumn('number', 'Fibonacci Number');

      data.addRows([
        [-16, -987],
        [-15, 610],
        [-14, -377],
        [-13, 233],
        [-12, -144],
        [-11, 89],
        [-10, -55],
        [-9, 34],
        [-8, -21],
        [-7, 13],
        [-6, -8],
        [-5, 5],
        [-4, -3],
        [-3, 2],
        [-2, -1],
        [-1, 1],
        [0, 0],
        [1, 1],
        [2, 1],
        [3, 2],
        [4, 3],
        [5, 5],
        [6, 8],
        [7, 13],
        [8, 21],
        [9, 34],
        [10, 55],
        [11, 89],
        [12, 144],
        [13, 233],
        [14, 377],
        [15, 610],
        [16, 987]
      ]);

      var linearOptions = {
        title: 'Fibonacci Numbers in Linear Scale',
        legend: 'none',
        pointSize: 5,
        width: 900,
        height: 500,
        hAxis: {
          gridlines: {
            count: -1
          }
        },
        vAxis: {
          ticks: [-1000, -500, 0, 500, 1000]
        }
      };

      var mirrorLogOptions = {
        title: 'Fibonacci Numbers in Mirror Log Scale',
        legend: 'none',
        pointSize: 5,
        width: 900,
        height: 500,
        hAxis: {
          gridlines: {
            count: -1
          }
        },
        vAxis: {
          scaleType: 'mirrorLog',
          ticks: [-1000, -250, -50, -10, 0, 10, 50, 250, 1000]
        }
      };

      function drawLinearChart() {
        var linearChart = new google.visualization.LineChart(chartDiv);
        linearChart.draw(data, linearOptions);
        button.innerText = 'Change to Mirror Log Scale';
        button.onclick = drawMirrorLogChart;
      }

      function drawMirrorLogChart() {
        var mirrorLogChart = new google.visualization.LineChart(chartDiv);
        mirrorLogChart.draw(data, mirrorLogOptions);
        button.innerText = 'Change to Linear Scale';
        button.onclick = drawLinearChart;
      }

      drawMirrorLogChart();
    }
    </script>
  </head>
  <body>
      
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <button id="change-chart"></button>
  <br><br>
  <div id="chart_divs"></div>
      
      
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
  </body>
</html>

*/ ?>
<?php
 /*
$dataPoints = array(
	array("y" => 25, "label" => "Sunday"),
	array("y" => 15, "label" => "Monday"),
	array("y" => 25, "label" => "Tuesday"),
	array("y" => 5, "label" => "Wednesday"),
	array("y" => 10, "label" => "Thursday"),
	array("y" => 1, "label" => "Friday"),
	array("y" => 20, "label" => "Saturday"),
    array("y" => 20, "label" => "Saturday"),
    array("y" => 22, "label" => "Saturday"),
    array("y" => 20, "label" => "Saturday"),
    array("y" => 20, "label" => "Saturday"),
    array("y" => 3, "label" => "Saturday"),
);
 $dataPoints2 = array( 
	array("y" => 3373.64, "label" => "Germany" ),
	array("y" => 2435.94, "label" => "France" ),
	array("y" => 1842.55, "label" => "China" ),
	array("y" => 1828.55, "label" => "Russia" ),
	array("y" => 1039.99, "label" => "Switzerland" ),
	array("y" => 765.215, "label" => "Japan" ),
	array("y" => 612.453, "label" => "Netherlands" )
);
?>
<!DOCTYPE HTML>
<html>
<head>
<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
	title: {
		text: "Push-ups Over a Week"
	},
	axisY: {
		title: "Number of Push-ups"
	},
	data: [{
		type: "line",
		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();
           
 var chart2 = new CanvasJS.Chart("chartContainer2", {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: "Gold Reserves"
	},
	axisY: {
		title: "Gold Reserves (in tonnes)"
	},
	data: [{
		type: "column",
		yValueFormatString: "#,##0.## tonnes",
		dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
	}]
});
chart2.render();
 
}
</script>
</head>
<body>
    <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
    
    
    
    <div>
<p>Hello!</p>
<button type="button" id="change">change</button>
        </div>
<script>
$(document).ready(function(){
  $( "button" ).click(function() {
      $( this ).replaceWith( "<div>" + $( this ).text() + "</div>" );
    });
    /*
    $('#change').click(function(){
        
        $(this).replaceWith($('<h5>' + this.innerHTML + '</h5>'));
    });
    
});
    


</script>    
    
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>      */ ?>      











<?php
/*
// Access Token
$access_token = 'กรอกข้อมูลจาก Channel access token (long-lived)';
// รับค่าที่ส่งมา
$content = file_get_contents('php://input');
// แปลงเป็น JSON
$events = json_decode($content, true);
if (!empty($events['events'])) {
    foreach ($events['events'] as $event) {
        if ($event['type'] == 'message' && $event['message']['type'] == 'text') {
            // ข้อความที่ส่งกลับ มาจาก ข้อความที่ส่งมา
            // ร่วมกับ USER ID ของไลน์ที่เราต้องการใช้ในการตอบกลับ
            $messages = array(
                'type' => 'text',
                'text' => 'Reply message : '.$event['message']['text']."\nUser ID : ".$event['source']['userId'],
            );
            $post = json_encode(array(
                'replyToken' => $event['replyToken'],
                'messages' => array($messages),
            ));
            // URL ของบริการ Replies สำหรับการตอบกลับด้วยข้อความอัตโนมัติ
            $url = 'https://api.line.me/v2/bot/message/reply';
            $headers = array('Content-Type: application/json', 'Authorization: Bearer '.$access_token);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);
            echo $result;
        }
    }
}
*/
// Access Token
$access_token = 'Dv9pS0sFfgShS5vBjl/OTMF01ZyY97hRYXUJ4R+dmSbeIRA1k2o7c8Vjb9XHwiaz2THaj0KXhwIpFBszkij53d7RtXADPg09r84fa0kDmYzXBhZbQ783pSzK1eh6ZqmfuHs2NkiSmCCfiB9r/nTU/gdB04t89/1O/w1cDnyilFU=';
// User ID
$userId = 'Ue6b7ba3e7d97d12b17956c3c6562d63f';
// ข้อความที่ต้องการส่ง
$messages = array(
    'type' => 'text',
    'text' => 'ทดสอบการส่งข้อความ',
);
$post = json_encode(array(
    'to' => array($userId),
    'messages' => array($messages),
));

// URL ของบริการ Replies สำหรับการตอบกลับด้วยข้อความอัตโนมัติ
$url = 'https://api.line.me/v2/bot/message/multicast';
$headers = array('Content-Type: application/json', 'Authorization: Bearer '.$access_token);
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

$result = curl_exec($ch);
echo var_dump($result);exit();
echo $result;

$accessToken = "Dv9pS0sFfgShS5vBjl/OTMF01ZyY97hRYXUJ4R+dmSbeIRA1k2o7c8Vjb9XHwiaz2THaj0KXhwIpFBszkij53d7RtXADPg09r84fa0kDmYzXBhZbQ783pSzK1eh6ZqmfuHs2NkiSmCCfiB9r/nTU/gdB04t89/1O/w1cDnyilFU=";//copy ข้อความ Channel access token ตอนที่ตั้งค่า
   $content = file_get_contents('php://input');
   $arrayJson = json_decode($content, true);
   $arrayHeader = array();
   $arrayHeader[] = "Content-Type: application/json";
   $arrayHeader[] = "Authorization: Bearer {$accessToken}";
   //รับข้อความจากผู้ใช้
   $message = $arrayJson['events'][0]['message']['text'];
   //รับ id ของผู้ใช้
   $id = $arrayJson['events'][0]['source']['userId'];
   if($message == "นับ 1-10"){
       for($i=1;$i<=10;$i++){
          $arrayPostData['to'] = $id;
          $arrayPostData['messages'][0]['type'] = "text";
          $arrayPostData['messages'][0]['text'] = $i;
          pushMsg($arrayHeader,$arrayPostData);
       }
    }
   function pushMsg($arrayHeader,$arrayPostData){
      $strUrl = "https://api.line.me/v2/bot/message/push";
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$strUrl);
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $arrayHeader);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayPostData));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($ch);
      curl_close ($ch);
   }
   exit;
?>








