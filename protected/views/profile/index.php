<?php
	$this->pageTitle = 'ข้อมูลส่วนตัว' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>

<section class="section-lg section bg-default">
        <div class="container">
          <div class="row justify-content-sm-center">
            <div class="col-sm-10 col-lg-4">
              <!-- Member block type 5-->
              <div class="member-block-type-5 inset-lg-right-20">
                  <img class="img-responsive center-block" src="<?php echo Yii::app()->params['prg_ctrl']['profile'] ?>our-team-05-384x410.jpg" width="320" height="320" alt="">
                <div class="member-block-body"><a class="btn-ellipse btn-primary btn" href="https://ld-wt73.template-help.com/wt_prod-20176/make-an-appointment.html">make an appointment</a>
                  <address class="contact-info offset-top-20 offset-sm-top-24 mt-4">
                    <ul class="list-unstyled p">
                      <li><span class="icon icon-xxxs text-middle text-primary mdi mdi-phone"></span><a class="text-middle d-inline-block text-gray-darker" href="tel:1-800-1234-567">1-800-1234-567</a></li>
                      <li><span class="icon icon-xxxs text-middle text-primary mdi mdi-email-open"></span><a class="text-middle d-inline-block" href="mailto:mail@demolink.org">mail@demolink.org</a></li>
                    </ul>
                  </address>
                  <div class="offset-top-24">
                    <ul class="list-inline list-inline-xs">
                      <li>                          
                          <a class="icon icon-xxs icon-circle icon-gray-light" href="#">
                              <i class="mdi mdi-facebook"></i>
                          </a>
                        </li>
                      <li>
                          <a class="icon icon-xxs icon-circle icon-gray-light" href="#">
                          <i class="mdi mdi-twitter"></i>
                          </a>
                       </li>
                       <li>
                          <a class="icon icon-xxs icon-circle icon-gray-light" href="#">
                          <i class="mdi mdi-google-plus"></i>
                          </a>
                       </li>
                       <li>
                          <a class="icon icon-xxs icon-circle icon-gray-light" href="#">
                          <i class="mdi mdi-rss"></i>
                          </a>
                       </li>
                    </ul>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="col-sm-10 col-lg-8 text-lg-left offset-top-60 offset-md-top-0">
              <div class="row">
                <div class="col-md-5">
                  <div>Position</div>
                  <h5 class="font-weight-bold">CMO, Pathologist</h5>
                </div>
                <div class="col-md-7 offset-top-41 offset-sm-top-0">
                  <div>Department</div>
                  <h5 class="font-weight-bold">Perelman School of Medicine at the University of Pennsylvania (1987)</h5>
                </div>
              </div>
              <div class="offset-top-66 text-left">
                <h6>the heart of medical center</h6>
                <hr class="text-subline">
                <p>Nullam non odio vitae velit volutpat vulputate tempor eu sapien. Phasellus porttitor diam in tellus semper, ut elementum arcu eleifend. Nullam in posuere orci, ac congue augue. Sed varius massa et tortor fermentum, a dapibus ligula varius. Duis nec elementum ante, non imperdiet libero. Sed nec ornare justo, quis vehicula mauris. Nam ornare dui vitae ex congue interdum. Donec luctus dignissim est at ultricies. Sed ullamcorper posuere leo vitae suscipit. Suspendisse ac sem at nulla pellentesque rutrum a vel diam. Morbi pretium interdum lorem nec faucibus.</p>
                <p>Aenean ac ex nunc. Phasellus tincidunt tempus enim. Sed elementum volutpat libero at pellentesque. Vestibulum interdum, dolor eget tristique dignissim, augue diam viverra ex, non malesuada ipsum mauris volutpat nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam efficitur accumsan condimentum.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
<script>
$(document).ready(function (){
    $(".breadcrumbs-custom.bg-image.context-dark").addClass("breadcrumbs-creative");
});
</script>