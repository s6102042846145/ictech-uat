<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta name="language" content="en" />

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Google font -->

        <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

        <!-- Bootstrap -->

        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/bootstrap.min.css" />

        <!-- Slick -->

        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/slick.css" />

        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/slick-theme.css" />

        <!-- nouislider -->

        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/nouislider.min.css" />

        <!-- Font Awesome Icon -->

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/line-awesome/css/all.css">
        
        <!-- Custom stlylesheet -->

        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/theme/css/style1.css" />

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css"> 

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery/jquery-1.11.0.min.js"></script>

		<title><?php echo CHtml::encode($this->pageTitle); ?></title>    

	</head>

	<body>

		<?php echo $content; ?>  

        <!-- jQuery Plugins -->

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/jquery.min.js"></script>

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/bootstrap.min.js"></script>

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/slick.min.js"></script>

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/nouislider.min.js"></script>

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/jquery.zoom.min.js"></script>

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/theme/js/main.js"></script>

	</body>

</html>



