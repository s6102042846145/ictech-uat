<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta name="description" content="โรงพยาบาล ระบบ ictech"> 
        <meta charset="UTF-8"> 
        <meta name="keywords" content="ictech, ictech.tech, การใช้งานโปรแกรม, โรงพยาบาล, ผาขาว,Yutthana,Suilon"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta name="google-site-verification" content="61rS7SfnOpKRv6Xhqyvsf1VDkGxbSXhz7vYuPbGrL4c" />
        <meta property="og:title" content="ictech">
        <meta property="og:type" content="website">
        <meta property="og:site_name" content="ระบบ ictech">
        <meta property="og:url" content="">
        <meta property="og:image" content="">
        <meta property="og:description" content="โรงพยาบาล ระบบ ictech">
        <meta property="og:image:alt" content="ictech">

        <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/icons/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/main.css"> 
        <!-- js -->        
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery/jquery-1.11.0.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/admin/main.js"></script>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>    
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GVQ5METZWP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GVQ5METZWP');
</script>
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQFSP5R');</script>
<!-- End Google Tag Manager -->
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQFSP5R"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
         <h1 class="d-none">ictech</h1>
        <?php echo $content; ?> 
         <button id="btnload" type="button" class="d-none" data-toggle="modal" data-target="#modalLoad"></button>
    </body>
</html>

<!-- Large modal -->

<div id="modalDepartment" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modaldetailLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               
            </div>
            <div class="modal-footer">
                <button type="button" id="btnClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="btnSave" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<div id="modalLoad" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog-centered justify-content-center">
         <div class="spinner-border"></div>        
    </div>
</div>