<!DOCTYPE html>
<html class="wide wow-animation scrollTo desktop landscape rd-navbar-static-linked" lang="en">
<head>
	<meta name="description" content="โรงพยาบาล ระบบ ictech"> 
        <meta charset="UTF-8"> 
        <meta name="keywords" content="ictech, ictech.tech, การใช้งานโปรแกรม, โรงพยาบาล, ผาขาว,Yutthana,Suilon"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta name="google-site-verification" content="61rS7SfnOpKRv6Xhqyvsf1VDkGxbSXhz7vYuPbGrL4c" />
        <meta property="og:title" content="ictech">
        <meta property="og:type" content="website">
        <meta property="og:site_name" content="ระบบ ictech">
        <meta property="og:url" content="">
        <meta property="og:image" content="">
        <meta property="og:description" content="โรงพยาบาล ระบบ ictech">
        <meta property="og:image:alt" content="ictech">

    
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/css/datepicker.css" rel="stylesheet">
   
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/dist/css/adminlte.min.css">
    <script type="text/javascript" async="" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ga.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script_02.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script_01.js"></script>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/materialdesignicons.min.css">
		<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/icons/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/css.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
				<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css">
		<!--link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css"-->
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-thaisarabun.css">
		<!--<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">-->
    
    
    <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/fontawesome-free/css/all.min.css">
  
  
    
    
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/core.min.js"></script>
    	<style>
			@media (max-width: 768px){
    .breadcrumbs-custom {
    padding: 114px 0 80px !important;
				}}
		.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}
            
            
            .message {
  background-color: white;
  width: calc(100% - 3em);
  /*max-width: 24em;*/
  padding: 1em 1em 1em 1.5em;
  border-left-width: 6px;
  border-left-style: solid;
  border-radius: 3px;
  position: relative;
  line-height: 1.5;
}
.message + .message {
  margin-top: 2em;
}
.message:before {
  color: white;
  width: 1.5em;
  height: 1.5em;
  position: absolute;
  top: 1em;
  left: -3px;
  border-radius: 50%;
  transform: translateX(-50%);
  font-weight: bold;
  line-height: 1.5;
  text-align: center;
}
.message p {
  margin: 0 0 1em;
}
.message p:last-child {
  margin-bottom: 0;
}

.message--error {
  border-left-color: firebrick;
}
.message--error:before {
  background-color: firebrick;
  content: "‼";
}

.message--warning {
  border-left-color: darkorange;
}
.message--warning:before {
  background-color: darkorange;
  content: "!";
}

.message--success {
  border-left-color: darkolivegreen;
}
.message--success:before {
  background-color: darkolivegreen;
  content: "✔";
}

		</style>
  		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dataurl.css">  
    
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GVQ5METZWP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GVQ5METZWP');
</script>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQFSP5R');</script>
<!-- End Google Tag Manager -->
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>    
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQFSP5R"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <h1 class="d-none">ictech</h1>
<?php echo $content; ?>
    
    <script>
        function popHaiAndCai(msg)
        {
            var id = $("#hdfid").val();
            
            //alert(id);return;
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/indicatordata/GetPopupHAIAndCAI"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) 
                {
                    
                    if (data.status=='success') { 
                        if(msg!="hai"){
                            $("#xxx .message.message--warning p").html(data.data[0].cai);
                        }else{
                            //alert(data.data[0].hai)
                            $("#xxx .message.message--warning p").html(data.data[0].hai);
                        }
                           $("#xxx").modal("show");
                    }
                    else
                    {
                        alert(data.msg);
                    } 
                }
            });
        }
        function popIndicator(id)
        {
            
             $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/indicatordata/GetPopupIndicator"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) 
                {
                    
                    if (data.status=='success') { 
                            $("#xxx .message.message--warning p").html(data.data[0].tooltip);
                        
                           $("#xxx").modal("show");
                    }
                    else
                    {
                        alert(data.msg);
                    } 
                }
            });
                   
        }
    </script>
    <div class="modal" tabindex="-1" id="xxx" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
       <div class="message  message--warning">
  <p>Pastrami fatback frankfurter ground round pork belly. Meatloaf landjaeger boudin pork strip steak. Bresaola tail capicola, salami landjaeger jerky pork loin tenderloin bacon filet mignon.</p>
</div>
        </div>
      </div>
    </div>
      <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker.js"></script>
         <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker-thai.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/locales/bootstrap-datepicker.th.js"></script>
        <!-- jQuery Plugins -->
		<div class="snackbars" id="form-output-global"></div>
    
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/_script.js"></script>
</body>
</html>
