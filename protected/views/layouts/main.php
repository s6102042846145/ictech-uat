<?php $this->beginContent('//layouts/web_skeleton'); ?>
    <?php require_once("header.php"); ?>
    <div id="container" class="container">
        <?php echo $content; ?>
    </div>
    <?php require_once("footer.php"); ?>
<?php $this->endContent(); ?>


