<?php $this->beginContent('//layouts/admin_web_skeleton_ui'); ?>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php require_once("admin_header.php"); ?>
    <div class="app-main">
        <?php require_once("admin_menu.php"); ?>
        <div class="app-main__outer">               
            <?php echo $content; ?>
            <?php require_once("admin_footer.php"); ?>             
        </div>
    </div>
</div>
<?php $this->endContent(); ?>