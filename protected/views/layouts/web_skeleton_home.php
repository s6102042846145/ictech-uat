<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		

        <meta name="description" content="โรงพยาบาล ระบบ ictech"> 
        <meta charset="UTF-8"> 
        <meta name="keywords" content="ictech, ictech.tech, การใช้งานโปรแกรม, โรงพยาบาล, ผาขาว,Yutthana,Suilon"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta name="google-site-verification" content="61rS7SfnOpKRv6Xhqyvsf1VDkGxbSXhz7vYuPbGrL4c" />
        <meta property="og:title" content="ictech">
        <meta property="og:type" content="website">
        <meta property="og:site_name" content="ระบบ ictech">
        <meta property="og:url" content="">
        <meta property="og:image" content="">
        <meta property="og:description" content="โรงพยาบาล ระบบ ictech">
        <meta property="og:image:alt" content="ictech">


        
        
		<script type="text/javascript" async="" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ga.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script_02.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script_01.js"></script>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">
		<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/icons/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/css.css">
				<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css">
		<!--link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css"-->
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-thaisarabun.css">
		<!--<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">-->
    	<style>
		.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}
		</style>
  		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dataurl.css">
	
		  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GVQ5METZWP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GVQ5METZWP');
</script>
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQFSP5R');</script>
<!-- End Google Tag Manager -->
	</head>
	<body>
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQFSP5R"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<h1 class="d-none">ictech</h1>
		<?php echo $content; ?>  

        <!-- jQuery Plugins -->
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/core.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
	

	</body>

</html>



