<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/icons/favicon.ico" type="image/x-icon">
  <meta name="description" content="โรงพยาบาล ระบบ ictech"> 
        <meta charset="UTF-8"> 
        <meta name="keywords" content="ictech, ictech.tech, การใช้งานโปรแกรม, โรงพยาบาล, ผาขาว,Yutthana,Suilon"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta name="google-site-verification" content="61rS7SfnOpKRv6Xhqyvsf1VDkGxbSXhz7vYuPbGrL4c" />
        <meta property="og:title" content="ictech">
        <meta property="og:type" content="website">
        <meta property="og:site_name" content="ระบบ ictech">
        <meta property="og:url" content="">
        <meta property="og:image" content="">
        <meta property="og:description" content="โรงพยาบาล ระบบ ictech">
        <meta property="og:image:alt" content="ictech">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GVQ5METZWP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GVQ5METZWP');
</script>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQFSP5R');</script>
<!-- End Google Tag Manager -->
  <title><?php echo CHtml::encode($this->pageTitle); ?> Administrator</title>  

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
  
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/fonts/thsarabunnew.css">
     <!-- jQuery -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/jquery/jquery.min.js"></script>
    
    
    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery-ui/1.10.4/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap/js/bootstrap.min.js"></script>    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery-upload/js/vendor/jquery.ui.widget.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery-upload/js/jquery.iframe-transport.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery-upload/js/jquery.fileupload.js"></script>

     <link href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/css/datepicker.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker.js"></script>
   
    
    
    
    
    
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    
    
        
</head>
    <style>
    label {
    padding-top: 0.5rem;
}
    </style>
    <body class="hold-transition layout-top-nav">
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQFSP5R"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
         <h1 class="d-none">ictech</h1>
        <div class="wrapper">
            <div class="content-wrapper">
                <?php echo $content; ?> 
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
              <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">Loading...</span>
              </div>
          </div>
        </div>
       
        <!-- Bootstrap 4 -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables  & Plugins -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/jszip/jszip.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/pdfmake/pdfmake.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/pdfmake/vfs_fonts.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-buttons/js/buttons.html5.js"></script>
        
        <!-- AdminLTE App -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/dist/js/demo.js"></script>
        <!-- Page specific script -->
        
        
        <!-- Daterangepicker -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/moment/moment.min.js"></script>
        <!-- date-range-picker -->
         <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker.js"></script>
         <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker-thai.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/locales/bootstrap-datepicker.th.js"></script>
        
        
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- End Daterangepicker -->
        
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.min.js"></script> 
        
        <script>
          $(function () {
            $("#example1").DataTable({
              "responsive": true, "lengthChange": false, "autoWidth": false, "pageLength": 40,
              "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
              "paging": true,
              "lengthChange": false,
              "searching": false,
              "ordering": true,
              "info": true,
              "autoWidth": false,
              "responsive": true,
            });
          });
            function getUrlParameter(sParam) {
                var sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
                return false;
            };
        </script>    
    </body>
</html>


