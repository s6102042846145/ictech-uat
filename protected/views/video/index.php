<?php
	$this->pageTitle = 'วิดีโอความรู้สำหรับเจ้าหน้าที่' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<style>
.videoWrapper {
	position: relative;
	padding-bottom: 56.25%; /* 16:9 */
	padding-top: 25px;
	height: 0;
}
.videoWrapper iframe {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
</style>
<div class="container page-detail-description pt-md-0 pt-1 pb-3  pb-md-0 pl-0 pr-0">
    <div class="accordion page-detail-accordion-quality" id="page-detail-collapse-quality">
        <?php
                $data=lkup_video::getUserSearch();
                foreach($data as $dataitem)
                {
                     $id = $dataitem['id'];
                     $name = $dataitem['name'];
                     $url = $dataitem['detail'];
                     echo '<div class=" page-detail-card-v2 d-block d-md-none d-md-block" id="section-quality-'.$id.'">
                            <div class="bg-auto-collapse header-in-accordion m-0 mb-3 pl-3 pr-3">
                                <div class="cursor-pointer row m-0 btn-collapse active" data-target="#collapse-quality-'.$id.'" aria-expanded="true" aria-controls="collapse-quality" data-toggle="collapse">
                                    <div class="col-11 p-0 float-left">
                                        <h5 class="font-weight-bold">'.$name.'</h5>
                                    </div>
                                    <div class="col-1 p-0 float-right text-right m-auto">
                                          <img class="collapse-arrow arrow-down" src="'.Yii::app()->params['prg_ctrl']['arrow-down'].'" />
                                          <img class="collapse-arrow collapse-arrow-active" src="'.Yii::app()->params['prg_ctrl']['arrow-up'].'" />
                                    </div>
                                </div>
                            </div>
                            <div id="collapse-quality-'.$id.'" class="data-show pt-3 mt-1 mt-md-0 pt-md-0 pl-3 pr-3 pb-1 pb-md-3 collapse" data-parent="#page-detail-collapse-quality" style="">
                                <div class="card-body p-0">
                                <div class="download-quality-doc m-0">
                                    <div class="videoWrapper">
                                    <iframe width="560" height="315" src="'.$url.'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                      
                                    </div>
                                    
                                </div>
                                
                                
                                ' ;
                     echo '</div></div><hr /></div>';
                }
        ?>
    </div>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           $(".data-show").eq(0).addClass("show");
           
           $(".collapse-arrow.collapse-arrow-active").attr("hidden",true);
           $(".page-detail-card-v2 img.collapse-arrow-active").eq(0).attr("hidden",false);
           $(".page-detail-card-v2 img.collapse-arrow").eq(0).attr("hidden",true);
           var begin = 0;


		   var x = window.matchMedia("(max-width: 767px)")
         
           $('.page-detail-quality .page-detail .nav .nav-item a.a-collapse-page-quality').each(function () {
               
			   let section = $(this).data('section');
               let control = 'page-detail-' + $(this).data('base_collapse');

			   if ((control == 'page-detail-collapse-quality')&& (section == 'section-quality-1' )) {
                   $(this).click();

				   $('.page-detail-quality .page-detail .page-detail-accordion-quality #section-quality-1 .btn-collapse').click();
               }

			   if ((control == 'page-detail-collapse-statistic') && (section == 'section-statistic-1')) {
                   $(this).click();
				   if (x.matches) {
					   $('.page-detail-quality .page-detail .page-detail-accordion-quality #section-statistic-1 .btn-collapse').click();
				   }

               }

			   if ((control == 'page-detail-collapse-debt-statistics') && (section == 'section-debt-statistics-1')) {
                   $(this).click();

				   if (x.matches) {
					   $('.page-detail-quality .page-detail .page-detail-accordion-quality #section-debt-statistics-1 .btn-collapse').click();
				   }
               }

           });
       });
   </script>