<?php
	$this->pageTitle = 'รายงานการประชุม' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>

<div class="container page-detail-description pt-md-0 pt-1 pb-3  pb-md-0 pl-0 pr-0">
    <div class="accordion page-detail-accordion-quality" id="page-detail-collapse-quality">
        <?php 
                function DateThai($strDate)
                {
                    $strYear = date("Y",strtotime($strDate))+543;
                    $strMonth= date("n",strtotime($strDate));
                    $strDay= date("j",strtotime($strDate));
                    $strHour= date("H",strtotime($strDate));
                    $strMinute= date("i",strtotime($strDate));
                    $strSeconds= date("s",strtotime($strDate));
                    $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
                    $strMonthThai=$strMonthCut[$strMonth];
                    return "$strDay $strMonthThai $strYear";
                }
                function DateThai2($strDate)
                {
                    $strYear = date("Y",strtotime($strDate))+543;
                    $strMonth= date("n",strtotime($strDate));
                    $strDay= date("j",strtotime($strDate));
                    $strHour= date("H",strtotime($strDate));
                    $strMinute= date("i",strtotime($strDate));
                    $strSeconds= date("s",strtotime($strDate));
                    $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
                    $strMonthThai=$strMonthCut[$strMonth];
                    return "$strMonthThai $strYear";
                }
                //echo "ThaiCreate.Com Time now : ".DateThai($strDate);
                $head=lkup_meetingreport::getUserSearchHead();
                foreach($head as $dataitem1)
                {
                     
                     $id = $dataitem1['id'];
                     $name1 = $dataitem1['name'];
                     echo '<div class=" page-detail-card-v2 d-block d-md-none d-md-block" id="section-quality-'.$id.'">
                            <div class="bg-auto-collapse header-in-accordion m-0 mb-3 pl-3 pr-3">
                                <div class="cursor-pointer row m-0 btn-collapse active" data-target="#collapse-quality-'.$id.'" aria-expanded="true" aria-controls="collapse-quality" data-toggle="collapse">
                                    <div class="col-11 p-0 float-left">
                                        <h5 class="font-weight-bold">'.$name1.'</h5>
                                    </div>
                                    <div class="col-1 p-0 float-right text-right m-auto">
                                          <img class="collapse-arrow arrow-down" src="'.Yii::app()->params['prg_ctrl']['arrow-down'].'" />
                                          <img class="collapse-arrow collapse-arrow-active" src="'.Yii::app()->params['prg_ctrl']['arrow-up'].'" />
                                    </div>
                                </div>
                            </div>
                            <div id="collapse-quality-'.$id.'" class="data-show pt-3 mt-1 mt-md-0 pt-md-0 pl-3 pr-3 pb-1 pb-md-3 collapse" data-parent="#page-detail-collapse-quality" style="">
                                <div class="card-body p-0">' ;
                
                    $data=lkup_meetingreport::getUserSearch($dataitem1['years']);
                    foreach($data as $dataitem)
                    {
                         $url = Yii::app()->params['prg_ctrl']['url']['upload']."/".Yii::app()->user->getInfo('create_by')."/".$dataitem['file_path'];                
                         $name = $dataitem['name'];
                         echo '<div class="row download-quality-doc m-0">
                                    <div class="col-12 col-md-9 p-0">
                                        <div class="row m-0 p-0">
                                            <div class="col-auto mt-1 p-0">
                                                <img src="'.Yii::app()->params['prg_ctrl']['pdf'].'" class="float-left  mr-md-3 mr-1 cursor-pointer" onclick="window.open(\' '. $url .' \');">
                                            </div>
                                            <div class="col-auto p-0">
                                                <h6 class="mb-0 cursor-pointer text-bold thsarabunnew" onclick="window.open(\' '. $url .' \');">
                                                    '.DateThai2($dataitem['file_date']).'
                                                </h6>
                                                <span>ประกาศ ณ วันที่ '.DateThai($dataitem['file_date']).' </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-3 mt-3 p-0 pt-2 pt-md-0 text-right">
                                        <a href="'.$url.'" download class="ml-1">
                                            <img src="'.Yii::app()->params['prg_ctrl']['download'].'"> ดาวน์โหลด
                                        </a>
                                    </div>
                                </div>
                                <hr>
                            ';
                    }
                    echo '</div></div><hr /></div>';
        
               
                } 
            ?>
            

    </div>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           $(".data-show").eq(0).addClass("show");
           
           $(".collapse-arrow.collapse-arrow-active").attr("hidden",true);
           $("#section-quality-1 img.collapse-arrow-active").eq(0).attr("hidden",false);
           $("#section-quality-1 img.collapse-arrow").eq(0).attr("hidden",true);
           var begin = 0;


		   var x = window.matchMedia("(max-width: 767px)")
         
           $('.page-detail-quality .page-detail .nav .nav-item a.a-collapse-page-quality').each(function () {
               
			   let section = $(this).data('section');
               let control = 'page-detail-' + $(this).data('base_collapse');

			   if ((control == 'page-detail-collapse-quality')&& (section == 'section-quality-1' )) {
                   $(this).click();

				   $('.page-detail-quality .page-detail .page-detail-accordion-quality #section-quality-1 .btn-collapse').click();
               }

			   if ((control == 'page-detail-collapse-statistic') && (section == 'section-statistic-1')) {
                   $(this).click();
				   if (x.matches) {
					   $('.page-detail-quality .page-detail .page-detail-accordion-quality #section-statistic-1 .btn-collapse').click();
				   }

               }

			   if ((control == 'page-detail-collapse-debt-statistics') && (section == 'section-debt-statistics-1')) {
                   $(this).click();

				   if (x.matches) {
					   $('.page-detail-quality .page-detail .page-detail-accordion-quality #section-debt-statistics-1 .btn-collapse').click();
				   }
               }

           });
       });
   </script>