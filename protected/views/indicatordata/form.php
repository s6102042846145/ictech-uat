<?php
	$this->pageTitle = 'ข้อมูลตัวชี้วัดของหน่วยงาน' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>

<section class="section section-20">
    
  <input type="hidden" id="hdfdate" value="<?php echo $_GET['date']; ?>" />
    <script>
        $().ready(function (){
            
            var myCurrentDate=new Date();
            var CurrentDate=new Date(myCurrentDate);
                CurrentDate.setDate(CurrentDate.getDate() - 3);
            var Currentmonth = CurrentDate.getMonth()+1;
            var Currentday = CurrentDate.getDate();
            var dateCurrent = CurrentDate.getFullYear() + '-' +
                (Currentmonth<10 ? '0' : '') + Currentmonth + '-' +
                (Currentday<10 ? '0' : '') + Currentday;
            
            
            
            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();
            var nowdate = d.getFullYear() + '-' +
                (month<10 ? '0' : '') + month + '-' +
                (day<10 ? '0' : '') + day;
            
            /*
            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();
*/
            var chkdate = $("#hdfdate").val();
            //console.log("date",dateCurrent,chkdate,nowdate)
            if(chkdate<dateCurrent||chkdate>nowdate){
                $("#btnsave").remove();
                $("input[type=text]").attr('disabled','disabled');
                $("input[type=checkbox]").attr('disabled','disabled');
            }
            //if()
            
        });
        
        
    </script>
    
    <?php 						
        $data = lkup_indicatordata::getDepartmentMornitors($_GET['indicator']);
        $result='<input type="hidden" id="hdfindicator" value="'.$data[0]['id'].'" />';
        $result.='<input type="hidden" id="hdfchkhai" value="'.$data[0]['hai'].'" />';
        foreach($data as $dataitem) 
        {
            //echo var_dump($dataitem);
            $data2 = lkup_indicatordata::getQuestion($dataitem['id']);
            $data3 = lkup_indicatordata::getQuestionData($dataitem['id']);
            $popup = $dataitem['tooltip']!="" ? ' <i onclick=popIndicator('.$data[0]['id'].') class="pop-up cursor-pointer fas fa-question-circle text-warning"></i>':"";
            
            $results="";
            if($dataitem['hai']==1){
                //$data4 = lkup_indicatordata::getHAIAndCAI($dataitem['id']);
                //echo var_dump($data4);
                $results.= '<tr>';
                $results.= '    <td>การติดเชื้อในรพ.(HAI) <i onclick=popHaiAndCai("hai") class="cursor-pointer fas fa-question-circle text-warning"></i></td>';
                $results.= '    <td><input type="text" class="w-60px" id="txthai"  /> <span>ราย</span></td>'; 
                $results.= '</tr>';
                $results.= '<tr>';
                $results.= '    <td>การติดเชื้อในชุมชน(HAI) <i onclick=popHaiAndCai("cai") class="cursor-pointer fas fa-question-circle text-warning"></i></td>';
                $results.= '    <td><input type="text" class="w-60px" id="txtcai"  /> <span>ราย</span></td>'; 
                $results.= '</tr>';
            
                
            }
            if($dataitem['theme']==1)
            {
                
                $result.=  '<div class="row">';
                $result.= ' <div class="col-12">';
                $result.= '    <table class="fz-20 w-100">';
                $result.=$results;
                foreach($data2 as $dataitem2) 
                {
                    /*
                    $result.= '     <div class="form-group">';
                    $result.= '         <label>'.$dataitem2['name'].'</label>';
                    $result.= '         <input type="text" class="w-60px theme1" answer-value="'.$dataitem2['id'].'" id="txtname'.$dataitem2['id'].'"  />';
                    $result.= '         <span>'.$dataitem2['unit'].'</span>';
                    $result.= '     </div>';
                   */
                     $result.= '<tr>';
                    
                    $result.= '    <td>'.$dataitem2['name'].$popup.'</td>';                    
                    $result.= '    <td><input answer-value="'.$dataitem2['id'].'" type="text" class="w-60px theme1" id="txtname'.$dataitem2['id'].'"  /> <span>'.$dataitem2['unit'].'</span></td>'; 
                    $result.= '</tr>';
                }
                $result.= ' </table></div>';
                $result.= '</div>';  
            }
            if($dataitem['theme']==2)
            {
                $result.= '<div class="row">';
                $result.= ' <div class="col-12">';
                $result.= '    <h5 class="text-bold">'.$dataitem['name'].'</h5>';
                $result.= '    <table class="fz-20 w-100">';
                foreach($data3 as $dataitem2) 
                {   
                    /*
                    $result.= '     <div class="form-group">';
                    $result.= '         <label>'.$dataitem2['name'].'</label>';
                    $result.= '         <input answer-value="'.$dataitem2['id'].'" type="text" class="w-60px theme2" id="txtname'.$dataitem2['id'].'"  />';
                    $result.= '         <span>'.$dataitem2['unit'].'</span>';
                    $result.= '     </div>';
                    */
                    $result.= '<tr>';
                    $result.= '    <td>'.$dataitem2['name'].'</td>';
                    $result.= '    <td><input answer-value="'.$dataitem2['id'].'" type="text" class="w-60px theme2" id="txtname'.$dataitem2['id'].'"  /> <span>'.$dataitem2['unit'].'</span></td>'; 
                    $result.= '</tr>';
       
        
    
                }
                $result.= ' </table></div>';
                $result.= '</div>';
            }
            if($dataitem['theme']==3)
            {
                $result.= '<div class="row">';
                $result.= ' <div class="col-12">';
                $result.= '    <h5 class="text-bold">'.$dataitem['name'].'</h5>';
                $result.= '    <table class="fz-20 w-100">';
                foreach($data3 as $dataitem2) 
                {   
                     $result.= '<tr>';
                    $result.= '    <td>'.$dataitem2['name'].'</td>';
                    $result.= '    <td><input answer-value="'.$dataitem2['id'].'" type="text" class="w-60px theme3" id="txtname'.$dataitem2['id'].'"  /> <span>'.$dataitem2['unit'].'</span></td>'; 
                    $result.= '</tr>';
                    /*
                    $result.= '     <div class="form-group">';
                    $result.= '         <label>'.$dataitem3['name'].'</label>';
                    $result.= '         <input answer-value="'.$dataitem3['id'].'" type="text" class="w-60px theme3" id="txtname'.$dataitem3['id'].'"  />';
                    $result.= '         <span>'.$dataitem3['unit'].'</span>';
                    $result.= '     </div>';
                    */
                }
                $result.= ' </table>';
               $data3 = lkup_indicatordata::getQuestion2($dataitem['id']);
               $result.= '<div class="form-group mt-2">';
               $result.= '  <label class="text-bold">'.$data3[0]["name"].'</label>';                
               $result.= '      <div class="input-group">';
               $data4 = lkup_indicatordata::getQuestion2Detail($data3[0]["id"]);
               foreach($data4 as $dataitem4) 
                {   
                    
                    $result.= ' <div class="p-md-2 pl-3" data-toggle="tooltip" data-placement="top" data-original-title="'.$dataitem4['name'].'">';
                    $result.= '     <input type="checkbox"  class="checkbox" id="ch'.$dataitem4['id'].'" value="'.$dataitem4['id'].'" />';
                    $result.= '     <label class="form-check-label" for="ch'.$dataitem4['id'].'">'.$dataitem4['item_order'].'</label>';
                    $result.= ' </div>';
                    
                }
                    $result.= '     </div>';
                    $result.= '     </div>';
                      
                          
                $result.= ' </div>';
                $result.= '</div>';
                
            }
            if($dataitem['theme']==4)
            {
                
                $head_table = lkup_indicatordata::getQuestionHeadTable($dataitem['id']);
                $result.=  '<div class="row">';
                $result.= ' <div class="col-12">';
                $result.= '  <h5 class="text-bold">'.$data2[0]["name"].'</h5>';      
                $result.= '  <table id="dtHorizontalExample" class="table table-striped table-bordered table-sm" cellspacing="0"
  width="100%"><thead class="text-center thead-light">
        <tr>
            <th></th>';
                foreach($head_table as $head) 
                { 
                     $result.= '<th>'.$head["th"].'</th>';
                }
            
                $id = 0;
                $unit = "";
                $result.= ' </tr></thead><tbody class="text-center">';
                
                
                foreach($data3 as $dataitem2) 
                { 
                     if($unit!=$dataitem2['unit']){
                        $unit=$dataitem2['unit'];
                         $result.='<tr><td>'.$dataitem2['unit'].'</td>';
                         $result.='<td><input type="text" answer-value="'.$dataitem2['id'].'" class="form-control theme2" id="txtname'.$dataitem2['id'].'" />';
                        
                     }else{
                                         
                        $result.='<td><input type="text" answer-value="'.$dataitem2['id'].'" class="form-control theme2" id="txtname'.$dataitem2['id'].'" /></td>';
                        //$rowBody.='</tr>';
                     }     
                    //$result.='</tr>';
                     //$result.= '<td><input type="text" answer-value="'.$dataitem2['id'].'" class="form-control" id="txtname'.$dataitem2['id'].'" /></tb>';
                } 
                
                
                
        
    $result.= '</tbody>
</table>';
                $result.= ' </table></div>';
                $result.= '</div>';
            }
            if($dataitem['theme']==5)
            {
                
                
                $result.= '<div class="row">';
                $result.= ' <div class="col-12">';
                $result.= '    <h5 class="text-bold">'.$dataitem['name'].'</h5>';
                $result.= '    <table class="box-member fz-20 w-100">';
                
                $result.= '<tr>';
                $result.= '    <td>'.$data3[0]['name'].' </td>';
                $result.= '    <td>
                
                <select class="form-control dropdown w-100" id="drpDisease">                            	
						<option value="">--เลือก--</option>';
											
								$dropdownDisease = lkup_indicatordata::getDropdownDisease();
								foreach($dropdownDisease as $valueitem) {
									$result.= "<option data='".$valueitem['name']."' value='".$valueitem['id']."'>".$valueitem['name']."</option>";
								}
				$result.= '<option value="0">อื่น ๆ</option></select>';
                $result.= '    </td><td>
                
                <select class="form-control dropdown w-100" id="drpSpecimen">                            	
						<option value="">--เลือก--</option>';
											
								$dropdownSpecimen = lkup_indicatordata::getDropdownSpecimen();
								foreach($dropdownSpecimen as $valueitem) {
									$result.= "<option value='".$valueitem['id']."'>".$valueitem['name']."</option>";
								}
                
				$result.= '<option value="0">อื่น ๆ</option></select> <span>'.$data3[1]['unit'].'</span></td>';
                $result.= '</tr>';
                
               
                    $result.= '<tr><td></td>';
                    $result.= '    <td>
                    <input answer-value="'.$data3[0]['id'].'" type="text" class="d-none theme2 w-100" id="txtname'.$data3[0]['id'].'"  /> </td>'; 
                    $result.= '    <td>
                    <input answer-value="'.$data3[1]['id'].'" type="text" class="d-none theme2 w-100" id="txtname'.$data3[1]['id'].'"  /></td>'; 
                
                    $result.= '</tr>';
       
        
                $result.= ' </table></div>';
                $result.= '</div>';
            }
               
        } 
        $result.= '<div class="p-5 text-center ">';
        $result.= '     <a href="/indicatordata/" class="mt-2 btn btn-ellipse btn-with-shadow thsarabunnew">ย้อนกลับ</a><button id="btnsave" type="button" onclick="ajax_savedata()" class="mt-2 ml-2 thsarabunnew btn btn-ellipse btn-with-shadow btn-primary">บันทึกข้อมูล</button>';
        $result.= '</div>';
        echo $result; 
    ?>
   
</section>
<input type="hidden" id="hdfid" />
<input type="hidden" id="hdfsavedate" />

       
<script>
    $(function () {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
        
        
        $('#drpDisease').on('change', function() {
          var optionsText = this.options[this.selectedIndex].text;            
          $("input.theme2").eq(0).val(optionsText);
          if(this.value==0){
              $("input.theme2").eq(0).removeClass("d-none");              
              $("input.theme2").eq(0).val("");
          }else{
              $("input.theme2").eq(0).addClass("d-none");
          }
        });
        $('#drpSpecimen').on('change', function() {
          var optionsText = this.options[this.selectedIndex].text;            
          $("input.theme2").eq(1).val(optionsText);
          if(this.value==0){
              $("input.theme2").eq(1).removeClass("d-none");
              $("input.theme2").eq(1).val("");
          }else{
              $("input.theme2").eq(1).addClass("d-none");
          }
        });
        
        $("[data-toggle='tooltip']").tooltip();
        var id = getUrlParameter('indicator');
        var save_date = getUrlParameter('date');
        $('#hdfid').val(getUrlParameter('theme'));
         var theme = $("#hdfid").val();
                if(theme==4){

                $("tbody tr td input").eq(4).prop( "disabled", true );
                $("tbody tr td input").eq(4).val("-").addClass("text-center");
                $("tbody tr td input").eq(9).prop( "disabled", true );
                $("tbody tr td input").eq(9).val("-").addClass("text-center");
                }
        $("#hdfsavedate").val(save_date);
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/indicatordata/getdata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'save_date':save_date},
            dataType: "json",				
            success: function (data) 
            {
                //console.log(data.data.length);return;
               
                if(data.data.length>0){
                    var param = data.data[0];
                    //console.log(param);
                    if (data.status=='success') { 
                        
                            if(data.data3.length>0){
                                $("#txthai").val(data.data3[0].hai);
                                $("#txtcai").val(data.data3[0].cai);
                            }
                            for (let i = 0; i < data.data.length; i++) {
                              //text += data.data[i] + "<br>";
                              $("#txtname"+data.data[i].question_id).val(data.data[i].name);

                            }
                            for (let i = 0; i < data.data2.length; i++) {
                              $("#ch"+data.data2[i].id+"[value='" + data.data2[i].question_id + "']").prop('checked', data.data2[i].name==0 ? false: true);
                            }
                        if(theme==5){
                            var theText = $("input.theme2").eq(0).val();
                            //alert(theText)
                            $("#drpDisease option").each(function() {
                              if($(this).text() == theText) {
                                $(this).attr('selected', 'selected');            
                              }                        
                            });
                            var theText2 = $("input.theme2").eq(1).val();
                            $("#drpSpecimen option").each(function() {
                              if($(this).text() == theText2) {
                                $(this).attr('selected', 'selected');            
                              }                        
                            });
                        }
                    }
                    else
                    {
                        alert(data.msg);
                    } 
                }
                
            }
        });
        
    });
    function ajax_savedata()
    {
        //$("#exampleModalCenter").modal('show');
        var i = 0;
        var j = 0;
        var k = 0;
        var l = 0;
        
        var id = $('#hdfid').val();
        if(id==4 || id==5 ){
            id=2;
        }
        var indicator_id = $('#hdfindicator').val();
        var save_date = $("#hdfsavedate").val();
        var data_question1 = $('.theme1').length;
        var data_question2 = $('.theme2').length;
        var data_question3 = $('.theme3').length;
        var data_question4 = $('.checkbox').length;
        
        var question1 = [];
        var question2 = [];
        var question3 = [];
        var question4 = [];
        
        var answer1 = [];
        var answer2 = [];
        var answer3 = [];
        var answer4 = [];

        if(data_question1==''){ data_question1=1}
        while(i <= data_question1)
        {      
            question1[i] = $('.theme1').eq(i).val();
            answer1[i] = $('.theme1').eq(i).attr("answer-value") 
            i++;
        }
        if(data_question2==''){ data_question2=1}
        while(j <= data_question2)
        {      
            question2[j] = $('.theme2').eq(j).val();
            answer2[j] = $('.theme2').eq(j).attr("answer-value") 
            j++;
        }
        if(data_question3==''){ data_question3=1}
        while(k <= data_question3)
        {      
            question3[k] = $('.theme3').eq(k).val();
            answer3[k] = $('.theme3').eq(k).attr("answer-value") 
            k++;
        }
        
        if(data_question4==''){ data_question4=1}
        while(l <= data_question4)
        {      
            if ($('.checkbox').eq(l).is(":checked"))
            {
               question4[l] = $('.checkbox').eq(l).val();
                answer4[l] = 1;
            }else{
                question4[l] = $('.checkbox').eq(l).val();
                answer4[l] = 0;
            }
            l++;
        }
        var chkhai = $("#hdfchkhai").val();
        var hai = $("#txthai").val();
        var cai = $("#txtcai").val();
        
        //console.log(question2,answer2);return;
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/indicatordata/savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id,'indicator_id':indicator_id,'question1':question1,'question2':question2,'question3':question3,'question4':question4,'answer1':answer1,'answer2':answer2,'answer3':answer3,'answer4':answer4,'save_date':save_date,'chkhai':chkhai,'hai':hai,'cai':cai},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {	
                    //$("#exampleModalCenter").modal('show');
                    //alert('บันทึกข้อมูลสำเร็จ');
                    window.location.href="/indicatordata/";
                    //showdata();	
                    //$("#modaldetail").modal('hide'); 

                    //fileimport();
                }else{
                    //$('#pleaseWaitDialog').modal('hide');
                    alert(data.msg);
                } 
            }
        });

    }    
</script>