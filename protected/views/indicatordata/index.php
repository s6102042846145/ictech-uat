<?php
	$this->pageTitle = 'ข้อมูลตัวชี้วัดของหน่วยงาน' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<style>
.btn {
    padding: 0px 10px;
    }
</style>

<section class="section section-20">
    
    <div class="row">
        <div class="col-md-8"><label></label></div>
        <div class="col-md-4">
            <div class="rd-mailform" data-form-output="form-output-global"  novalidate="novalidate">
              <div class="input-group input-group-custom input-group-sm no-wrap">
               <!-- <label class="form-label rd-input-label" for="txtkeyword">Search...</label>-->
                <input class="form-input mr-2 form-control-has-validation" id="txtkeyword" type="date">
                <button onClick="getSearch()" class="btn btn-sm btn-darkest" type="button">Search</button>
              </div>
            </div>
        </div>
    </div>
  <?php
               $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'list-grid',
                    'dataProvider' => $model,
                    'htmlOptions' => array('width' => '500px'),
                    'itemsCssClass' => 'table table-bordered table-striped',	
                    'rowHtmlOptionsExpression'=>'array("data-id"=>$data["id"],"data-date"=>$data["create_date"],"data-theme"=>$data["theme"],"data-create"=>$data["create_status"])',
                    'summaryText' => 'แสดงข้อมูล: {start} - {end} จาก {count} รายการ',
                    'pagerCssClass'=>'mailbox-pager',
                    'pager' => array(
                        'class'=>'CLinkPager',
                        'header' => '',
                        'firstPageLabel'=>'หน้าแรก',
                        'prevPageLabel'=>'ก่อนหน้า',
                        'nextPageLabel'=>'หน้าถัดไป', 
                        'lastPageLabel'=>'หน้าสุดท้าย',	

                    ),		
                    'columns' => array(
                        array(
                            'name'=>'create_date',
                            'header' => 'วันที่',
                            'htmlOptions'=>array('style'=>'text-align:left;'),
                            'headerHtmlOptions'=>array('style'=>'width:100px;text-align:center;'),
                          ),
                            
                         array(
                            'name'=>'indicator_name',
                            'header' => 'ตัวชี้วัด',
                            'htmlOptions'=>array('style'=>'text-align:left;'),
                            'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                          ),
                        array(
                            //'name'=>'active',
                            'type' => 'html',
                            'value' => array($this, 'getStatus'),
                            'header' => 'สถานะ',
                            'htmlOptions'=>array(
                                'style'=>'text-align:center;width:100px;'),
                                //'class'=>function($data){return $data['status']==2?"abc":"yes";}),
                            'headerHtmlOptions'=>array('style'=>'width:100px; text-align:center;'),
                        ),	
                         array(
                            'header' => 'บันทึก',
                            'class' => 'CLinkColumn',
                            'label' => '<i class="fas fa-edit"></i>',
                            'htmlOptions' => array(
                                'width' => '30px',
                                'align' => 'center',
                                'onclick'=>'setUpdate(this);'
                            ),
                            'linkHtmlOptions'=>array('class'=>'btn btn-info'),
                            'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                        ), 	
                    ),
                ));
            ?> 
    
    
</section>
<input type="hidden" id="hdfid" />


<script>
    $().ready(function(){
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0,10);
        });
        
    });
    function setUpdate(el)
    {
        /*
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (day<10 ? '0' : '') + day;
       
        //alert(status)
       
        */
        var id = $(el).parent().attr("data-id"); 
        var theme = $(el).parent().attr("data-theme"); 
        var status = $(el).parent().attr("data-create"); 
        var output = $(el).parent().attr("data-date");
        //alert(output);return;
        /*
        if(status!=undefined){
            output=status.substring(0, 10);
        }
        */
	    window.location.href = "/indicatordata/form?indicator="+id+"&date="+output+"&theme="+theme;
    }
    function getSearch() 
    {
        
        var keyword =  $('#txtkeyword').val();
        $('#txtkeyword').val(keyword);
        var data = {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','keyword':keyword}; 
        $.fn.yiiGridView.update('list-grid', {
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl("/indicatordata/search"); ?>',
            data: data,

        });

    } 
</script>