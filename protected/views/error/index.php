<?php
	$this->pageTitle = 'ไม่พบหน้าที่คุณต้องการ' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<div class="page animated" style="animation-duration: 500ms;">
  <section class="section section-single bg-accent">
    <div class="section-single-inner">
      <div class="section-single-header">
        <div class="container">
          <!--Brand--><a class="brand" href="/home"><img class="brand-logo-dark" src="/images/logo-default-154x53.png" alt="" width="77" height="26"><img class="brand-logo-light" src="/images/logo-inverse-154x53.png" alt="" width="77" height="26"></a>
        </div>
      </div>
      <div class="section-single-main">
        <div class="container">
          <!-- 404-->
          <h2>PAGE NOT FOUND</h2>
          <div class="text-extra-big text-white-opacity-02 font-accent">404</div>
          <p class="block-sm offset-top-50">The page requested couldn't be found - this could be due to a spelling error in the URL or a removed page.</p>
          <div class="group offset-top-30"><a class="btn btn-ellipse btn-specific" href="/home">back to home page</a></div>
        </div>
      </div>
      <div class="section-single-footer">
        <div class="container">
           <p class="rights"><span>©&nbsp;</span><span class="copyright-year">2022</span><span>&nbsp;</span><span>Developed by </span><span>.&nbsp;</span><a href="https://web.facebook.com/suilon">Yutthana Suilon</a></p>
        </div>
      </div>
    </div>
  </section>
</div>