<?php

class DepartmentController extends Controller
{
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{	
        $this->layout='adm_main'; 
        $model = lkup_department::Search();
        $this->render('index', array(	
			'model'=>$model
		));
	}
    public function actionForm()
	{	
        $id=isset($_GET['id'])?addslashes(trim($_GET['id'])):'';
        $indicators=isset($_GET['indicators'])?addslashes(trim($_GET['indicators'])):'';
        $this->layout='adm_main';  
        $modelTitle = lkup_department::SearchTitle();
        $modelFAQ = lkup_department::SearchFAQ($indicators);
		$this->render('/admin/department/form', array(	
			'modelTitle'=>$modelTitle,
            'modelFAQ'=>$modelFAQ
		));
	}
    public function actionIc()
	{	
        $this->layout='adm_main';  
		$this->render('/admin/department/ic');
	}
    public function actionSearchDataCheckList()
	{	
        $id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
        $data = lkup_department::Searchdatalist($id);
        echo CJSON::encode(array('data'=>$data));
	}
    
     public function actionSetHAI()
	{	
        $id=isset($_POST['indicator_id'])?addslashes(trim($_POST['indicator_id'])):'';
		$data=lkup_department::getHai($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'hai'=>$data[0]["hai"],
			'cai'=>$data[0]["cai"],
			
			));		

	}
    public function actionIndicators()
	{
			$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
			$data = lkup_department::getIndicators($id);
			echo CJSON::encode(array('data'=>$data));
	}
    
    
    public function actionSearch()
	{	
			$data = lkup_department::getSearch();
			echo CJSON::encode(array('data'=>$data));
	}
    public function actionSearchICRound()
	{	
            $id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
			$data = lkup_department::getICRound($id);
			echo CJSON::encode(array('data'=>$data));
	}
	
    
    public function actionSavedata()
    {

		$model=new frm_department;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->code=isset($_POST['code'])?addslashes(trim($_POST['code'])):'';
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
	
		if($model->id==''){
			if($model->save_insert()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
		}else{
        if($model->save_update()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    public function actionSavedataicround()
    {
//'id':id,'name':name,'department_id':department_id,'data_faq':data_faq},
		$model=new frm_department;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->department_id=isset($_POST['department_id'])?addslashes(trim($_POST['department_id'])):'';
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
		$model->data_faq=isset($_POST['data_faq'])?$_POST['data_faq']:''; 
	
		if($model->id==''){
			if($model->save_insert_icround()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
		}else{
        if($model->save_update_icround()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    public function actionSavedataicround2()
    {
		$model=new frm_department;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->department_id=isset($_POST['department_id'])?addslashes(trim($_POST['department_id'])):'';
		$model->title=isset($_POST['title'])?addslashes(trim($_POST['title'])):'';
		//$model->titleOld=isset($_POST['titleOld'])?$_POST['titleOld']:''; 
		$model->group_id=isset($_POST['group_id'])?$_POST['group_id']:''; 
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
        $model->data_id=isset($_POST['data_id'])?$_POST['data_id']:''; 
		$model->data_faq=isset($_POST['data_faq'])?$_POST['data_faq']:''; 
        if($model->save_insert_icround2()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');
        }
    }
    public function actionSavedatadetail()
    {

		$model=new frm_department;		
        $model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->indicator_id=isset($_POST['indicator_id'])?$_POST['indicator_id']:'';
		$model->theme=isset($_POST['theme'])?$_POST['theme']:'';
		$model->name=isset($_POST['name'])?$_POST['name']:'';
		$model->faq_sub=isset($_POST['faq_sub'])?$_POST['faq_sub']:'';
		$model->title_detail=isset($_POST['title_detail'])?$_POST['title_detail']:'';
        
		$model->data_faq=isset($_POST['data_faq'])?$_POST['data_faq']:'';
		$model->data_unit=isset($_POST['data_unit'])?$_POST['data_unit']:'';
		$model->data_number=isset($_POST['data_number'])?$_POST['data_number']:'';
        $model->data_detail=isset($_POST['data_detail'])?$_POST['data_detail']:'';
	//echo var_dump($model->data_faq); exit();
		if($model->id==''){
			if($model->save_insert_detail()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
		}else{
            if($model->save_update_detail()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    
    public function actionSaveHAI()
    {

        $model=new frm_department;		
        $model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
        $model->hai=isset($_POST['hai'])?$_POST['hai']:'';
        $model->cai=isset($_POST['cai'])?$_POST['cai']:'';
        if($model->id==''){
            if($model->save_hai()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
        }else{
            if($model->update_hai()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
		
    }
    public function actionDepartmentdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_department::getDepartment($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'name'=>$data[0]["name"],
			'code'=>$data[0]["code"],
			
			));		

	}
    public function actionDeletedata()
    {

		$model=new frm_department;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';	
        if($model->save_delete()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }

    }
    public function actionSetdata()
    {

		$model=new frm_department;		
		$model->indicators_id=isset($_POST['indicators_id'])?addslashes(trim($_POST['indicators_id'])):'';	
		$model->department_id=isset($_POST['department_id'])?addslashes(trim($_POST['department_id'])):'';	
		$model->status=isset($_POST['status'])?addslashes(trim($_POST['status'])):'';	
        if($model->save_indicators()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }

    }
    public function actionSetUpdateIC()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$department_id=isset($_POST['department_id'])?addslashes(trim($_POST['department_id'])):'';
		$data=lkup_department::getDataIC($id,$department_id);
		$data2=lkup_department::getDataICDetail($data[0]["id"]);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'data'=>$data,
			'data2'=>$data2,
			));		

	}
    /*
    public function getData($data) {
		$ret = "<input type='checkbox' value='".$data['id']."' ".$data['checked']." /> ";
		return $ret;
	}	
    */
    public function actionSetUpdateTitle()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_department::getTitle($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'name'=>$data[0]["name"],
			'detail'=>$data[0]["detail"],
			
			));		

	}
    public function actionSetUpdateFAQ()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		//$data=lkup_department::getUnit($id);
        
		$data=lkup_department::getQuesttionTitle($id);
        $title_name="";
        $question2="";
        if($data[0]["theme"]==3){
              $data2=lkup_department::getQuesttionTitle2($data[0]["id"]);
		      $question2=lkup_department::getQuesttion2($data[0]["id"]);
              $title_name=$data2[0]["name"];
        }
		$question=lkup_department::getQuesttion($data[0]["id"]);
        
        
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
            //'title_id'=>$data[0]["title_id"],
			'name'=>$data[0]["name"],
			'unit'=>$data[0]["unit"],
			//'indicator_id'=>$data[0]["indicator_id"],
			'tooltip'=>$data[0]["tooltip"],
            'theme'=>$data[0]["theme"],
            'question'=>$question,
            'title'=>$title_name,
            'question2'=>$question2,
			));		

	}
    
    public function getTheme($data){
		$active = $data['theme'];
		$ret = "";
		if($active==1){
			$ret.= "<img src='".Yii::app()->params['prg_ctrl']['theme-1']."' style='width: 50px;' />";
		}else if($active==2){
			$ret.= "<img src='".Yii::app()->params['prg_ctrl']['theme-2']."' style='width: 50px;' />";
		}else if($active==3){
			$ret.= "<img src='".Yii::app()->params['prg_ctrl']['theme-3']."' style='width: 50px;' />";
		}else{
			$ret.="";
		}
		return $ret;
	}
}