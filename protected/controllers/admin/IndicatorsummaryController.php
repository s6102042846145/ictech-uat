<?php

class IndicatorsummaryController extends Controller
{
	
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{		
        $this->layout='adm_main';
		$this->render('index');
	}
    public function actionSearch()
	{	
        $data = lkup_user::getSearch();
        echo CJSON::encode(array('data'=>$data));
	}
    public function actionSavedata()
    {
		$model=new frm_user;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->fname=isset($_POST['fname'])?addslashes(trim($_POST['fname'])):'';
		$model->lname=isset($_POST['lname'])?addslashes(trim($_POST['lname'])):'';
		$model->username=isset($_POST['username'])?addslashes(trim($_POST['username'])):'';
        $model->password=isset($_POST['password'])?addslashes(trim($_POST['password'])):'';        
        $model->department=isset($_POST['department'])?addslashes(trim($_POST['department'])):'';        
        $model->department_name=isset($_POST['department_name'])?addslashes(trim($_POST['department_name'])):'';        
        $model->tel=isset($_POST['tel'])?addslashes(trim($_POST['tel'])):'';        
        $model->address=isset($_POST['address'])?addslashes(trim($_POST['address'])):'';        
        $model->email=isset($_POST['email'])?addslashes(trim($_POST['email'])):'';        
        $model->remark=isset($_POST['remark'])?addslashes(trim($_POST['remark'])):'';        
	
		if($model->id==''){
			if($model->save_insert()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
		}else{
        if($model->save_update()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    public function actionGetdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lookupdata::getDayIndicators($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'data'=>$data,
			));		

	}
    
    
	public function actionDeletedata(){

		$model=new frm_user;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		
			if($model->save_delete()) {
					echo CJSON::encode(array('status' => 'success','msg' => '',));		 
				} else {
					echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_user'], ));		
						Yii::app()->session->remove('errmsg_user');
				}
	}
}