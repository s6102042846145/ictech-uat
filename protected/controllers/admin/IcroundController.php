<?php

class IcroundController extends Controller
{
	
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{		
        $this->layout='adm_main';
		$this->render('index');
	}
    public function actionSearch()
	{	
        $data = lkup_icround::getSearch();
        echo CJSON::encode(array('data'=>$data));
	}
    public function actionChecksaveIC()
	{	        
        $id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
        $data = lkup_icround::checkSave($id);
        echo CJSON::encode(array('data'=>$data));
	}
    public function actionSearchData()
	{	
        $data = lkup_icround::Searchdatalist();
        echo CJSON::encode(array('data'=>$data));
	}
    public function actionForm()
	{	
        $this->layout='adm_main';  
		$this->render('/admin/icround/form');
	}
    public function actionEdit()
	{	
        $this->layout='adm_main';  
		$this->render('/admin/icround/edit');
	}
    public function actionSavedata()
    {
		$model=new frm_icround;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->data_check_list=isset($_POST['data_check_list'])?$_POST['data_check_list']:'';   
		$model->id_check_list=isset($_POST['id_check_list'])?$_POST['id_check_list']:'';   
		$model->data_comment_list=isset($_POST['data_comment_list'])?$_POST['data_comment_list']:'';   
		$model->data_sub_check_list=isset($_POST['data_sub_check_list'])?$_POST['data_sub_check_list']:'';   
		$model->id_sub_check_list=isset($_POST['id_sub_check_list'])?$_POST['id_sub_check_list']:'';   
		$model->data_sub_comment_list=isset($_POST['data_sub_comment_list'])?$_POST['data_sub_comment_list']:'';   
	    $model->status=isset($_POST['status'])?addslashes(trim($_POST['status'])):'';
		if($model->status==''){
            if($model->save_insert()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
        }else{
            if($model->save_update()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
        }
		
    }
    
    public function actionGetdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_icround::getData($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'data'=>$data,
			));		

	}
     public function actionSetData()
     {
        $id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
        $save_date=isset($_POST['save_date'])?addslashes(trim($_POST['save_date'])):'';
		$data=lkup_icround::getDataGroup($id);
        $data2=lkup_icround::getDataGroupDetail($id);
        $data3=lkup_icround::getDataDetail($id);
         
		$department_data=lookupdata::getDepartmentlogin($id);
         
        $chk_data=lkup_icround::getDataCheckData($id,$save_date);         
        $chk_sub_data=lkup_icround::getDataCheckSubData($id,$save_date);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'data'=>$data,
            'data2'=>$data2,
            'data3'=>$data3,
            'department_data'=>$department_data,
            'chk_data'=>$chk_data,
            'chk_sub_data'=>$chk_sub_data,
			));		
     }
   
    public function actionUpdatelist(){
        $model=new frm_icround;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->chk=isset($_POST['chk'])?addslashes(trim($_POST['chk'])):'';
		
        if($model->save_update_list()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
            Yii::app()->session->remove('errmsg');
        }
    }
    public function actionUpdatelistsub(){
        $model=new frm_icround;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->chk=isset($_POST['chk'])?addslashes(trim($_POST['chk'])):'';
		
        if($model->save_update_list_sub()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
            Yii::app()->session->remove('errmsg');
        }
    }
    
	public function actionDeletedata(){

		$model=new frm_icround;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		
			if($model->save_delete()) {
					echo CJSON::encode(array('status' => 'success','msg' => '',));		 
				} else {
					echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_icround'], ));		
						Yii::app()->session->remove('errmsg_icround');
				}
	}
}