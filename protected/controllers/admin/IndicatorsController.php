<?php

class IndicatorsController extends Controller
{
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{		
        $this->layout='adm_main';       
		$this->render('index');
	}
    public function actionForm()
	{		
        $this->layout='adm_main';       
		$this->render('/admin/indicators/form');
	}
    public function actionSavedatadetail()
    {
        $model=new frm_indicators;		
        $model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
        $model->name=isset($_POST['name'])?$_POST['name']:'';
		$model->title_detail=isset($_POST['title_detail'])?$_POST['title_detail']:'';
        $model->theme=isset($_POST['theme'])?$_POST['theme']:'';
		$model->data_faq=isset($_POST['data_faq'])?$_POST['data_faq']:'';       
		$model->data_unit=isset($_POST['data_unit'])?$_POST['data_unit']:'';
        $model->data_number=isset($_POST['data_number'])?$_POST['data_number']:'';
		$model->data_detail=isset($_POST['data_detail'])?$_POST['data_detail']:'';
        $model->chkHai=isset($_POST['chkHai'])?$_POST['chkHai']:'';        
        $model->hai=trim(isset($_POST['hai'])?$_POST['hai']:'');        
        $model->cai=trim(isset($_POST['cai'])?$_POST['cai']:'');        
		$model->faq_sub=isset($_POST['faq_sub'])?$_POST['faq_sub']:'';
        /*
		$model->indicator_id=isset($_POST['indicator_id'])?$_POST['indicator_id']:'';
		
        
		$model->data_faq=isset($_POST['data_faq'])?$_POST['data_faq']:'';
		$model->data_unit=isset($_POST['data_unit'])?$_POST['data_unit']:'';
		$model->data_number=isset($_POST['data_number'])?$_POST['data_number']:'';
        $model->data_detail=isset($_POST['data_detail'])?$_POST['data_detail']:'';
        */
	//echo var_dump($model->data_faq); exit();
		if($model->id==''){
			if($model->save_insert_detail()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
		}else{
            if($model->save_update_detail()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    public function actionSetData()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		//$data=lkup_department::getUnit($id);
        
		$data=lkup_indicators::getData($id);
        $data_theme="";
        $data_number="";
        $title="";
        if($data[0]["theme"]==2){
              $data_theme=lkup_indicators::getDataTheme2($data[0]["id"]);
		     
        }
        if($data[0]["theme"]==3){
              $data_theme=lkup_indicators::getDataTheme2($data[0]["id"]);
		      $data_number=lkup_indicators::getDataTheme3($data[0]["id"]);
		      $title=$data_number[0]["title"];
        }
        echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
            'tooltip'=>$data[0]["tooltip"],
			'name'=>$data[0]["name"],
			'unit'=>$data[0]["unit"],
			//'indicator_id'=>$data[0]["indicator_id"],
			'hai'=>$data[0]["hai"],
			'd_hai'=>$data[0]["d_hai"],
			'd_cai'=>$data[0]["d_cai"],
            'theme'=>$data[0]["theme"],
            'data_theme'=>$data_theme,
            'data_number'=>$data_number,
            'title'=>$title,
            
			));		
        
        /*
		$data=lkup_department::getQuesttionTitle($id);
        $title_name="";
        $question2="";
        if($data[0]["theme"]==3){
              $data2=lkup_department::getQuesttionTitle2($data[0]["id"]);
		      $question2=lkup_department::getQuesttion2($data[0]["id"]);
              $title_name=$data2[0]["name"];
        }
		$question=lkup_department::getQuesttion($data[0]["id"]);
        
        
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
            //'title_id'=>$data[0]["title_id"],
			'name'=>$data[0]["name"],
			'unit'=>$data[0]["unit"],
			//'indicator_id'=>$data[0]["indicator_id"],
			'tooltip'=>$data[0]["tooltip"],
            'theme'=>$data[0]["theme"],
            'question'=>$question,
            'title'=>$title_name,
            'question2'=>$question2,
			));		
*/
	}
    
    
    
    
    
    
    
    
    public function actionSavedata()
    {

		$model=new frm_indicators;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->code=isset($_POST['code'])?addslashes(trim($_POST['code'])):'';
		$model->hai=isset($_POST['hai'])?addslashes(trim($_POST['hai'])):'';
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
	
		if($model->id==''){
			if($model->save_insert()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
		}else{
        if($model->save_update()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    public function actionGetdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_indicators::getGetdata($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'code'=>$data[0]["code"],
			'name'=>$data[0]["name"],
			'hai'=>$data[0]["hai"],
			
			));		

	}
    public function actionDeletedata()
    {

		$model=new frm_indicators;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';	
        if($model->save_delete()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }

    }
}