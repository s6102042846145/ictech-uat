

<?php

class ProblemandsuggestionController extends Controller
{
    
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{		
        $this->layout='adm_main';
		$this->render('index');
	}
    public function actionSearch()
	{	
        $data = lkup_problemandsuggestion::getSearch();
        echo CJSON::encode(array('data'=>$data));
	}
    
    public function actionDeletedata()
    {

		$model=new frm_problemandsuggestion;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';	
        if($model->save_delete()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }

    }
    
    
}