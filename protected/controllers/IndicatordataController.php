<?php

class IndicatordataController extends Controller
{
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{	
		$model = lkup_indicatordata::Search(Yii::app()->session['keyword']);	
		$this->render('index', array(	
			'model'=>$model
		));
	}
    public function actionSearch()
	{
		//if(isset($_GET['ajax']) && isset($_GET['sort'])){
			
		if(isset($_GET['ajax']) && !isset($_POST['YII_CSRF_TOKEN'])){
			$keyword = Yii::app()->session['keyword'];	
				
		} else {
			$keyword = isset($_POST['keyword'])?addslashes(trim($_POST['keyword'])):'';				
			Yii::app()->session['keyword']=$keyword;	
		}
        //echo var_dump($keyword);exit();
		$model = lkup_indicatordata::Search($keyword);			
		$this->renderPartial('index', array('model'=>$model));
		
		
	}
	public function actionForm()
	{			
        //$indicators=isset($_GET['indicator'])?addslashes(trim($_GET['indicator'])):'';
		$this->render('/indicatordata/form');
	}
    
	
	public function actionSavedata()
	{
		
		$model=new frm_indicatordata;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->indicator_id=isset($_POST['indicator_id'])?addslashes(trim($_POST['indicator_id'])):'';
		$model->question1=isset($_POST['question1'])?$_POST['question1']:'';
		$model->question2=isset($_POST['question2'])?$_POST['question2']:'';
		$model->question3=isset($_POST['question3'])?$_POST['question3']:'';
		$model->question4=isset($_POST['question4'])?$_POST['question4']:'';
		$model->answer1=isset($_POST['answer1'])?$_POST['answer1']:'';
		$model->answer2=isset($_POST['answer2'])?$_POST['answer2']:'';
		$model->answer3=isset($_POST['answer3'])?$_POST['answer3']:'';
		$model->answer4=isset($_POST['answer4'])?$_POST['answer4']:'';
		$model->save_date=isset($_POST['save_date'])?$_POST['save_date']:'';
		$model->chkhai=isset($_POST['chkhai'])?addslashes(trim($_POST['chkhai'])):'';
		$model->hai=isset($_POST['hai'])?addslashes(trim($_POST['hai'])):'';
		$model->cai=isset($_POST['cai'])?addslashes(trim($_POST['cai'])):'';
        
        if($model->save_insert()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
            Yii::app()->session->remove('errmsg');
        }
		
	}
    
    public function actionGetdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$save_date=isset($_POST['save_date'])?addslashes(trim($_POST['save_date'])):'';
		$data=lkup_indicatordata::chkData($id,$save_date);
        $data2=lkup_indicatordata::chkData2($id);
        $data3=lkup_indicatordata::chkData3($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'data'=>$data,
			'data2'=>$data2,
			'data3'=>$data3,
			));		

	}
    public function actionGetPopupHAIAndCAI()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_indicatordata::getHAIAndCAI($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'data'=>$data,
			));		

	}
    public function actionGetPopupIndicator()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data = lkup_indicatordata::getPopupIndicator($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'data'=>$data,
			));		

	}
    
    public function getStatus($data){
		$status = $data['create_status'];
		$ret = "";
		if($status!=""){
			$ret.= "<div class='text-bold text-success'>บันทึกแล้ว</div>";
		}else{
			$ret.="<div class='text-bold text-warning'>รอการบันทึก</div>";
		}
		return $ret;
	}
    
	public function actionDeletedata(){

		$model=new frm_user;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		
			if($model->save_delete()) {
					echo CJSON::encode(array('status' => 'success','msg' => '',));		 
				} else {
					echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_user'], ));		
						Yii::app()->session->remove('errmsg_user');
				}
	}
}