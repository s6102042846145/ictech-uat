<?php
class Controller extends CController
{	
	public $layout='//layouts/main';	
	public $menu=array();
	public $breadcrumbs=array();
    public $pageTitle = '';	
	
	public function chkLogin() {
		if(Yii::app()->user->isGuest) { 
			$this->redirect(Yii::app()->getBaseUrl(true)); 
		} else {	
			if(Yii::app()->user->getInfo('username')==false){$this->redirect(Yii::app()->createUrl('dashboard'));}
		}
	}		
    public function init()
    {
        parent::init();
    }	

}