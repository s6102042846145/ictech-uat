<?php

class CompanyUploadHandle extends UploadHandler
{
	/* Process after uploaded */
    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null)   {
        $file = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error, $index, $content_range);
		if (!empty($file->error)) { 
			$file->error = Yii::t('err',$file->error); 
		}
        return $file;
    }

    /* Converts a filename into a randomized file name */
    private function _generateRandomFileName($name) {
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        return md5(uniqid(rand(), true)).'.'.$ext;
    }

    /* Overrides original functionality */
    protected function trim_file_name($file_path, $name, $size, $type, $error,$index, $content_range) {
        $name = parent::trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range);
        return $this->_generateRandomFileName($name);
    }

	/* Overrides original functionality */
    protected function get_file_name($file_path, $name, $size, $type, $error,$index, $content_range) 
	{
		$ext = pathinfo($name, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Bangkok');
		$name = 'company_'.date('YmdHis').'.'.$ext;
		return $name;
		
    }
 	protected function get_file_name_attach($file_path, $name, $size, $type, $error,$index, $content_range) 
	{
		
		$ext = pathinfo($name, PATHINFO_EXTENSION);
		date_default_timezone_set('Asia/Bangkok');
		$name = 'at'.date('YmdHis').'.'.$ext;
		return $name;
		
    }

	protected function get_file_resize($name)
	{
		require_once Yii::getPathOfAlias('application') . Yii::app()->params['prg_ctrl']['vendor']['phpthumb']['path']; 
		$fullpath=Yii::app()->params['prg_ctrl']['path']['upload'];
		$fullurl=Yii::app()->params['prg_ctrl']['url']['upload'];
		$ext = pathinfo($name, PATHINFO_EXTENSION);
		$name_file = pathinfo($name, PATHINFO_FILENAME);
		
	
		$company_whidth = '150';
		$company_height = '35';				

		Yii::app()->session['size_company'] = $company_whidth.'x'.$company_height;
				
		try
		{
			 $thumb_company = PhpThumbFactory::create($fullpath.'\\'.$name);
		}
		catch (Exception $e)
		{
			 // handle error here however you'd like
		}
		
		$thumb_company->resize($company_whidth, $company_height);
		//$thumb_s->adaptiveResize($size_s_whidth, $size_s_height);
		if (!file_exists($fullpath.'\\company')) {
			mkdir($fullpath.'\\company', 0777, true);
		}
		$thumb_company->save($fullpath.'\\company'.'\\'.$name);
		$file_company_size=filesize($fullpath.'\\company'.'\\'.$name);
		Yii::app()->session['file_company_size']=$file_company_size;

	}


}

?>