function ex_date2db(ddmmyyyy) {
	//dd/mm/yyyy => yyyy-mm-dd
	return ddmmyyyy.substr(6, 4)+'-'+ddmmyyyy.substr(3, 2)+'-'+ddmmyyyy.substr(0, 2); 
} 